/*
 Localizable.strings
 AlcoFind
 
 Created by GreamSoft_Mingz on 2015. 10. 29..
 Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
 */
// @"English" : 0
// @"Germany" : 1
// @"Polish"  : 2
// @"Swedish" : 3
// @"Franch"  : 4
// @"Spanish" : 5

// 기본
"BAC" = "%BAC";
"Promille" = "‰(g/L)";
"ug/L" = "ug/L";
"mg/L" = "mg/L";
"mg/100mL" = "mg/100mL";
"English" = "English";
"Finnish" = "Suomalainen";
"Franch" = "Français";
"German" = "Deutsch";
"Norwegian" = "Norsk";
"Polish" = "Polski";
"Russian" = "Русский";
"Spanish" = "Español";
"Korean" = "한국어";

// Intro 화면
"titleIntro" = "Set language and unit";

// Main 화면
"SCANNING" = "SCANNING";
"SCANNING_TITLE" = "Make sure your device is turned on and connected to app.";
"START_TEST" = "START TEST";
"START_TITLE" = "Wait at least 30 mins after drinking, smoking, or eating before testing.";
"DISCONNECT_TITLE" = "블루투스 해제"; //?
"DISCONNECT_MESSAGE" = "블루투스 해제 되었습니다."; //?
"Calibration" = "Contact your seller for calibration service. The result could be inaccurate if calibration is not performed periodically.";
"Done" = "Done";

// Test 화면
"TestTitle" = "Who's going to test?";
"Analyzing" = "Analyzing data";
"WarningTitle" = "Healthcare warning message";
"WarningMessage" = "You are drinking more than a set number of drinking.Take care of your health!";
"WarmingUp" = "Warming Up";
"BreathDeep" = "Breath deep";
"KeepBlowing" = "Keep blowing";
"StartBlowing" = "Start blowing";
"TestResult" = "Test Result";
"Retest" = "Retest";
"TestMessageInAlert" = "Failed to continue testing because you exited the app without completing testing. \nPlease try again.";

// Game 화면
"NEXT" = "NEXT";
"People" = "";
"Game1Title" = "How many players?";
"start" = "Start";
"Game" = "B.A.C Guessing Game";
"no" = "No.";
"name" = "Name";
"estimated" = "your guess";
"Result" = "Result";
"Restart" = "Restart";
"Ranking" = "Ranking";
"result" = "Result";

"hintMemo" = "Enter memo";
"hintName" = "Enter name";
"hintGuess" = "Enter your guess";

// Data 화면
"Graph" = "Graph";
"Detail" = "Detail";
"Me" = "Me";
"Mine" = "Mine";
"Friend" = "Friend";
"Day" = "Day";
"Week" = "Week";
"Month" = "Month";
"All" = "All";
"Edit" = "Edit";
"Memo" = "Memo";
"Photo" = "Photo";
"TestedWithout" = "Tested\nwithout\napp";
"titleInAlertDelData" = "Are you sure you want to delete this data?";
"ErrorTitle1" = "Replace with a new alkaline AAA battery.";
"ErrorTitle2" = "Calibration service is needed. Contact your seller for calibration service.";
"ErrorTitle5" = "An error has occurred. Please try again.";
"ErrorTitle6" = "The blowing session timed out. Please try again.";
"ErrorTitle7" = "The sampling volume is not enough to analyze the sample. Please try again.";
"ErrorTitle9" = "An error has occurred. Please try again.";
"ErrorTitle11" = "Testing cannot be performed because the device is in unacceptable temperature range. Try again after the device has reached a temperature within the operating range (5-40 °C).";
"Temperature" = "Temperature error";

// Setting 화면
"titleInAlertDel" = "Are you sure you want to delete all saved user data?";
"OK" = "OK";
"Cancel" = "Cancel";
"Delete" = "Delete";
"Back" = "Back";

"Healthcare" = "Healthcare warning message";
"Alarmlimit" = "Limit line";
"LimitedNumber" = "Limited\nNumber";
"WarningLimit" = "Warning\nLimit";
"DeleteAll" = "Delete all data";
"Sound" = "Sound";
"Camera" = "Camera";
"Unit" = "Unit";
"Language" = "Language";
"OFF" = "OFF";
"ON" = "ON";

"HelpMessage1" = "The \"Limit line\" will be displayed as the blue line in all Graphs, which helps you understand easily how different your BAC value and the \"Limit line\" are. \nIn the Standalone mode, the device will show the test result based on this \"Limit line\".";
"HelpMessage2" = "This feature is kind of like the healthcare service. If your BAC value is over \"Warning Limit\" and the total number of test result that is over \"Warning Limit\" is more than \"Limited Number\" for a month, the app will shows the warning message to help you control the drinking habits.";
"HelpMessage3" = "You can delete all saved user data.";
"HelpMessage4" = "You can enable the beep sound for blowing.";
"HelpMessage5" = "If you enable the camera feature, the app will take a photo every time, once the blowing has been completed.";

// How To Use
// Alert
"Info" = "Info"; //?
"Error" = "Error";

"HTU_Title1" = "The remaining battery power";
"HTU_Message1" = "If the battery indicator display is empty, install a new alkaline AAA battery.";

"HTU_Title2" = "The total number of tests performed";
"HTU_Message2" = "If the total number of tests reaches 500 times, contact your seller for proper recalibration service.\n\nNote: The result could be inaccurate if calibration is not performed periodically.";

"HTU_Title3" = "The remaining calibration date";
"HTU_Message3" = "If the countdown reaches 0 day, contact your seller for proper recalibration service.\n\nNote: The result could be inaccurate if calibration is not performed periodically.";

"HTU_Title4" = "Data";
"HTU_Message4" = "View all history of test results.";

"HTU_Title5" = "How to use";
"HTU_Message5" = "View how it works.";

"HTU_Title6" = "Home";
"HTU_Message6" = "Enable you to start testing.";

"HTU_Title7" = "Setting";
"HTU_Message7" = "Enable you to change the options.";

"HTU_Title8" = "B.A.C Guessing Game";
"HTU_Message8" = "Check out who has a better perception of BAC level by comparison between guessed BAC level and actual BAC level.";


// 1페이지
"HowToUsePage1Title" = "HOW TO USE";
"HowToUsePage1Text" = "Tap the question mark to get a clear overview of different menus. \n\nSwipe left from the right side of screen to view quick operation guide.";

// 2페이지
"HowToUsePage2Title" = "Operation";
"HowToUseText1" = "*Wait at least 30 minutes after dinking, eating, or smoking before testing. Testing without this waiting period can cause inaccurate readings and damage to the sensor.\n\n\nStep #1.\nOpen the app. \n\nNote: Bluetooth must first be switched on in order to connect to the device (Go to settings on your smartphone and turn on Bluetooth). ";
"HowToUseText1_Big" = "Step #1.";
"HowToUseText1_Color" = "Bluetooth";

"HowToUseText2" = "Step #2.\nTo turn on the device, press and hold the Power Button for 2 seconds until all the Blue, Green, and Red LEDs appear on the device.";

"HowToUseText2_1" = "Note: If you press and hold the Power Button continuously until the Blue LED is blinking, the device moves on the Standalone Mode for testing without using the app.";

"HowToUseText2_Big" = "Step #2.";

"HowToUseText3" = "Step #3.\nThe Blue LED on the device blinks during the pairing process.";
"HowToUseText3_Big" = "Step #3.";


"HowToUseText4" = "Step #4.\nThe \"Start test\" will appear on the app if the device has successfully connected to the app. Tap \"Start test\" on the app( or press the Power Button on the device) to start test. ";
"HowToUseText4_Big" = "Step #4.";
"HowToUseText4_Color" = "Start test";

"HowToUseText5" = "Step #5.\nChoose the user. Each test result will be saved separately according to the selection of user - All history of saved test results are viewable from the Graph icon at the bottom menu.";
"HowToUseText5_Big" = "Step #5.";
"HowToUseText5_Color" = "Graph icon";

"HowToUseText6" = "Step #6.\nThe circular progress bar on the app indicates the progress of warm-up. When the progress bar reaches 95%, take a deep breath and get ready for blowing.";
"HowToUseText6_Big" = "Step #6.";
"HowToUseText6_Color" = "95";

"HowToUseText7" = "Step #7.\nWhen the \"Start blowing\" is shown on the app, blow into your device for 5 seconds until the progress bar on the app reaches 100% or you feel the air has been pulled through the air pump in the device with a click. ";
"HowToUseText7_Big" = "Step #7.";
"HowToUseText7_Color" = "Start blowing";
"HowToUseText7_Color1" = "100";

"HowToUseText8" = "Step #8.\nIf a sufficient breath sample is taken correctly, the \"Analyzing data\" will be shown on the app.";
"HowToUseText8_Big" = "Step #8.";
"HowToUseText8_Color" = "Analyzing data";

"HowToUseText9" = "Step #9.\nThe BAC (Blood Alcohol Content) value will be displayed on the app in a few seconds (It may vary depending on the BAC level). In this page, the app enables you to make a memo which is viewable with the test result from the Graph icon, and test result can be also shareable Text message, Facebook, or Twitter.\n\nTap either \"RETEST\" or The \"BAC value\" on the app( or press the Power Button on the device) for retesting without selecting user.\n\nTap the Home icon at the bottom menu If you want to start again from the beginning.\n\nWhen you have finished testing, press and hold the Power Button on the device for 2 seconds to turn it off.\n\nNote: To enable the camera feature, tap the Setting icon at the bottom menu on the app, and enable the camera by sliding the ON/OFF switch.";
"HowToUseText9_EN" = "Step #9.\nThe BAC (Blood Alcohol Content) value will be displayed on the app in a few seconds (It may vary depending on the BAC level). In this page, the app enables you to make a memo which is viewable with the test result from the Graph icon, and test result can be also shareable Text message, Facebook, or Twitter.\n\nNote: The test results will be shown as HIGH if the result is over 1.00‰ (0.50 mg/L / 0.10 %BAC) according to the requirements of EN Standard (EN 16280).\n\nNote: The test results will be shown for 10 seconds. During this time, all other functions will be inactive according to the requirements of EN Standard (EN 16280).\n\nTap either \"RETEST\" or The \"BAC value\" on the app( or press the Power Button on the device) for retesting without selecting user.\n\nTap the Home icon at the bottom menu If you want to start again from the beginning.\n\nWhen you have finished testing, press and hold the Power Button on the device for 2 seconds to turn it off.\n\nNote: To enable the camera feature, tap the Setting icon at the bottom menu on the app, and enable the camera by sliding the ON/OFF switch.";
"HowToUseText9_AU" = "Step #9.\nThe BAC (Blood Alcohol Content) value will be displayed on the app in a few seconds (It may vary depending on the BAC level). In this page, the app enables you to make a memo which is viewable with the test result from the Graph icon, and test result can be also shareable Text message, Facebook, or Twitter.\n\nNote: The test results will be shown as HIGH if the result is over 0.400%BAC (2.00 mg/L / 4.00 ‰) according to the requirements of Australian Standard (AS 3547).\n\nNote: The test results will be shown for 15 seconds. During this time, all other functions will be inactive according to the requirements of Australian Standard (AS 3547).\n\nTap either \"RETEST\" or The \"BAC value\" on the app( or press the Power Button on the device) for retesting without selecting user.\n\nTap the Home icon at the bottom menu If you want to start again from the beginning.\n\nWhen you have finished testing, press and hold the Power Button on the device for 2 seconds to turn it off.\n\nNote: To enable the camera feature, tap the Setting icon at the bottom menu on the app, and enable the camera by sliding the ON/OFF switch.";
"HowToUseText9_Big" = "Step #9.";
"HowToUseText9_Color" = "memo";
"HowToUseText9_Color1" = "Text message";
"HowToUseText9_Color2" = "Facebook";
"HowToUseText9_Color3" = "Twitter";
"HowToUseText9_Color4" = "RETEST";
"HowToUseText9_Color5" = "BAC value";

"BlueToothFindMessage" = "I can't find the previously paired device.\n Would you like to try to pair with another device?";

"labelTitleInAlertPIN" = "Bluetooth pairing request";
"labelMessageInAlertPIN" = "Type the device's requested PIN:\n[000000]";
"labelPinError" = "You entered an incorrect PIN.";
"Pair" = "Pair";

"HIGH" = "HIGH";
