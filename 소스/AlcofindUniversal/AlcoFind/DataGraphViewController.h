//
//  DataGraphViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataGraphView.h"
#import "DataGraphLabelY.h"
#import "DataDensity.h"
#import "UtilLocal.h"
#import "ActionSheetStringPicker.h"
#import "TabbarViewController.h"

@interface DataGraphViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet DataGraphView *dataGraphView;
@property (strong, nonatomic) IBOutlet DataGraphLabelY *dataGraphLabelY;
@property (strong, nonatomic) IBOutlet UIButton *buttonMe;
@property (strong, nonatomic) IBOutlet UIButton *buttonFriend;
@property (strong, nonatomic) IBOutlet UIButton *buttonDate;
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;

@property (assign, nonatomic) NSUInteger pickerDateSelectedIndex;
@property (retain, nonatomic) NSString* type;
@property (retain, nonatomic) NSMutableArray* arrayDate;

@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

//그래프 화살표 데이터 있을 시 표시(2019.01.07)
@property (strong, nonatomic) IBOutlet UIButton *leftBtn;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;


@end
