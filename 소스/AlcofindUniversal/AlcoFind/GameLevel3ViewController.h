//
//  GameLevel3ViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ArcView.h"
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"
#import "UIButtonCustom.h"

@interface GameLevel3ViewController : UIViewController <TIBLEDelegate>

@property (strong, nonatomic) IBOutlet ArcView *arcView;
@property (assign, nonatomic) NSUInteger number;
@property (strong, nonatomic) IBOutlet UILabel *labelNumber;
@property (strong, nonatomic) IBOutlet UILabel *labelText;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewGuage;

@property (strong, nonatomic) IBOutlet UIView *viewAnalyzing;
@property (strong, nonatomic) IBOutlet UIView *viewBlowing;
@property (strong, nonatomic) IBOutlet UIView *viewAlertInfo;

@property (strong, nonatomic) IBOutlet UILabel *labelInfoName;
@property (strong, nonatomic) IBOutlet UILabel *labelInfoPage;

@property (retain, nonatomic) NSTimer* timerSend5;
@property (assign, nonatomic) NSUInteger timerCount;

@property (retain, nonatomic) AVAudioPlayer* audioPlayer;

@property (strong, nonatomic) IBOutlet UILabel *labelAnalyzing;
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;

// 알림창 : Info
@property (strong, nonatomic) IBOutlet UILabel *labelInfoInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertInfo;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertInfo;
@property (strong, nonatomic) IBOutlet UIView *viewDimmedNavBar;

@property(retain, nonatomic) TabbarViewController* tabbarVC;

@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

@property (assign, nonatomic) BOOL flagNumberAnimation;
@property (assign, nonatomic) BOOL flagBecome;
@end