//
//  DataGraphLabelY.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 30..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataGraphLabelY.h"

@implementation DataGraphLabelY

@synthesize level;
@synthesize spacingY;

// 초기화 생성자 : 스토리보드
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
    }
    return self;
}

- (void) drawClear
{
    [[self subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
}

// 라벨 텍스트
- (void)drawLabelY:(CGRect)rect
{
    // UI 관련 변수
    NSMutableArray* arrayLabelY = [NSMutableArray array];

    
    float fontsizeLabel = 0.0f;
    
    if ([UtilScreen getInch] == INCH_3_5)
    {
        fontsizeLabel = 8.0f;
    }
    // 4 inch
    else if ([UtilScreen getInch] == INCH_4_0)
    {
        fontsizeLabel = 8.0f;
    }
    // 4.7 inch
    else if ([UtilScreen getInch] == INCH_4_7)
    {
        fontsizeLabel = 9.0f;
    }
    // 5.5 inch
    else if ([UtilScreen getInch] == INCH_5_5)
    {
        fontsizeLabel = 11.0f;
    }
    // 5.8 inch 1812 ko
    else if ([UtilScreen getInch] == INCH_5_8)
    {
        fontsizeLabel = 11.0f;
    }
    
    // 라벨 생성
    for (int i = 0; i<level; i++)
    {
        CGRect frame = CGRectMake(0.0f, 0.0f+(spacingY*i), rect.size.width-3.0f, spacingY);
        UILabel* label = [[UILabel alloc] initWithFrame:frame];
        
        [label setTextAlignment:NSTextAlignmentRight];
        [label setFont:[UIFont fontWithName:@"Roboto-Medium" size:fontsizeLabel]];   // 5.5inch:11.0f / 4.7inch:10.0f  / 4inch : 9.0f
        [label setTextColor:[UIColor whiteColor]];
        
//        label.layer.borderWidth = 1.0f;
//        label.layer.borderColor = [[UIColor whiteColor] CGColor];
        [self addSubview:label];
        
        [arrayLabelY addObject:label];
    }
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray* arrayUnitMax = [userDefaults arrayForKey:@"UNIT_MAX"];
    NSInteger selectedIndex = [userDefaults integerForKey:@"UNIT_SELECTED"];
    NSArray* array = [arrayUnitMax objectAtIndex:selectedIndex];
    
    // 라벨 값 설정
    for (int i=0; i<level; i++)
    {
        if (i != 0)
        {
            UILabel* label = [arrayLabelY objectAtIndex:level-1-i];
            NSString* str = [array objectAtIndex:i];
            label.text = str;
        }
    }
}

- (void)drawRect:(CGRect)rect
{
    [self drawLabelY:rect];
}

@end
