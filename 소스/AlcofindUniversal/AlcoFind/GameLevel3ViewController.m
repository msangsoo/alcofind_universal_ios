//
//  GameLevel3ViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "GameLevel3ViewController.h"

@interface GameLevel3ViewController ()

@end

@implementation GameLevel3ViewController

@synthesize number;
@synthesize arcView;
@synthesize imageViewGuage;
@synthesize labelNumber;
@synthesize labelText;
@synthesize labelInfoName;
@synthesize labelInfoPage;

@synthesize viewBlowing;
@synthesize viewAnalyzing;
@synthesize viewAlertInfo;

@synthesize timerCount;
@synthesize timerSend5;

@synthesize audioPlayer;
@synthesize labelAnalyzing;
@synthesize buttonBack;

@synthesize labelInfoInAlertInfo;
@synthesize labelMessageInAlertInfo;
@synthesize buttonOkInAlertInfo;
@synthesize viewDimmedNavBar;

@synthesize tabbarVC;

@synthesize labelDday;
@synthesize labelCount;

@synthesize flagBecome;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    [[TIBLECBKeyfob keyfob] setDelegate:self];
    
    self.flagBecome = NO;
    
    self.labelAnalyzing.text = [UtilLocal localString:@"Analyzing"];
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    [self.buttonOkInAlertInfo setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    
    // 백버튼 탭바 딤드
    [self.viewDimmedNavBar setHidden:YES];
    [self.tabbarVC.viewDimmed setHidden:YES];
    
    // 탭바컨트롤러 설정
    [self.tabbarVC setNoActionButtonIndex:4];
    [self.tabbarVC setSelectedButtonIndex:4];
    
    // 노티 등록
    NSNotificationCenter* noti = [NSNotificationCenter defaultCenter];
    [noti addObserver:self selector:@selector(viewBecome) name:UIApplicationWillEnterForegroundNotification object:nil];
    [noti addObserver:self selector:@selector(viewResign) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [self setNavBarInfo];
    
    self.flagNumberAnimation = NO;
    
    
    ///////////////// 임시 ////////////////
//    NSMutableDictionary* dicc = [NSMutableDictionary dictionary];
//    [dicc setObject:[NSNumber numberWithInt:5] forKey:@"WARMINGUP_TIME"];
//    [self performSelector:@selector(receiveData5:) withObject:dicc afterDelay:1.0f];
//    [self performSelector:@selector(receiveData6:) withObject:nil afterDelay:6.0f];
//    [self performSelector:@selector(receiveData7:) withObject:nil afterDelay:7.0f];
//    [self performSelector:@selector(receiveData8:) withObject:nil afterDelay:8.0f];
//    [self performSelector:@selector(receiveData9:) withObject:nil afterDelay:9.0f];
    ///////////////// 임시 ////////////////
}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = nil;
    
    // 노티 해제
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            self.tabbarVC = (TabbarViewController*)vc;
        }
    }

    [self.viewBlowing setHidden:NO];
    [self.viewAnalyzing setHidden:YES];
    [self.viewAlertInfo setHidden:YES];
    self.labelNumber.text = @"00";
    [self.labelText.layer removeAllAnimations];
    self.labelText.text = [UtilLocal localString:@"WarmingUp"];
    self.labelText.textColor = [UIColor whiteColor];

    
    [[TIBLECBKeyfob keyfob] setDelegate:self];
    
    // 블루투스 통신 초기화
    timerCount = 0;
    self.timerSend5 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sendData5:) userInfo:nil repeats:YES];
    [[TIBLECBKeyfob keyfob] setArrayGameInfoIndex:0];
}

- (void) sendData5:(NSTimer *)timer
{
    // 백버튼 탭바 딤드
    [self.viewDimmedNavBar setHidden:YES];
    [self.tabbarVC.viewDimmed setHidden:YES];
    self.labelNumber.text = @"00";
    [self.labelText.layer removeAllAnimations];
    self.labelText.text = [UtilLocal localString:@"WarmingUp"];
    self.labelText.textColor = [UIColor whiteColor];

    
    timerCount++;
    if (timerCount <=5)
    {
        [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_05];
        NSLog(@"5번 보내기");
    } else {
        [self.timerSend5 invalidate];
        self.timerSend5 = nil;
        timerCount = 0;
    }
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

- (void) viewSetWarming
{
    self.labelNumber.text = @"00";
    [self.arcView animateArcTo0:0.0f];
    [self.labelText.layer removeAllAnimations];
    self.labelText.text = [UtilLocal localString:@"WarmingUp"];
    self.labelText.textColor = [UIColor whiteColor];
    
    timerCount = 0;
    self.timerSend5 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sendData5:) userInfo:nil repeats:YES];
}

- (void) viewBecome
{
    self.flagBecome = YES;
    self.labelInfoInAlertInfo.text = [UtilLocal localString:@"Info"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"TestMessageInAlert"];
    [self.viewAlertInfo setHidden:NO];
}

// UIApplicationDidBecomeActiveNotification
- (void) viewResign
{
    self.labelInfoInAlertInfo.text = [UtilLocal localString:@"Info"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"TestMessageInAlert"];
    [self.viewAlertInfo setHidden:NO];
}

// 네비바 설정 : 카운트, 디데이 정보
- (void) setNavBarInfo
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
    
}

//////////////////////////////////////////////////
////////// MARK: - 노티피케이션
//////////////////////////////////////////////////

// 1.
- (void)receiveData1:(NSDictionary *)dic
{
    NSLog(@"1. 연결 ");
}

// 4.
- (void)receiveData4:(NSDictionary *)dic
{
    NSLog(@"4. 연결 ");
}

// 5. 워밍업 시작
- (void)receiveData5:(NSDictionary *)dic
{
    NSLog(@"5. 워밍업 시작");
    
    if (self.flagBecome) return;
    
    self.flagNumberAnimation = NO;
    [self.timerSend5 invalidate];
    self.timerSend5 = nil;
    timerCount = 0;
    
    // 이름
    NSUInteger index = [[TIBLECBKeyfob keyfob] arrayGameInfoIndex];
    NSArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
    NSDictionary* obj = [array objectAtIndex:index];
    NSString* name = [obj valueForKey:@"Name"];
    
    // 페이징
    self.labelInfoName.text = name;
    self.labelInfoPage.text = [NSString stringWithFormat:@"%d/%d", index+1, array.count];
    
    NSInteger time = [[dic objectForKey:@"WARMINGUP_TIME"] integerValue];
    
    [self.viewBlowing setHidden:NO];
    [self.viewAnalyzing setHidden:YES];
    
    self.labelNumber.text = @"00";
    [self.arcView animateArcTo0];
    
    self.number =  0;
    float second = time;
    [self.arcView animateArcTo100:second];
    [self animateNumberTo100:second];
    
    self.labelText.text = [UtilLocal localString:@"WarmingUp"];
    self.labelText.textColor = [UIColor whiteColor];
    
    // 워밍업 95% : Breath deep
    [self performSelector:@selector(setBreathDeep) withObject:nil afterDelay:second*0.95f];
    
}

// 1-1. 워밍업 95% : Breath deep
- (void) setBreathDeep
{
    self.labelText.text = [UtilLocal localString:@"BreathDeep"];
    self.labelText.textColor = [UIColor whiteColor];
}

// 5_1. 워밍업 종료
- (void)receiveData5_1:(NSDictionary *)dic
{
    if (self.flagBecome) return;
    NSLog(@"5_1. 워밍업 종료");
}

// 6. 블로우 대기
- (void)receiveData6:(NSDictionary *)dic
{
    NSLog(@"6. 블로우 대기");
    if (self.flagBecome) return;
    
    ////////////////// 임시 //////////////
//    NSUInteger index = [[TIBLECBKeyfob keyfob] arrayGameInfoIndex];
//    if (index == 1)
//    {
//        [self receiveError6:nil];
//    }
    ////////////////// 임시 /////////////
    
    float second = 0.25f;
    //[self animateNumberTo0:second];
    labelNumber.text = @"00";
    [self.arcView animateArcTo0:second];
    [self performSelector:@selector(animateBlickText) withObject:nil afterDelay:second];
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_06];
}

// 7. 블로우 시작
- (void)receiveData7:(NSDictionary *)dic
{
    NSLog(@"7. 블로우 시작");
    
    if (self.flagBecome) return;
    
    // 백버튼 탭바 딤드
    [self.viewDimmedNavBar setHidden:NO];
    [self.tabbarVC.viewDimmed setHidden:NO];
    
    float second = 5.0f;
    [self.arcView animateArcTo100:second];
    [self animateNumberTo100:second];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagSound = [userDefaults boolForKey:@"SOUND_FLAG"];
    
    if (flagSound)
    {
        // beep 5초간 재생
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        NSURL* url = [[NSBundle mainBundle] URLForResource:@"beep-11" withExtension:@"mp3"];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        [self.audioPlayer play];
    }
    
    // 깜빡임 애니메이션 종료
    [self.labelText.layer removeAllAnimations];
    self.labelText.text = [UtilLocal localString:@"KeepBlowing"];
    self.labelText.textColor = [UIColor whiteColor];
    
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_07];
}

// 7_1. 블로우 : 카메라
- (void)receiveData7_1:(NSDictionary *)dic
{
    NSLog(@"7_1. 블로우 : 카메라");
}

// 8. 분석중 : 센서반응
- (void)receiveData8:(NSDictionary *)dic
{
    [self.audioPlayer stop];
    
    if (self.flagBecome) return;
    
    NSLog(@"8. 분석중 : 센서반응");
    [self.viewBlowing setHidden:YES];
    [self.viewAnalyzing setHidden:NO];
    [self rotateImageView];    // 애니메이션 : 로딩 360도
    
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_08];
}

// 9. 결과 : 농도 표시
- (void)receiveData9:(NSDictionary *)dic
{
    NSLog(@"9. 결과 : 농도 표시");
    if (self.flagBecome) return;
    
    NSUInteger density = [[dic objectForKey:@"density"] integerValue];
    
    [self.imageViewGuage.layer removeAllAnimations];
    
    //////////////// 임시 ///////////////
    //density = 351;
    //////////////// 임시 ///////////////
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    // 카운트
    NSInteger count = [[dic objectForKey:@"count"] integerValue];
    [keyfob.dicNavInfo setObject:[NSNumber numberWithInteger:count] forKey:@"count"];
    
    
    // 디데이
    NSString* strDate = [dic objectForKey:@"date"];
    NSDate* date = [UtilDate dateFromString:strDate];
    
    // 디데이 : 날짜 초기화 되지 않았을때
    if ([@"00000000" isEqualToString:strDate]) {
        date = [NSDate date];
    } else {
        date = [UtilDate dateFromString:strDate];
    }
    
    NSInteger gap = [UtilDate compareDate:date date2:[NSDate date]];
    gap = 365 - gap;
    [keyfob.dicNavInfo setObject:[NSNumber numberWithInteger:gap] forKey:@"gap"];
    
    // 네비바 설정 : 카운트, 디데이 정보
    [self setNavBarInfo];
    
    // 데이터
    NSArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
    NSInteger index = [[TIBLECBKeyfob keyfob] arrayGameInfoIndex];
    NSMutableDictionary* obj = [array objectAtIndex:index];
    [obj setValue:[NSNumber numberWithInteger:density] forKey:@"Density"];
    
    if (index+1 < array.count )
    {
        // 워밍업 단계로
        [self.viewBlowing setHidden:NO];
        [self.viewAnalyzing setHidden:YES];
        
        self.labelNumber.text = @"00";
        [self.arcView animateArcTo0];
        
        self.labelText.text = [UtilLocal localString:@"WarmingUp"];
        self.labelText.textColor = [UIColor whiteColor];
        
        timerCount = 0;
        self.timerSend5 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sendData5:) userInfo:nil repeats:YES];
        
    } else {
        [self performSegueWithIdentifier:@"segueShowGameResult" sender:self];
    }
    
    if (index < array.count-1)
    {
        [[TIBLECBKeyfob keyfob] setArrayGameInfoIndex:index+1];
    }
    
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_09];
}

- (void)receiveError5:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle5"];
    [self.viewAlertInfo setHidden:NO];
}
- (void)receiveError6:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle6"];
    [self.viewAlertInfo setHidden:NO];
}
- (void)receiveError7:(NSDictionary *)dic
{
    // 소리 꺼짐
    if (self.audioPlayer != nil && self.audioPlayer.playing)
    {
        [self.audioPlayer stop];
    }
    
    self.flagNumberAnimation = YES;
    
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle7"];
    [self.viewAlertInfo setHidden:NO];
}
- (void)receiveError9:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle9"];
    [self.viewAlertInfo setHidden:NO];
}
- (void)receiveError11:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Temperature"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle11"];
    [self.viewAlertInfo setHidden:NO];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트
//////////////////////////////////////////////////

- (IBAction)touchUpButtonInAlertInfo:(id)sender
{
    NSString* text = self.labelMessageInAlertInfo.text;

    if ([text isEqualToString:[UtilLocal localString:@"ErrorTitle6"]])
    {
        [self viewSetWarming];
        [self.viewAlertInfo setHidden:YES];
    }
    else if ([text isEqualToString:[UtilLocal localString:@"ErrorTitle7"]])
    {
        [self viewSetWarming];
        [self.viewAlertInfo setHidden:YES];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
- (IBAction)touchUpButtonBack:(id)sender
{
    NSUInteger index = self.navigationController.viewControllers.count - 3;
    UIViewController* vc = [self.navigationController.viewControllers objectAtIndex:index];
    [self.navigationController popToViewController:vc animated:YES];
}

//////////////////////////////////////////////////
////////// MARK: - 애니메이션
//////////////////////////////////////////////////

// 숫자 카운트 : 0 -> 100
- (void) animateNumberTo100:(float)second
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (int i = 1; i < 101; i ++) {
            
            if (!self.flagNumberAnimation) {
                
                usleep(second/103 * 1000000); // sleep in microseconds
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (self.flagNumberAnimation) {
                        labelNumber.text = @"00";
                    } else {
                        NSString* str = [NSString stringWithFormat:@"%02d", i];
                        labelNumber.text = str;
                    }
                    
                });
            }
        }
    });
}

// 숫자 카운트 : 100 -> 0
- (void) animateNumberTo0:(float)second
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (int i = 100; i >= 0; i --) {
            usleep(second/103 * 1000000); // sleep in microseconds
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString* str = [NSString stringWithFormat:@"%02d", i];
                labelNumber.text = str;
            });
        }
    });
}

// 애니메이션 : 로딩 360도
- (void)rotateImageView
{
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        imageViewGuage.transform = CGAffineTransformRotate(imageViewGuage.transform, M_PI_2);
    } completion:^(BOOL finished){
        if(finished){
            [self rotateImageView];
        }
    }];
}

// 애니메이션 : 문자 깜빡임
- (void) animateBlickText
{
    self.labelText.text = [UtilLocal localString:@"StartBlowing"];
    
    [UIView transitionWithView:self.labelText duration:0.25f options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         self.labelText.textColor = [UIColor colorWithRed:0.28f green:0.72f blue:0.84f alpha:1.0f];
     }
                    completion:^(BOOL finished)
     {
         if (finished) {
             [UIView transitionWithView:self.labelText duration:0.25f options:UIViewAnimationOptionTransitionCrossDissolve
                             animations:^
              {
                  self.labelText.textColor = [UIColor whiteColor];
              }
                             completion:^(BOOL finished)
              {
                  if (finished) {
                      [self animateBlickText];
                  }
              }];
         }
     }];
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

// 블루투스 장치 연결 해제
- (void) disconnectedDevice
{
//    NSString* s1 = [UtilLocal localString:@"DISCONNECT_TITLE"];
//    NSString* s2 = [UtilLocal localString:@"DISCONNECT_MESSAGE"];
//    NSString* s3 = [UtilLocal localString:@"Done"];
//    
//    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:s1 message:s2 delegate:self cancelButtonTitle:s3 otherButtonTitles: nil];
//    alert.alertViewStyle = UIAlertViewStyleDefault;
//    [alert show];
    
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// 블루투스 Off
- (void) statePoweredOff
{
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
