//
//  MainViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 25..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "MainViewController.h"
#import "TIBLEUtil.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize btnConnect;

@synthesize imageViewCircle1;
@synthesize imageViewCircle2;
@synthesize imageViewCircle3;
@synthesize imageViewCircle4;
@synthesize imageViewCircle5;
@synthesize imageViewStartingCircle;

@synthesize viewScanning;
@synthesize viewStarting;
@synthesize viewLoading;

@synthesize imageViewLoading;
@synthesize timerSend1;
@synthesize timerCount;

@synthesize labelCount;
@synthesize imageViewBattery;
@synthesize labelDday;

@synthesize labelScanning;
@synthesize labelScanningTitle;
@synthesize labelStartTest;
@synthesize labelStartTitle;

@synthesize alertDisconnected;

@synthesize labelInfoInAlertInfo;
@synthesize labelMessageInAlertInfo;
@synthesize labelData;
@synthesize labelDeviceName;
@synthesize buttonOkInAlertInfo;
@synthesize viewAlertInfo;

@synthesize buttonStartTest;

@synthesize labelTitleInAlertPair;
@synthesize labelMessageInAlertPair;
@synthesize buttonOKInAlertPair;
@synthesize buttonCancelInAlertPair;
@synthesize viewAlertPair;
@synthesize timerBluetoothOff;
@synthesize viewAlertPIN;
@synthesize labelMessageInAlertPIN;
@synthesize labelTitleInAlertPIN;
@synthesize fieldPinInAlertPIN;

@synthesize dicConnectingDevice;
@synthesize activeField;
@synthesize labelPinError;

@synthesize buttonCancelInAlertPIN;
@synthesize buttonPairInAlertPIN;

//////////////////////////////////////////////////
////////// MARK: - Segue
//////////////////////////////////////////////////

// 다른 뷰로 이동하기 전 실행
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    [self.imageViewLoading.layer removeAllAnimations];
//    [imageViewStartingCircle.layer removeAllAnimations];
//    [imageViewCircle5.layer removeAllAnimations];
//    [imageViewCircle4.layer removeAllAnimations];
//    [imageViewCircle3.layer removeAllAnimations];
//    [imageViewCircle2.layer removeAllAnimations];
//    [imageViewCircle1.layer removeAllAnimations];
}

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    // 블루투스 연결 매니저 초기화
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    if (keyfob.CM == nil || keyfob.CM.state == CBCentralManagerStatePoweredOff)
    {
        [keyfob controlSetup];
    }
    
    [self viewBecome];
    
    // 노티 등록
    NSNotificationCenter* noti = [NSNotificationCenter defaultCenter];
    [noti addObserver:self selector:@selector(viewBecome) name:UIApplicationDidBecomeActiveNotification object:nil];
    [noti addObserver:self selector:@selector(viewResign) name:UIApplicationWillResignActiveNotification object:nil];
    
    self.labelTitleInAlertPIN.text = [UtilLocal localString:@"labelTitleInAlertPIN"];
    self.labelMessageInAlertPIN.text = [UtilLocal localString:@"labelMessageInAlertPIN"];
    self.labelPinError.text = [UtilLocal localString:@"labelPinError"];
    
    [self.buttonCancelInAlertPIN setTitle:[UtilLocal localString:@"Cancel"] forState:UIControlStateNormal];
    [self.buttonPairInAlertPIN setTitle:[UtilLocal localString:@"Pair"] forState:UIControlStateNormal];
    
    ///////////////// 임시 /////////////////
    //[self setStateStarting];
    ///////////////// 임시 /////////////////
    
    self.timerBluetoothOff = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self selector:@selector(initBluetooth) userInfo:nil repeats:YES];
    
    // 탭바 딤드 2019.01.07
    NSDictionary *notiDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"YES",@"dimmed", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabbarDimmed" object:nil userInfo:notiDic];
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = nil;
    [keyfob performSelector:@selector(stopScanning) withObject:nil afterDelay:0.5f];
    
    // 노티 해제
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.timerBluetoothOff invalidate];
    self.timerBluetoothOff = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    self.fieldPinInAlertPIN.delegate = self;

}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - UI 변경
//////////////////////////////////////////////////

// 블루투스 객체 초기화
- (void) initBluetooth
{
    // 블루투스 연결 매니저 초기화
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    NSLog(@"블루투스 체크");
    if (keyfob.CM.state == CBCentralManagerStatePoweredOff)
    {
        NSLog(@"블루투스 꺼졌어");
        [keyfob controlSetup];
    }
}

// UIApplicationWillResignActiveNotification
- (void) viewBecome
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = self;
    
    if (keyfob.activePeripheral == nil || keyfob.activePeripheral.state == CBPeripheralStateDisconnected)
    {
        [self setStateScanning];
    } else {
        [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_01];
        [self setStateStarting];
    }
    
    // localizable
    self.labelScanning.text = [UtilLocal localString:@"SCANNING"];
    self.labelScanningTitle.text = [UtilLocal localString:@"SCANNING_TITLE"];
    self.labelStartTest.text = [UtilLocal localString:@"START_TEST"];
    self.labelStartTitle.text = [UtilLocal localString:@"START_TITLE"];
    
    // 프랑스어
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger selectedIndex = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    
    if (selectedIndex == 2) {
        [self.labelStartTest setNumberOfLines:2];
    } else {
        [self.labelStartTest setNumberOfLines:1];
    }
    
    // localizable : AlertInfo
    [self.buttonOkInAlertInfo setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.viewAlertInfo setHidden:YES];
    
    // AlertPair
    [self.labelTitleInAlertPair setText:[UtilLocal localString:@"Info"]];
    [self.labelMessageInAlertPair setText:[UtilLocal localString:@"BlueToothFindMessage"]];
    [self.buttonOKInAlertPair setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.buttonCancelInAlertPair setTitle:[UtilLocal localString:@"Cancel"] forState:UIControlStateNormal];
    [self.viewAlertPair setHidden:YES];
    [self.viewAlertPIN setHidden:YES];
    [self.labelPinError setHidden:YES];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:2];
            [tabbarVC setSelectedButtonIndex:2];
        }
    }
}

// UIApplicationDidBecomeActiveNotification
- (void) viewResign
{
    [imageViewStartingCircle.layer removeAllAnimations];
    [imageViewCircle5.layer removeAllAnimations];
    [imageViewCircle4.layer removeAllAnimations];
    [imageViewCircle3.layer removeAllAnimations];
    [imageViewCircle2.layer removeAllAnimations];
    [imageViewCircle1.layer removeAllAnimations];
}

- (void) showViewAlertPIN
{
    [self.viewAlertPIN setHidden:NO];
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

- (void)reFindDevice
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob stopScanning];
    if (keyfob.CM.state == CBCentralManagerStatePoweredOn){
        [self.viewAlertPair setHidden:NO];
    }
}

// 블루투스 장치 연결시도
- (void)connecting:(NSDictionary *)dic
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    //[keyfob performSelector:@selector(stopScanning) withObject:nil afterDelay:0.5f];
    [self.viewAlertPIN setHidden:NO];
    self.dicConnectingDevice = dic;
    
    NSLog(@"connecting UUID :%@", [dic objectForKey:@"UUID"]);
    
    
    //연결
//    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
//    [keyfob connectDirectDevice:dic];
}

// 블루투스 장치 연결 성공
- (void)connectedDevice:(NSDictionary *)dic
{
    [self.viewAlertPIN setHidden:YES];
    
    // 버튼 상태 변경
    [self setStateStarting];
    
    // 데이터 보내기 : ALCOFIND_DATA_1
    // 5번 보내고 받을때 까지 로딩
    timerCount = 0;
    self.timerSend1 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sendData1:) userInfo:nil repeats:NO];
    
    // 4번 받을때 까지 로딩
    [self.viewLoading setHidden:NO];
    [self performSelector:@selector(animateLoading) withObject:nil afterDelay:0.5f];
}

// 블루투스 장치 연결 해제
- (void) disconnectedDevice
{
    // 버튼 상태 변경
    //[self.btnConnect setTitle:@"Connect" forState:UIControlStateNormal];
    
    // 로딩 제거
    [self.viewLoading setHidden:YES];
    [self.imageViewLoading.layer removeAllAnimations];
    
    [self setStateScanning];
    
//    NSString* s1 = [UtilLocal localString:@"DISCONNECT_TITLE"];
//    NSString* s2 = [UtilLocal localString:@"DISCONNECT_MESSAGE"];
//    NSString* s3 = [UtilLocal localString:@"Done"];
//    
//    
//    if (!self.alertDisconnected.isVisible)
//    {
//        self.alertDisconnected =[[UIAlertView alloc ] initWithTitle:s1 message:s2 delegate:self cancelButtonTitle:s3 otherButtonTitles: nil];
//        self.alertDisconnected.alertViewStyle = UIAlertViewStyleDefault;
//        [self.alertDisconnected show];
//    }
}

- (void) sendData1:(NSTimer *)timer
{
    NSLog(@"1번 타이머 ");
    timerCount++;
    if (timerCount <=5)
    {
        [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_01];
        NSLog(@"1번 보내기");
    } else {
        NSLog(@"1번 타이머 종료");
        [self.timerSend1 invalidate];
        self.timerSend1 = nil;
        timerCount = 0;
    }
}

- (void)receiveData1:(NSDictionary*)dic
{
    [self.timerSend1 invalidate];
    self.timerSend1 = nil;
    timerCount = 0;
    
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 배터리
    NSInteger battery = [[dic objectForKey:@"battery"] integerValue];
    [keyfob.dicNavInfo setObject:[NSNumber numberWithInteger:battery] forKey:@"battery"];
    
    UIImage* image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_battery_%d", battery]];
    self.imageViewBattery.image = image;
    
    // 배터리 : 0 일경우 : 깜빡임
    if (battery <= 1) {
        
    }
    
    // 카운트
    NSInteger count = [[dic objectForKey:@"count"] integerValue];
    [keyfob.dicNavInfo setObject:[NSNumber numberWithInteger:count] forKey:@"count"];
    /////////// 임시 ///////////////
//    if ([[dic objectForKey:@"date"] isEqualToString:@"20151020"]) {
//      count = 450;
//    }
    /////////// 임시 ///////////////
    
    self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    
    // 카운트 : 450 이상일 경우 : 깜빡임
    if (count >= 450) {
        [self animateBlickText:self.labelCount];
    }
    // 카운트 : 500 이상일 경우 :
    if (count >= 500) {
        [self animateBlickText:self.labelCount];
        self.labelInfoInAlertInfo.text = [UtilLocal localString:@"Info"];
        self.labelMessageInAlertInfo.text = [UtilLocal localString:@"Calibration"];
        [self.viewAlertInfo setHidden:NO];
    }
    
    // 디데이
    NSString* strDate = [dic objectForKey:@"date"];
    NSDate* date = [UtilDate dateFromString:strDate];
    
    // 디데이 : 날짜 초기화 되지 않았을때
    if ([@"00000000" isEqualToString:strDate]) {
        date = [NSDate date];
    } else {
        date = [UtilDate dateFromString:strDate];
    }
    
    NSInteger gap = [UtilDate compareDate:date date2:[NSDate date]];
    gap = 365 - gap;
    self.gap = gap;
    /////////// 임시 ///////////////
    //self.gap = -100;
    /////////// 임시 ///////////////
    [keyfob.dicNavInfo setObject:[NSNumber numberWithInteger:self.gap] forKey:@"gap"];
    
    if (self.gap >= 0){
        self.labelDday.text = [NSString stringWithFormat:@"D-%d", self.gap];
    } else {
        NSInteger num = fabsf(self.gap);
        self.labelDday.text = [NSString stringWithFormat:@"D+%d", num];
    }
    
    
    // 디데이 : 20 이하일 경우 : 깜빡임
    if (gap <= 20) {
        [self animateBlickText:self.labelDday];
    }
    // 디데이 : 0 이하일 경우 :
    if (gap <= 0) {
        [self animateBlickText:self.labelDday];
        self.labelInfoInAlertInfo.text = [UtilLocal localString:@"Info"];
        self.labelMessageInAlertInfo.text = [UtilLocal localString:@"Calibration"];
        [self.viewAlertInfo setHidden:NO];
    }
}

- (void)receiveData4:(NSDictionary*)dic
{
    NSArray* array = [dic objectForKey:@"array"];

    for (int i=0; i<array.count; i++)
    {
        NSUInteger density = [[array objectAtIndex:i] integerValue];
        [self addCoreDataDensity:density];
    }
    
    [self.viewLoading setHidden:YES];
    [self.imageViewLoading.layer removeAllAnimations];
}

- (void)receiveData10:(NSDictionary*)dic
{
    [self touchUpButtonStart:nil];
}

- (void)receiveError2:(NSDictionary *)dic
{
    self.labelInfoInAlertInfo.text = [UtilLocal localString:@"Error"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle2"];
    [self.viewAlertInfo setHidden:NO];
    [self.labelStartTest setEnabled:NO];
    [self.buttonStartTest setEnabled:NO];
}

// 블루투스 Off
- (void) statePoweredOff
{
}

//////////////////////////////////////////////////
////////// MARK: - 기능
//////////////////////////////////////////////////

- (void) addCoreDataDensity:(NSInteger)value
{
    // 정보 저장
    NSMutableDictionary* dic =[NSMutableDictionary dictionary];
    BOOL breathalyzer = YES;
    
    // 데이터 디비에 저장
    NSDate* date = [NSDate date];
    NSString* strDate = [UtilDate stringFormat:@"yyyy-MM-dd" date:date];
    
    [dic setValue:@"ME" forKey:@"type"];
    [dic setValue:[NSNumber numberWithInteger:value] forKey:@"value"];
    [dic setValue:date forKey:@"reg_date"];
    [dic setValue:strDate forKey:@"group_date"];
    [dic setValue:[NSNumber numberWithBool:breathalyzer] forKey:@"breathalyzer"];
    
    [DataDensity addObject:dic];
}

- (void) setStateScanning
{
    [self.viewScanning setHidden:NO];
    [self.viewStarting setHidden:YES];
    
    [self.labelStartTest setEnabled:YES];
    [self.buttonStartTest setEnabled:YES];
    
    labelCount.textColor = [UIColor whiteColor];
    labelDday.textColor = [UIColor whiteColor];
    [labelCount.layer removeAllAnimations];
    [labelDday.layer removeAllAnimations];
    [imageViewStartingCircle.layer removeAllAnimations];
    [imageViewCircle5.layer removeAllAnimations];
    [imageViewCircle4.layer removeAllAnimations];
    [imageViewCircle3.layer removeAllAnimations];
    [imageViewCircle2.layer removeAllAnimations];
    [imageViewCircle1.layer removeAllAnimations];
    
//    [self rotateImageView];    // 애니메이션 : 로딩 360도
//    [self loadingCircle];    // 애니메이션 : 로딩 원 이미지
    
    [self performSelector:@selector(rotateImageView) withObject:nil afterDelay:0.2f];
    [self performSelector:@selector(loadingCircle) withObject:nil afterDelay:0.4f];

    [self.viewLoading setHidden:YES];
    [self.imageViewLoading.layer removeAllAnimations];
    
    // 상태 초기화
    self.labelCount.text = @"No. 0";
    self.labelDday.text = @"D-0";
    self.gap = 0;
    self.imageViewBattery.image = [UIImage imageNamed:@"icon_battery"];
    
    [[TIBLECBKeyfob keyfob] performSelector:@selector(findDevices) withObject:nil afterDelay:0.5f];
}

- (void) setStateStarting
{
    [self.viewScanning setHidden:YES];
    [self.viewStarting setHidden:NO];
    
    labelCount.textColor = [UIColor whiteColor];
    labelDday.textColor = [UIColor whiteColor];
    [labelCount.layer removeAllAnimations];
    [labelDday.layer removeAllAnimations];
    [imageViewStartingCircle.layer removeAllAnimations];
    [imageViewCircle5.layer removeAllAnimations];
    [imageViewCircle4.layer removeAllAnimations];
    [imageViewCircle3.layer removeAllAnimations];
    [imageViewCircle2.layer removeAllAnimations];
    [imageViewCircle1.layer removeAllAnimations];
    
    [self animationLoadingStaring];    // 애니메이션 : 로딩 360도
}

//////////////////////////////////////////////////
////////// MARK: - 애니메이션
//////////////////////////////////////////////////

// 애니메이션 : 로딩중 원 돌기
- (void) animateLoading
{
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        self.imageViewLoading.transform = CGAffineTransformRotate(self.imageViewLoading.transform, M_PI_2);
    } completion:^(BOOL finished){
        if(finished){
            [self animateLoading];
        }
    }];
}
// 애니메이션 : 스타트 테스트 원돌기
- (void) animationLoadingStaring
{
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        imageViewStartingCircle.transform = CGAffineTransformRotate(imageViewStartingCircle.transform, M_PI_2);
    } completion:^(BOOL finished){
        if(finished){
            [self animationLoadingStaring];
        }
    }];
}

// 애니메이션 : 로딩 원 이미지
- (void)loadingCircle
{
    [UIView animateWithDuration:0.6f delay:0.2f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle4 setAlpha:1.0f];} completion:^(BOOL finished){}];
    [UIView animateWithDuration:0.6f delay:0.6f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle3 setAlpha:1.0f];} completion:^(BOOL finished){}];
    [UIView animateWithDuration:0.6f delay:1.0f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle2 setAlpha:1.0f];} completion:^(BOOL finished){}];
    [UIView animateWithDuration:0.6f delay:1.4f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle1 setAlpha:1.0f];} completion:^(BOOL finished){
        if (finished) {
            [self loadingCircleFadeOut];
        }
    }];
}

// 애니메이션 : 로딩 원 이미지 사라짐
- (void)loadingCircleFadeOut
{
    [UIView animateWithDuration:0.6f delay:0.2f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle1 setAlpha:0.3f];} completion:^(BOOL finished){}];
    [UIView animateWithDuration:0.6f delay:0.6f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle2 setAlpha:0.3f];} completion:^(BOOL finished){}];
    [UIView animateWithDuration:0.6f delay:1.0f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle3 setAlpha:0.3f];} completion:^(BOOL finished){}];
    [UIView animateWithDuration:0.6f delay:1.4f options:UIViewAnimationOptionCurveEaseIn animations:^{ [imageViewCircle4 setAlpha:0.3f];} completion:^(BOOL finished){
        if (finished) {
            [self loadingCircle];
        }
    }];
}

// 애니메이션 : 스캐닝 원 돌기
- (void)rotateImageView
{
    [UIView animateWithDuration:0.375f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        imageViewCircle5.transform = CGAffineTransformRotate(imageViewCircle5.transform, M_PI_2);
    } completion:^(BOOL finished){
        if(finished){
            [self rotateImageView];
        }
        
    }];
}

// 애니메이션 : 문자 깜빡임
- (void) animateBlickText:(UILabel*)view
{
    [UIView transitionWithView:view duration:0.25f options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
    {
        view.textColor = [UIColor clearColor];

    }
                    completion:^(BOOL finished)
     {
         if (finished) {
             [UIView transitionWithView:view duration:0.25f options:UIViewAnimationOptionTransitionCrossDissolve
                             animations:^
              {
                  view.textColor = [UIColor whiteColor];
              }
                             completion:^(BOOL finished)
              {
                  if (finished) {
                      [self animateBlickText:view];
                  }
              }];
         }
     }];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트:
//////////////////////////////////////////////////

- (IBAction)resignOnTop:(id)sender
{
    [self.fieldPinInAlertPIN resignFirstResponder];
}

- (IBAction)touchUpButtonStart:(id)sender
{
    [self performSegueWithIdentifier:@"showTestLevel1" sender:self];
}

- (IBAction)touchUpButtonCloseInAlertINfo:(id)sender
{
    NSString* text = self.labelMessageInAlertInfo.text;
    
    if ([text isEqualToString:[UtilLocal localString:@"ErrorTitle2"]])
    {
        [self.viewAlertInfo setHidden:YES];
        TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
        [keyfob disConnectNotiPeripheral];
    }
    else
    {
        [self.viewAlertInfo setHidden:YES];
    }
}

- (IBAction)touchUpButtonOkInAlertINfo:(id)sender
{
    NSString* text = self.labelMessageInAlertInfo.text;
    
    if ([text isEqualToString:[UtilLocal localString:@"ErrorTitle2"]])
    {
        [self.viewAlertInfo setHidden:YES];
        TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
        [keyfob disConnectNotiPeripheral];
    }
    else
    {
        [self.viewAlertInfo setHidden:YES];
    }
}

// AlertPair
- (IBAction)touchUpButtonOkInAlertPair:(id)sender
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob findBLEPeripherals2];
    [self.viewAlertPair setHidden:YES];
}
- (IBAction)touchUpButtonCancelInAlertPair:(id)sender
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob findBLEPeripherals:5];
    [self.viewAlertPair setHidden:YES];
}
- (IBAction)touchUpButtonCloseInAlertPair:(id)sender
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob findBLEPeripherals:5];
    [self.viewAlertPair setHidden:YES];
}

// AlertPIN
- (IBAction)touchUpButtonPairInAlertPIN:(id)sender
{
    if ([self.fieldPinInAlertPIN.text isEqualToString:@"000000"])
    {
        NSLog(@"PIN 번호 맞음");
        TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
        [keyfob connectDirectDevice:self.dicConnectingDevice];
        [self performSelector:@selector(checkConnected) withObject:nil afterDelay:5.0f];
        
        [self.viewAlertPIN setHidden:YES];
        self.fieldPinInAlertPIN.text = @"";
        [self.labelPinError setHidden:YES];
    }
    else {
        NSLog(@"PIN 번호 틀림");
        [self.labelPinError setHidden:NO];
    }
    self.fieldPinInAlertPIN.text = @"";
}
- (IBAction)touchUpButtonCancelInAlertPIN:(id)sender
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob findBLEPeripherals2];
    
    [self.viewAlertPIN setHidden:YES];
    self.fieldPinInAlertPIN.text = @"";
    [self.labelPinError setHidden:YES];
}
- (IBAction)touchUpButtonCloseInAlertPIN:(id)sender
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob findBLEPeripherals2];
    
    [self.viewAlertPIN setHidden:YES];
    self.fieldPinInAlertPIN.text = @"";
    [self.labelPinError setHidden:YES];
}

- (void) checkConnected
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    if (keyfob.activePeripheral.state != CBPeripheralStateConnected)
    {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"DEVICE_UUID"];
        
        keyfob.activePeripheral = nil;
        keyfob.activeAdvertisement = nil;
        [keyfob findBLEPeripherals2];
    }
}

//////////////////////////////////////////////////
////////// MARK: - UITextFieldDelegate
//////////////////////////////////////////////////


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self touchUpButtonPairInAlertPIN:nil];
    [self.fieldPinInAlertPIN resignFirstResponder];
    return YES;
}

@end
