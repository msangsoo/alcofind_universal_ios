//
//  TIBLECBKeyfob.m
//  TI-BLE-Demo
//
//  Created by Ole Andreas Torvmark on 10/31/11.
//  Copyright (c) 2011 ST alliance AS. All rights reserved.
//

#import "TIBLECBKeyfob.h"

static TIBLECBKeyfob* sharedInstance = nil;

@implementation TIBLECBKeyfob

@synthesize CM;
@synthesize peripherals;
@synthesize activePeripheral;
@synthesize activeAdvertisement;
@synthesize boolBlowFlag;
@synthesize boolResultFlag;
@synthesize boolDeviceFlag;
@synthesize boolBatteryFlag;
@synthesize arrayGameInfo;
@synthesize arrayGameInfoIndex;
@synthesize dicTestInfo;
@synthesize data4Length;
@synthesize data4Index;
@synthesize delegate;
@synthesize delegateTabbar;
@synthesize delegateAlert;
@synthesize timer;

//////////////////////////////////////////////////
////////// MARK: - 초기화
//////////////////////////////////////////////////

+ (TIBLECBKeyfob*) keyfob
{
    if(!sharedInstance) {
        sharedInstance = [[TIBLECBKeyfob alloc] init];
    }
    return sharedInstance;
}

//////////////////////////////////////////////////
////////// MARK: - 데이터 보내기 받기
//////////////////////////////////////////////////

// 블루투스 장치에 데이터 보내기 : 0xDA20
- (void)sendData:(Byte)b
{
    // 블루투스 연결 되었을 때
    if (activePeripheral != nil || activePeripheral.state == CBPeripheralStateConnected)
    {
        // 유저 데이터
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        
        // 날짜
        NSString* date = [UtilDate stringFormat:@"yyyyMMddHHmm"];
        NSInteger year =  CFSwapInt16HostToBig([[date substringWithRange:NSMakeRange(0, 4)] integerValue]);
        NSInteger month =  [[date substringWithRange:NSMakeRange(4, 2)] integerValue];
        NSInteger day =  [[date substringWithRange:NSMakeRange(6, 2)] integerValue];
        NSInteger hour =  [[date substringWithRange:NSMakeRange(8, 2)] integerValue];
        NSInteger min =  [[date substringWithRange:NSMakeRange(10, 2)] integerValue];
        
        // Alarm limit
        NSUInteger limit = [userDefaults integerForKey:@"ALARM_LIMIT_VALUE_LEVEL"];
        
        NSMutableData* data = [NSMutableData dataWithCapacity:8];
        [data appendBytes:&b length:1];    //APP 상태
        [data appendBytes:&year length:2];   // 년
        [data appendBytes:&month length:1];   // 월
        [data appendBytes:&day length:1];   // 일
        [data appendBytes:&hour length:1];   // 시
        [data appendBytes:&min length:1];   // 분
        [data appendBytes:&limit length:1];// limit
        
        [self writeValue:0xFFE0 characteristicUUID:0xDA20 p:[self activePeripheral] data:data];
    }
}

// 블루투스 장치에 데이터 보내기 : 0xDA04
- (void)sendData:(Byte)b1 b2:(Byte)b2
{
    if (activePeripheral != nil || activePeripheral.state == CBPeripheralStateConnected)
    {
        Byte arrayByte[] = {b1, b2};
        NSData *data = [[NSData alloc] initWithBytes:arrayByte length:2];
        [self writeValue:0xFFE0 characteristicUUID:0xDA04 p:[self activePeripheral] data:data];
    }
}

/*!
 *  @method writeValue:
 *
 *  @param serviceUUID Service UUID to write to (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to write to (e.g. 0x2401)
 *  @param data Data to write to peripheral
 *  @param p CBPeripheral to write to
 *
 *  @discussion Main routine for writeValue request, writes without feedback. It converts integer into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service. 
 *  If this is found, value is written. If not nothing is done.
 *
 */

-(void) writeValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p data:(NSData *)data
{
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        printf("Could not find service with UUID %s on peripheral with UUID \r\n",[self CBUUIDToString:su]);
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID \r\n",[self CBUUIDToString:cu],[self CBUUIDToString:su]);
        return;
    }
    [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
    
    NSLog(@"▲ 데이터 : %@ %@", characteristic, data);
}


/*!
 *  @method readValue:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for read value request. It converts integers into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service. 
 *  If this is found, the read value is started. When value is read the didUpdateValueForCharacteristic 
 *  routine is called.
 *
 *  @see didUpdateValueForCharacteristic
 */

-(void) readValue: (int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p
{
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        printf("Could not find service with UUID %s on peripheral with UUID \r\n",[self CBUUIDToString:su]);
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID \r\n",[self CBUUIDToString:cu],[self CBUUIDToString:su]);
        return;
    }  
    [p readValueForCharacteristic:characteristic];
}


/*!
 *  @method notification:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for enabling and disabling notification services. It converts integers 
 *  into CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service. 
 *  If this is found, the notfication is set. 
 *
 */
-(void) notification:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p on:(BOOL)on
{
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        NSLog(@"실패 : service UUID %s",[self CBUUIDToString:su]);
        return;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        NSLog(@"실패 : characteristic UUID :%s on service UUID :%s",[self CBUUIDToString:cu],[self CBUUIDToString:su]);
        return;
    }
    [p setNotifyValue:on forCharacteristic:characteristic];
    NSLog(@"성공 : characteristic UUID :%s on service UUID :%s",[self CBUUIDToString:cu],[self CBUUIDToString:su]);
}


/*!
 *  @method swap:
 *
 *  @param s Uint16 value to byteswap
 *
 *  @discussion swap byteswaps a UInt16 
 *
 *  @return Byteswapped UInt16
 */

-(UInt16) swap:(UInt16)s {
    UInt16 temp = s << 8;
    temp |= (s >> 8);
    return temp;
}

/*!
 *  @method controlSetup:
 *
 *  @param s Not used
 *
 *  @return Allways 0 (Success)
 *  
 *  @discussion controlSetup enables CoreBluetooths Central Manager and sets delegate to TIBLECBKeyfob class 
 *
 */
- (int) controlSetup
{   
    self.CM = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue() options:@{CBCentralManagerOptionShowPowerAlertKey: @(YES)}];
    self.arrayGameInfo = [NSMutableArray array];
    return 0;
}

/*!
 *  @method findBLEPeripherals:
 *
 *  @param timeout timeout in seconds to search for BLE peripherals
 *
 *  @return 0 (Success), -1 (Fault)
 *  
 *  @discussion findBLEPeripherals searches for BLE peripherals and sets a timeout when scanning is stopped
 *
 */

- (int) findDevices
{
    return [self findBLEPeripherals:5];
}

- (int) findBLEPeripherals:(int) timeout
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString* DEVICE_UUID = [userDefaults valueForKey:@"DEVICE_UUID"];
    NSLog(@"디바이스 첫 검색 %@", DEVICE_UUID);
    
    [self stopScanning];
    
    // 이전에 연결됐었던 기기 없는경우
    if ([@"" isEqualToString:DEVICE_UUID])
    {
        [self findBLEPeripherals];
    }
    // 이전에 연결됐었던 기기 먼저 5초동안 연결
    else
    {
        
        self.boolDeviceFlag = true;
        [self findBLEPeripherals];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:delegate selector:@selector(reFindDevice) userInfo:nil repeats:NO];
    }
    
    return 0;
}

- (int) findBLEPeripherals
{
    if (self->CM.state != CBCentralManagerStatePoweredOn)
    {
        printf("CoreBluetooth not correctly initialized !\r\n");
        return -1;
    }
    
    // 디바이스 검색
    
//    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], CBCentralManagerOptionShowPowerAlertKey, nil];
    
    [self.CM scanForPeripheralsWithServices:nil options:0]; // Start scanning
    return 0;
}


- (void) findBLEPeripherals2
{
    NSLog(@"디바이스 첫 검색 실패 ");
    
    self.boolDeviceFlag = false;
    [self stopScanning];
    
    if (self.activePeripheral.state == CBPeripheralStateDisconnected)
    {
        NSLog(@"디바이스 두번째 검색");
        [self findBLEPeripherals];
    }
}


/*!
 *  @method connectPeripheral:
 *
 *  @param p Peripheral to connect to
 *
 *  @discussion connectPeripheral connects to a given peripheral and sets the activePeripheral property of TIBLECBKeyfob.
 *
 */

// 블투투스 장치 연결
- (void) connectDirectDevice:(NSDictionary*)dic
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[dic valueForKey:@"UUID"] forKey:@"DEVICE_UUID"];
    
    activePeripheral = [dic objectForKey:@"PERIPHERAL"];
    activeAdvertisement = [dic objectForKey:@"ADVERTISEMENT"];
    
    activePeripheral.delegate = self;
    [CM connectPeripheral:activePeripheral options:@{CBConnectPeripheralOptionNotifyOnConnectionKey: @NO,
                                                     CBConnectPeripheralOptionNotifyOnDisconnectionKey: @NO,
                                                     CBConnectPeripheralOptionNotifyOnNotificationKey: @NO}];
    
    //@"Connecting.."
}

// 블투투스 장치 해제
- (void) disConnectNotiPeripheral
{
    
    [CM cancelPeripheralConnection:self.activePeripheral];
    activePeripheral = nil;
    activeAdvertisement = nil;
    
    
    BOOL flag = [self.delegate respondsToSelector:@selector(disconnectedDevice)];
    if  (flag)  [self.delegate disconnectedDevice];
    
    BOOL flag2 = [self.delegateTabbar respondsToSelector:@selector(disconnectedDevice)];
    if  (flag2)  [self.delegateTabbar disconnectedDevice];
    
    self.dicNavInfo = nil;
    
    self.boolBatteryFlag = NO;

}



//////////////////////////////////////////////////
////////// MARK: - CBCentralManagerDelegate protocol
//////////////////////////////////////////////////

// CentralManager 상태 변경시
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    //NSLog(@"central %@", central);
    printf("상태:Status of CoreBluetooth central manager changed %d (%s)\r\n",central.state,[self centralManagerStateToString:central.state]);
    
    // 블루투스 Off
    if (central.state == CBCentralManagerStatePoweredOff)
    {
        NSLog(@"상태:꺼짐");
        
        BOOL flag2 = [self.delegate respondsToSelector:@selector(statePoweredOff)];
        if  (flag2)  [self.delegate statePoweredOff];
    }
}

// 블루투스 장치 찾았을때
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {

    // 이름에 Keyfob 포함인것 찾기
    //if (peripheral.name != NULL && [peripheral.name rangeOfString:@"Keyfob"].location != NSNotFound) {
        
        NSString* deviceName = [advertisementData valueForKey:@"kCBAdvDataLocalName"];
        if (deviceName != NULL)
        {
            NSMutableDictionary* dic = [NSMutableDictionary dictionary];
            [dic setObject:peripheral forKey:@"PERIPHERAL"];
            [dic setObject:peripheral.name forKey:@"DEVICE_NAME"];
            [dic setObject:peripheral.identifier.UUIDString forKey:@"UUID"];
            [dic setObject:advertisementData forKey:@"ADVERTISEMENT"];
            [dic setObject:[advertisementData valueForKey:@"kCBAdvDataLocalName"] forKey:@"NAME"];
            
            
            NSString* deviceName = [advertisementData valueForKey:@"kCBAdvDataLocalName"];
            
//            NSLog(@"블루투스 장치 찾았을때");
//            NSLog(@"기기 기본 이름: %@", peripheral.name);
//            NSLog(@"기기 기본 UUID: %@", peripheral.identifier.UUIDString);
//            NSLog(@"기기 설정 이름: %@", deviceName);
            
            // 디바이스 찾을때
            //ALCOFIND EN
            //if ([@"ALCOFIND EN" isEqualToString:deviceName] || [@"AFM 5" isEqualToString:deviceName]) {
            
            NSLog(@"deviceName = %@", deviceName);
            if ([deviceName isEqualToString:@"iThylo15"] || [deviceName isEqualToString:@"ALCOFIND EN"] || [deviceName isEqualToString:@"ALCOFIND AUS"] || [deviceName isEqualToString:@"ALCOFIND AU"] || [deviceName isEqualToString:@"ALCOFIND"] || [deviceName isEqualToString:@"ALCOFIND SVR"]) {
                // 유저 데이터
                NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
                
//                //1812 ko
//                if ([deviceName isEqualToString:@"iThylo15"] || [deviceName isEqualToString:@"ALCOFIND EN"]) {
//                    [userDefaults setObject:@"EN" forKey:@"DEVICE_TYPE"];
//                }else if ([deviceName isEqualToString:@"ALCOFIND AUS"] || [deviceName isEqualToString:@"ALCOFIND AU"]) {
//                    [userDefaults setObject:@"AU" forKey:@"DEVICE_TYPE"];
//                    [userDefaults setInteger:0 forKey:@"UNIT_SELECTED"];
//
//                }else {
//                    [userDefaults setObject:@"ST" forKey:@"DEVICE_TYPE"];
//                }
                [userDefaults synchronize];
                
                NSString* DEVICE_UUID = [userDefaults valueForKey:@"DEVICE_UUID"];
                NSString* UUID = [dic valueForKey:@"UUID"] ;
                
                // 이전에 접속했던 디바이스 연결
                if (self.boolDeviceFlag)
                {
                    if ([UUID isEqualToString:DEVICE_UUID])
                    {
                        self.boolBatteryFlag = NO;
                        [self connectDirectDevice:dic];
                        [userDefaults setObject:[dic valueForKey:@"UUID"] forKey:@"DEVICE_UUID"];
                        
                        [self.timer invalidate];
                        self.timer = nil;
                        
                        NSLog(@"이전에 접속했던 디바이스 연결");
                    }
                }
                // 일반 디바이스 연결
                else
                {
                    NSLog(@"일반 디바이스 연결");
                    self.boolBatteryFlag = NO;
                    
                    if ([UUID isEqualToString:DEVICE_UUID])
                    {
                        [self connectDirectDevice:dic];
                    } else {
                        [delegate connecting:dic];
                    }
                }
            }
        }
        
//    } else {
//        //NSLog(@"이름 다른거 : %@", peripheral.name);
//    }
    
    
}

// 블루투스 장치 연결 콜백
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"블루투스 장치 연결");
    self.activePeripheral = peripheral;
    NSLog(@"deviceName = %@",peripheral.name);
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    //1812 ko
    if ([peripheral.name isEqualToString:@"iThylo15"] || [peripheral.name isEqualToString:@"ALCOFIND EN"]) {
        [userDefaults setObject:@"EN" forKey:@"DEVICE_TYPE"];
    }else if ([peripheral.name isEqualToString:@"ALCOFIND AUS"] || [peripheral.name isEqualToString:@"ALCOFIND AU"]) {
        [userDefaults setObject:@"AU" forKey:@"DEVICE_TYPE"];
        [userDefaults setInteger:0 forKey:@"UNIT_SELECTED"];
        
    } else if ([peripheral.name isEqualToString:@"ALCOFIND SVR"]){
        [userDefaults setObject:@"ST_SVR" forKey:@"DEVICE_TYPE"];
    } else {
        [userDefaults setObject:@"ST" forKey:@"DEVICE_TYPE"];
    }
    [userDefaults synchronize];
    
//    [self.activePeripheral discoverServices:nil]; // 서비스 아이디 검색 (모두)
    [self.activePeripheral discoverServices:@[[CBUUID UUIDWithString:@"FFE0"]]]; // 서비스 아이디 검색 (설정값만)
    [central stopScan];
    
    self.boolBlowFlag = NO;
    self.boolResultFlag = NO;
}

// 블루투스 장치 해제 콜백
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"블루투스 장치 연결 해제");
    ////////////////// 임시 /////////////////////
//    {
//        BOOL flag = [self.delegateAlert respondsToSelector:@selector(receiveError1:)];
//        if  (flag)  [self.delegateAlert receiveError1:nil];
//    }
    ////////////////// 임시 /////////////////////
 
    BOOL flag = [self.delegate respondsToSelector:@selector(disconnectedDevice)];
    if  (flag && !self.boolBatteryFlag)  [self.delegate disconnectedDevice];
    
    BOOL flag2 = [self.delegateTabbar respondsToSelector:@selector(disconnectedDevice)];
    if  (flag2)  [self.delegateTabbar disconnectedDevice];
    
    self.dicNavInfo = nil;
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"블루투스 장치 연결 실패 ");
}


- (void)cleanup {
    
    
    // See if we are subscribed to a characteristic on the peripheral
    if (activePeripheral.services != nil) {
        for (CBService *service in activePeripheral.services) {
            if (service.characteristics != nil) {
                for (CBCharacteristic *characteristic in service.characteristics) {
//                    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
                        if (characteristic.isNotifying) {
                            [activePeripheral setNotifyValue:NO forCharacteristic:characteristic];
                            return;
                        }
//                    }
                }
            }
        }
    }
    
    [CM cancelPeripheralConnection:activePeripheral];
}


//////////////////////////////////////////////////
////////// MARK: - CBPeripheralDelegate protocol
//////////////////////////////////////////////////

/*
 *  @method didDiscoverCharacteristicsForService
 *
 *  @param peripheral Pheripheral that got updated
 *  @param service Service that characteristics where found on
 *  @error error Error message if something went wrong
 *
 *  @discussion didDiscoverCharacteristicsForService is called when CoreBluetooth has discovered 
 *  characteristics on a service, on a peripheral after the discoverCharacteristics routine has been called on the service
 *
 */

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (!error) {
        printf("성공 : service UUID : %s found\r",[self CBUUIDToString:service.UUID]);

        // UUID 등록
        for(int i=0; i < service.characteristics.count; i++)
        {
            CBCharacteristic* characteristic =  [service.characteristics objectAtIndex:i];
            int serviceUUID = [self CBUUIDToInt:service.UUID];
            int characteristicUUID = [self CBUUIDToInt:characteristic.UUID];
            [self notification:serviceUUID characteristicUUID:characteristicUUID p:peripheral on:YES];
        }
        
        // 데이터 생성
        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
        [dic setObject:peripheral forKey:@"PERIPHERAL"];
        [dic setObject:self.activeAdvertisement forKey:@"ADVERTISEMENT"];
        
        BOOL flag = [self.delegate respondsToSelector:@selector(connectedDevice:)];
        if  (flag)  [self.delegate connectedDevice:dic];
        
        BOOL flag2 = [self.delegateTabbar respondsToSelector:@selector(connectedDevice:)];
        if  (flag2)  [self.delegateTabbar connectedDevice:dic];
        
        self.dicNavInfo = [NSMutableDictionary dictionary];
    }
    else {
        [self cleanup];
        printf("Characteristic discorvery unsuccessfull !\r\n");
    }
}

/*
 *  @method didDiscoverServices
 *
 *  @param peripheral Pheripheral that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didDiscoverServices is called when CoreBluetooth has discovered services on a 
 *  peripheral after the discoverServices routine has been called on the peripheral
 *
 */

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (!error) {

        for (int i=0; i < peripheral.services.count; i++)
        {
            CBService *s = [peripheral.services objectAtIndex:i];
            [peripheral discoverCharacteristics:nil forService:s];
        }
    }
    else {
        printf("Service discovery was unsuccessfull !\r\n");
    }
}


/*
 *  @method didUpdateValueForCharacteristic
 *
 *  @param peripheral Pheripheral that got updated
 *  @param characteristic Characteristic that got updated
 *  @error error Error message if something went wrong
 *
 *  @discussion didUpdateValueForCharacteristic is called when CoreBluetooth has updated a 
 *  characteristic for a peripheral. All reads and notifications come here to be processed.
 *
 */



- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
    //NSLog(@"didWriteValueForCharacteristic : %@", characteristic);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (!error)
    {
        UInt16 characteristicUUID = [self CBUUIDToInt:characteristic.UUID];
        NSLog(@"▼ 데이터 : %@", characteristic);
        NSData* data = characteristic.value;
        
        switch(characteristicUUID){
                
            case 0xDA01:
            {
                // 초기화
                data4Length = 0;
                
                NSInteger status = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(0, 1)]];         // status
                NSInteger battery = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(1, 1)]];        // battery : 배터리
                NSInteger count = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(2, 2)]];          // count : 측정횟수
                NSInteger year = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(4, 2)]];           // year : 년
                NSInteger month = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(6, 1)]];          // month : 월
                NSInteger day = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(7, 1)]];            // day : 일
                NSInteger hour = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(8, 1)]];           // hour : 시
                NSInteger min = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(9, 1)]];            // min : 분
                NSInteger time = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(10, 2)]];          // time : 대기시간
                NSInteger camera = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(12, 1)]];    // flagCamera : 카메라 동작 1/0
                NSInteger density = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(13, 2)]];       // density : 측정값
                
                
                // 데이터 생성
                NSMutableDictionary* dic = [NSMutableDictionary dictionary];
                
                // 네비바 공통 정보
                [dic setObject:[NSNumber numberWithInteger:battery] forKey:@"battery"];
                [dic setObject:[NSNumber numberWithInteger:count] forKey:@"count"];
                
                
                //
                NSString* date = [NSString stringWithFormat:@"%04ld%02ld%02ld", (long)year, (long)month, (long)day];
                [dic setObject:date forKey:@"date"];
                
                // 교정일
                NSString* date_service = [NSString stringWithFormat:@"%04ld-%02ld-%02ld %02ld:%02ld:00", (long)year, (long)month, (long)day, (long)hour, (long)min];
                [dic setObject:date_service forKey:@"col_service_date"];
                
                // 측정시간
                NSString* col_time = [UtilDate stringFormat:@"yyyy-MM-dd HH:mm:ss"];
                [dic setObject:col_time forKey:@"col_time"];
                
                // 현재시간(키값)
                NSString* col_number = [UtilDate stringFormat:@"yyyyMMddHHmmssSSS"];
                [dic setObject:col_number forKey:@"col_number"];
                
                NSInteger col_error_code = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(15, 1)]];
                [dic setObject:[NSNumber numberWithInteger:col_error_code] forKey:@"col_error_code"];
                
                // 노티피케이션에 등록
                if (status == 1)
                {
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData1:)];
                    if  (flag)  [self.delegate receiveData1:dic];
                }
                else if (status == 5) // 대기 시간(Warm up time) 표시
                {
                    // 대기시간 : ex)13초
                    if (time > 0)
                    {
                        time = time/10;
                        
                        // 데이터 생성
                        [dic setObject:[NSNumber numberWithInteger:time] forKey:@"WARMINGUP_TIME"];
                        
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveData5:)];
                        if  (flag)  [self.delegate receiveData5:dic];

                    }
                    // 대기시간 : 0초
                    else if (time == 0)
                    {
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveData5_1:)];
                        if  (flag)  [self.delegate receiveData5_1:nil];
                    }
                }
                
                else if (status == 6) // 대기 시간(Warm up time) 표시
                {
                    // 노티피케이션에 등록
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData6:)];
                    if  (flag)  [self.delegate receiveData6:nil];
                }
                
                else if (status == 7) // Blow 상태
                {
                    // 0 : 블로우 시작
                    if (!boolBlowFlag && camera == 0)
                    {
                        NSLog(@"노티 등록 7 : 블로우 시작");
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveData7:)];
                        if  (flag)  [self.delegate receiveData7:nil];
                        boolBlowFlag = YES;
                    }
                    // 1 : 카메라
                    else if (boolBlowFlag && camera == 1)
                    {
                        NSLog(@"노티 등록 7 : 블로우 카메라");
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveData7_1:)];
                        if  (flag)  [self.delegate receiveData7_1:nil];
                        boolBlowFlag = NO;
                    }
                    // 2 : 블로우 종료
                    else if (boolBlowFlag && camera == 0)
                    {
                        NSLog(@"노티 등록 7 : 블로우 종료");
                        boolBlowFlag = NO;
                    }
                }
                
                else if (status == 8) // 분석중: 센서 반응
                {
                    NSLog(@"노티 등록 8");
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData8:)];
                    if  (flag)  [self.delegate receiveData8:nil];
                }
                
                else if (status == 9) // 결과 : 농도 표시
                {
                    // 데이터 생성
                    [dic setObject:[NSNumber numberWithInteger:density] forKey:@"density"];
                    
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData9:)];
                    if  (flag)  [self.delegate receiveData9:dic];
                }
                
                else if (status == 10) // 결과 : 농도 표시
                {
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData10:)];
                    if  (flag)  [self.delegate receiveData10:dic];
                }
                
                ////////////////// 임시 /////////////////////
                else if (status == 11) // 결과 : 농도 표시
                {
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData10:)];
                    if  (flag)  [self.delegate receiveData10:dic];
                }
                ////////////////// 임시 /////////////////////
                
                else if (status == 129)
                {
                    NSInteger code = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(15, 1)]];       // errorcode : 에러코드
                    
                    // 배터리 부족
                    if (code == 1)
                    {
                        self.boolBatteryFlag = YES;
                        BOOL flag = [self.delegateAlert respondsToSelector:@selector(receiveError1:)];
                        if  (flag)  [self.delegateAlert receiveError1:nil];
                    }
                    // 측정불가
                    else if (code == 2)
                    {
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveError2:)];
                        if  (flag)  [self.delegate receiveError2:nil];
                    }
                    // Warm up time 초과
                    else if (code == 5)
                    {
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveError5:)];
                        if  (flag)  [self.delegate receiveError5:nil];
                    }
                    // Blow 대기 상태에서 시간 초과
                    else if (code == 6)
                    {
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveError6:)];
                        if  (flag)  [self.delegate receiveError6:nil];
                    }
                    // Blow 시간 부족
                    else if (code == 7)
                    {
                        boolBlowFlag = NO;
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveError7:)];
                        if  (flag)  [self.delegate receiveError7:nil];
                    }
                    // 분석시간 초과
                    else if (code == 9)
                    {
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveError9:)];
                        if  (flag)  [self.delegate receiveError9:nil];
                    }
                    // 온도에러
                    else if (code == 11)
                    {
                        BOOL flag = [self.delegate respondsToSelector:@selector(receiveError11:)];
                        if  (flag)  [self.delegate receiveError11:nil];
                    }
                }
                break;
            }
            case 0xDA04: // 단독 사용 데이터 전송
            {
                NSMutableDictionary* dic = [NSMutableDictionary dictionary];
                [dic setObject:characteristic.value forKey:@"RAW"];
                NSInteger status = [TIBLEUtil byteToInt:[characteristic.value subdataWithRange:NSMakeRange(0, 1)]];
               
                // 데이터가 넘어올때
                if (data.length == 1 && status ==0) {
                
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData4:)];
                    if  (flag)  [self.delegate receiveData4:nil];
                }
                else if (data.length == 1 && status >0)
                {
                    data4Length = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange(0, 1)]];         // length
                    data4Index = 0;
                    [self sendData:0 b2:0];
                }
                else if (data.length > 2) {
                
                    NSMutableArray* array = [NSMutableArray array];
                    NSUInteger densityLength = (data.length-2)/2;
                    for (int i=0; i<densityLength; i++)
                    {
                        NSUInteger density = [TIBLEUtil byteToInt:[data subdataWithRange:NSMakeRange((i*2)+2, 2)]];
                        NSLog(@"density:%d range:%d", density, (i*2)+2);
                        [array addObject:[NSNumber numberWithInteger:density]];
                    }
                    [dic setObject:array forKey:@"array"];
                    
                    BOOL flag = [self.delegate respondsToSelector:@selector(receiveData4:)];
                    if  (flag)  [self.delegate receiveData4:dic];
                    
                    data4Index += densityLength;
                    if (data4Index < data4Length-1) {
                        [self sendData:0 b2:data4Index];
                    }
                }
                break;
            }
        }
    }
    else {
        printf("updateValueForCharacteristic failed !");
    }
}


/*!
 *  @method centralManagerStateToString:
 *
 *  @param state State to print info of
 *
 *  @discussion centralManagerStateToString prints information text about a given CBCentralManager state
 *
 */
- (const char *) centralManagerStateToString: (int)state
{
    switch(state) {
        case CBCentralManagerStateUnknown:
            return "State unknown (CBCentralManagerStateUnknown)";
        case CBCentralManagerStateResetting:
            return "State resetting (CBCentralManagerStateUnknown)";
        case CBCentralManagerStateUnsupported:
            return "State BLE unsupported (CBCentralManagerStateResetting)";
        case CBCentralManagerStateUnauthorized:
            return "State unauthorized (CBCentralManagerStateUnauthorized)";
        case CBCentralManagerStatePoweredOff:
            return "State BLE powered off (CBCentralManagerStatePoweredOff)";
        case CBCentralManagerStatePoweredOn:
            return "State powered up and ready (CBCentralManagerStatePoweredOn)";
        default:
            return "State unknown";
    }
    return "Unknown state";
}

/*!
 *  @method scanTimer:
 *
 *  @param timer Backpointer to timer
 *
 *  @discussion scanTimer is called when findBLEPeripherals has timed out, it stops the CentralManager from scanning further and prints out information about known peripherals
 *
 */
- (void) scanTimer:(NSTimer *)timer
{
    [self.CM stopScan];
    NSLog(@"블루투스 검색 스톱");
    //printf("Known peripherals : %d\r\n",[self->peripherals count]);
    [self printKnownPeripherals];
}

- (void) stopScanning
{
    [self.timer invalidate];
    self.timer = nil;
    [self.CM stopScan];
    NSLog(@"블루투스 검색 스톱");
}


/*!
 *  @method printKnownPeripherals:
 *
 *  @discussion printKnownPeripherals prints all curenntly known peripherals stored in the peripherals array of TIBLECBKeyfob class
 *
 */
- (void) printKnownPeripherals
{
    int i;
    printf("List of currently known peripherals : \r\n");
    for (i=0; i < self->peripherals.count; i++)
    {
        CBPeripheral *p = [self->peripherals objectAtIndex:i];
        //        CFStringRef s = CFUUIDCreateString(NULL, p.UUID);
        //        printf("%d  |  %s\r\n",i,CFStringGetCStringPtr(s, 0));
        [self printPeripheralInfo:p];
    }
}

/*
 *  @method printPeripheralInfo:
 *
 *  @param peripheral Peripheral to print info of
 *
 *  @discussion printPeripheralInfo prints detailed info about peripheral
 *
 */
- (void) printPeripheralInfo:(CBPeripheral*)peripheral
{
    //    CFStringRef s = CFUUIDCreateString(NULL, peripheral.UUID);
    //    printf("------------------------------------\r\n");
    //    printf("Peripheral Info :\r\n");
    //    printf("UUID : %s\r\n",CFStringGetCStringPtr(s, 0));
    //    printf("RSSI : %d\r\n",[peripheral.RSSI intValue]);
    //    NSLog(@"Name : %@\r\n",peripheral.name);
    //    printf("isConnected : %d\r\n",peripheral.isConnected);
    //    printf("-------------------------------------\r\n");
    
}

/*
 *  @method UUIDSAreEqual:
 *
 *  @param u1 CFUUIDRef 1 to compare
 *  @param u2 CFUUIDRef 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compares two CFUUIDRef's
 *
 */

- (int) UUIDSAreEqual:(CFUUIDRef)u1 u2:(CFUUIDRef)u2 {
    CFUUIDBytes b1 = CFUUIDGetUUIDBytes(u1);
    CFUUIDBytes b2 = CFUUIDGetUUIDBytes(u2);
    if (memcmp(&b1, &b2, 16) == 0) {
        return 1;
    }
    else return 0;
}

/*
 *  @method CBUUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion CBUUIDToString converts the data of a CBUUID class to a character pointer for easy printout using printf()
 *
 */
-(const char *) CBUUIDToString:(CBUUID *) UUID {
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}


/*
 *  @method UUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion UUIDToString converts the data of a CFUUIDRef class to a character pointer for easy printout using printf()
 *
 */
-(const char *) UUIDToString:(CFUUIDRef)UUID {
    if (!UUID) return "NULL";
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    return CFStringGetCStringPtr(s, 0);
    
}

/*
 *  @method compareCBUUID
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUID compares two CBUUID's to each other and returns 1 if they are equal and 0 if they are not
 *
 */

-(int) compareCBUUID:(CBUUID *) UUID1 UUID2:(CBUUID *)UUID2 {
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    if (memcmp(b1, b2, UUID1.data.length) == 0)return 1;
    else return 0;
}

/*
 *  @method compareCBUUIDToInt
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UInt16 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUIDToInt compares a CBUUID to a UInt16 representation of a UUID and returns 1
 *  if they are equal and 0 if they are not
 *
 */
-(int) compareCBUUIDToInt:(CBUUID *)UUID1 UUID2:(UInt16)UUID2 {
    char b1[16];
    [UUID1.data getBytes:b1];
    UInt16 b2 = [self swap:UUID2];
    if (memcmp(b1, (char *)&b2, 2) == 0) return 1;
    else return 0;
}
/*
 *  @method CBUUIDToInt
 *
 *  @param UUID1 UUID 1 to convert
 *
 *  @returns UInt16 representation of the CBUUID
 *
 *  @discussion CBUUIDToInt converts a CBUUID to a Uint16 representation of the UUID
 *
 */
-(UInt16) CBUUIDToInt:(CBUUID *) UUID {
    char b1[16];
    [UUID.data getBytes:b1];
    return ((b1[0] << 8) | b1[1]);
}

/*
 *  @method IntToCBUUID
 *
 *  @param UInt16 representation of a UUID
 *
 *  @return The converted CBUUID
 *
 *  @discussion IntToCBUUID converts a UInt16 UUID to a CBUUID
 *
 */
-(CBUUID *) IntToCBUUID:(UInt16)UUID {
    char t[16];
    t[0] = ((UUID >> 8) & 0xff); t[1] = (UUID & 0xff);
    NSData *data = [[NSData alloc] initWithBytes:t length:16];
    return [CBUUID UUIDWithData:data];
}


/*
 *  @method findServiceFromUUID:
 *
 *  @param UUID CBUUID to find in service list
 *  @param p Peripheral to find service on
 *
 *  @return pointer to CBService if found, nil if not
 *
 *  @discussion findServiceFromUUID searches through the services list of a peripheral to find a
 *  service with a specific UUID
 *
 */
-(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p {
    
    for(int i = 0; i < p.services.count; i++) {
        CBService *s = [p.services objectAtIndex:i];
        if ([self compareCBUUID:s.UUID UUID2:UUID]) return s;
    }
    return nil; //Service not found on this peripheral
}

/*
 *  @method findCharacteristicFromUUID:
 *
 *  @param UUID CBUUID to find in Characteristic list of service
 *  @param service Pointer to CBService to search for charateristics on
 *
 *  @return pointer to CBCharacteristic if found, nil if not
 *
 *  @discussion findCharacteristicFromUUID searches through the characteristic list of a given service
 *  to find a characteristic with a specific UUID
 *
 */
-(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service {
    for(int i=0; i < service.characteristics.count; i++) {
        CBCharacteristic *c = [service.characteristics objectAtIndex:i];
        if ([self compareCBUUID:c.UUID UUID2:UUID]) return c;
    }
    return nil; //Characteristic not found on this service
}

@end
