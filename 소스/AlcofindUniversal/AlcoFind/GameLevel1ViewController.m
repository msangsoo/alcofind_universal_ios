//
//  GameLevel1ViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "GameLevel1ViewController.h"

@interface GameLevel1ViewController ()

@end

@implementation GameLevel1ViewController

@synthesize actionPicker;
@synthesize buttonPeople;
@synthesize seletedIndexPicker;
@synthesize arrayPicker;
@synthesize buttonBack;
@synthesize buttonNext;
@synthesize labelTitle;
@synthesize labelDday;
@synthesize labelCount;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = self;
    
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    [self.buttonNext setTitle:[UtilLocal localString:@"NEXT"] forState:UIControlStateNormal];
    self.labelTitle.text = [UtilLocal localString:@"Game1Title"];
    
    NSString* strPeople1 = [NSString stringWithFormat:@"1 %@", [UtilLocal localString:@"People"]];
    NSString* strPeople2 = [NSString stringWithFormat:@"2 %@", [UtilLocal localString:@"People"]];
    NSString* strPeople3 = [NSString stringWithFormat:@"3 %@", [UtilLocal localString:@"People"]];
    NSString* strPeople4 = [NSString stringWithFormat:@"4 %@", [UtilLocal localString:@"People"]];
    NSString* strPeople5 = [NSString stringWithFormat:@"5 %@", [UtilLocal localString:@"People"]];
    
    self.arrayPicker = @[strPeople1, strPeople2, strPeople3, strPeople4, strPeople5];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:4];
            [tabbarVC setSelectedButtonIndex:4];
        }
    }
    
    [self setNavBarInfo];
}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.seletedIndexPicker = 0;
    
    // 현재 상태 보내기
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob sendData:ALCOFIND_SEND_4B];
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

// 네비바 설정 : 카운트, 디데이 정보
- (void) setNavBarInfo
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
    
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트:
//////////////////////////////////////////////////

- (IBAction)touchUpButtonPeople:(id)sender
{
    // 액션시트 버튼 클릭: 확인
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
    {
        self.seletedIndexPicker = selectedIndex;
        [self.buttonPeople setTitle:selectedValue forState:UIControlStateNormal];
    };
    
    // 액션시트 버튼 클릭: 취소
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker){};
    
    // 액션시트
    self.actionPicker = [ActionSheetStringPicker showPickerWithTitle:@"" rows:self.arrayPicker initialSelection:self.seletedIndexPicker doneBlock:done cancelBlock:cancel origin:sender];
}

- (IBAction)touchUpButtonNext:(id)sender
{
    NSMutableArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
    [array removeAllObjects];
    
    for (int i=0; i<self.seletedIndexPicker+1; i++)
    {
        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
        //[dic setValue:@"" forKey:@"Name"];
        [array addObject:dic];
    }
    
    // segue 호출
    [self performSegueWithIdentifier:@"segueShowGameLevel2" sender:self];
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

// 블루투스 장치 연결 해제
- (void) disconnectedDevice
{
//    NSString* s1 = [UtilLocal localString:@"DISCONNECT_TITLE"];
//    NSString* s2 = [UtilLocal localString:@"DISCONNECT_MESSAGE"];
//    NSString* s3 = [UtilLocal localString:@"Done"];
//    
//    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:s1 message:s2 delegate:self cancelButtonTitle:s3 otherButtonTitles: nil];
//    alert.alertViewStyle = UIAlertViewStyleDefault;
//    [alert show];
    
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}
// 블루투스 Off
- (void) statePoweredOff
{
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
