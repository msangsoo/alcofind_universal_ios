//
//  DismissSegue.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 26..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DismissSegue.h"

@implementation DismissSegue

- (void)perform
{
    [self.sourceViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
