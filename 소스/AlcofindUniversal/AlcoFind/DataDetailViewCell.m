//
//  DataDetailViewCell.m
//  AlcoFind  //  //  Created by MNJ_Mac on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataDetailViewCell.h"

@implementation DataDetailViewCell

@synthesize vLine2Trailling;
@synthesize buttonMemoTrailling;
@synthesize buttonDelTrailling;

@synthesize buttonMap;
@synthesize buttonPhoto;
@synthesize buttonMemo;
@synthesize buttonDel;
@synthesize labelBreath;

@synthesize viewBreathanylazer;
@synthesize viewTime;

//////////////////////////////////////////////////
////////// MARK: - 이벤트 : 버튼
//////////////////////////////////////////////////

- (IBAction)touchUpButtonDel:(UIButton*)sender
{
    NSIndexPath* indexPath = [(UITableView*)self.superview.superview indexPathForCell:self];
    
    NSLog(@"indexpath : %@", indexPath);
}

@end
