//
//  UtilDate.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 29..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilDate : NSObject

// 날짜가져오기
+ (NSDate*) today;     // 오늘
+ (NSDate*) yesterday; // 어제
+ (NSDate*) tomorrow;  // 내일
+ (NSDate*) firstdayInWeek; // 이번주 첫째날 (일요일)
+ (NSDate*) day:(NSInteger)num; // 며칠전 며칠후
+ (NSDate*) dateFromString:(NSString*)string; // 스트링을 날짜로 반환
+ (NSDate*) dateFromString:(NSString*)string format:(NSString*)format;

// 날짜포멧 가져오기
+ (NSString*)stringFormat:(NSString*)format;
+ (NSString*)stringFormat:(NSString*)format date:(NSDate*)date;
+ (NSString*)stringFormat:(NSString*)format date:(NSDate*)date locale:(NSString*)locale;

// 두날짜 비교 차이
+ (NSInteger) compareDate:(NSDate *)date1 date2:(NSDate *)date2;

// 현재 날짜 밀리세컨드로 가져오기
+ (double) getMilliseconds;

@end
