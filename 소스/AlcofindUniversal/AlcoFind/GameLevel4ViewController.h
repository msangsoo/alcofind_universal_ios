//
//  GameLevel4ViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameLevel4ViewCell.h"
#import "GameLevel4ViewHeaderCell.h"
#import "UtilDensity.h"
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"

@interface GameLevel4ViewController : UIViewController <TIBLEDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *buttonRestart;
@property (strong, nonatomic) IBOutlet UIButton *buttonResult;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

@end
