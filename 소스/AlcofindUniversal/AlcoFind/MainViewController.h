//
//  MainViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 25..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TIBLECBKeyfob.h"
#import "UtilDate.h"
#import "DataDensity.h"
#import "UtilImage.h"
#import "UtilLocal.h"
#import "TabbarViewController.h"
#import "UIButtonCustom.h"

@interface MainViewController : UIViewController <TIBLEDelegate, UITextFieldDelegate> {
    
}

@property (strong, nonatomic) IBOutlet UIButton *btnConnect;
@property (strong, nonatomic) IBOutlet UILabel *labelData;
@property (strong, nonatomic) IBOutlet UILabel *labelDeviceName;

@property (retain, nonatomic) NSDictionary* dicDevice;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewCircle1;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewCircle2;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewCircle3;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewCircle4;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewCircle5;

@property (strong, nonatomic) IBOutlet UIView *viewScanning;
@property (strong, nonatomic) IBOutlet UIView *viewStarting;
@property (strong, nonatomic) IBOutlet UIView *viewLoading;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewLoading;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewStartingCircle;

@property (retain, nonatomic) NSTimer* timerSend1;
@property (retain, nonatomic) NSTimer* timerBluetoothOff;
@property (assign, nonatomic) NSUInteger timerCount;
@property (assign, nonatomic) NSInteger gap;

@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewBattery;

@property (strong, nonatomic) IBOutlet UILabel *labelScanning;
@property (strong, nonatomic) IBOutlet UILabel *labelScanningTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelStartTest;
@property (strong, nonatomic) IBOutlet UILabel *labelStartTitle;

@property (retain, nonatomic) UIAlertView * alertDisconnected;

@property (strong, nonatomic) IBOutlet UIView *viewAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelInfoInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertInfo;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertInfo;

@property (strong, nonatomic) IBOutlet UIButton *buttonStartTest;

@property (strong, nonatomic) IBOutlet UILabel *labelTitleInAlertPair;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertPair;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOKInAlertPair;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonCancelInAlertPair;

@property (strong, nonatomic) IBOutlet UIView *viewAlertPair;
@property (strong, nonatomic) IBOutlet UIView *viewAlertPIN;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleInAlertPIN;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertPIN;
@property (strong, nonatomic) IBOutlet UITextField *fieldPinInAlertPIN;
@property (strong, nonatomic) NSDictionary* dicConnectingDevice;

@property (retain, nonatomic) UITextField* activeField;
@property (strong, nonatomic) IBOutlet UILabel *labelPinError;

@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonPairInAlertPIN;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonCancelInAlertPIN;

@end
