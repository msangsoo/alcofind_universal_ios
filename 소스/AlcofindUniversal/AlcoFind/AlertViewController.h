//
//  AlertViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 11. 12..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButtonCustom.h"
#import "UtilLocal.h"
#import "TIBLECBKeyfob.h"

@interface AlertViewController : UIViewController <TIBLEDelegate>

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelMessage;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOk;

@end
