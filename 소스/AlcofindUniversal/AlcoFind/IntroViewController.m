//
//  IntroViewController.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 24..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "IntroViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController
@synthesize buttonLanguage;
@synthesize buttonUnit;
@synthesize buttonOK;
@synthesize labelTitle;
@synthesize arrayUnit;
@synthesize arrayLanguage;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    self.arrayUnit = [[userDefaults arrayForKey:@"UNIT"] mutableCopy];
    self.arrayLanguage = [[userDefaults arrayForKey:@"LANGUAGE"] mutableCopy];
    self.labelTitle.text = NSLocalizedString(@"titleIntro", @"titleIntro");
    
    // localizable
    [self.buttonUnit setTitle:NSLocalizedString(@"Unit", @"Unit") forState:UIControlStateNormal];
    [self.buttonLanguage setTitle:NSLocalizedString(@"Language", @"Language") forState:UIControlStateNormal];
    
    for (int i=0; i<self.arrayUnit.count; i++)
    {
        NSString* s = [self.arrayUnit objectAtIndex:i];
        [self.arrayUnit replaceObjectAtIndex:i withObject:NSLocalizedString(s, s)];
    }
    
    for (int i=0; i<self.arrayLanguage.count; i++)
    {
        NSString* s = [self.arrayLanguage objectAtIndex:i];
        [self.arrayLanguage replaceObjectAtIndex:i withObject:NSLocalizedString(s, s)];
    }
    
    [userDefaults setInteger:0
                      forKey:@"UNIT_SELECTED"];
    [userDefaults synchronize];
    
    // 카메라 허용 메시지 띄움
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {}];
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트
//////////////////////////////////////////////////

// 버튼 클릭: Language
- (IBAction)pressedButtonLanguage:(id)sender
{
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    // 액션시트 버튼 클릭: 확인
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
    {
        [userDefaults setInteger:selectedIndex forKey:@"LANGUAGE_SELECTED"];
        [userDefaults synchronize];
        
        [sender setTitle:[self.arrayLanguage objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        
        [self checkSelected];
    };
    // 액션시트 버튼 클릭: 취소
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker)
    {
    };
    
    // 액션시트
    NSInteger selectedIndex = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    if (selectedIndex == -1) { selectedIndex = 0; }
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:self.arrayLanguage initialSelection:selectedIndex doneBlock:done cancelBlock:cancel origin:sender];
}

// 버튼 클릭: Unit
- (IBAction)pressedButtonUnit:(id)sender
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    // 액션시트 버튼 클릭: 확인
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
    {
        [userDefaults setInteger:selectedIndex forKey:@"UNIT_SELECTED"];
        [userDefaults synchronize];
        
        [sender setTitle:[self.arrayUnit objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        
        [self checkSelected];

    };
    // 액션시트 버튼 클릭: 취소
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker)
    {
    };
    
    // 액션시트
    NSInteger selectedIndex = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    if (selectedIndex == -1) { selectedIndex = 0; }
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:self.arrayUnit initialSelection:selectedIndex doneBlock:done cancelBlock:cancel origin:sender];
}

//////////////////////////////////////////////////
////////// MARK: - 기능
//////////////////////////////////////////////////

-(BOOL) checkSelected
{
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger intLanguageSelected = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    NSInteger intUnitSelected = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    if (intLanguageSelected > -1 && intUnitSelected > -1)
    {
        [self.buttonOK setEnabled:YES];
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
