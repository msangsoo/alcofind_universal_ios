//
//  UtilLocal.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 29..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "UtilLocal.h"

@implementation UtilLocal

// 앱 지정 로컬 스트링 반환
+ (NSString*) localString:(NSString*)key
{
    NSString *path = nil;
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger selectedIndex = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    
    if      (selectedIndex == 0) path = [[NSBundle mainBundle] pathForResource:@"Base" ofType:@"lproj"];
    else if (selectedIndex == 1) path = [[NSBundle mainBundle] pathForResource:@"fi"   ofType:@"lproj"];
    else if (selectedIndex == 2) path = [[NSBundle mainBundle] pathForResource:@"fr"   ofType:@"lproj"];
    else if (selectedIndex == 3) path = [[NSBundle mainBundle] pathForResource:@"de"   ofType:@"lproj"];
    else if (selectedIndex == 4) path = [[NSBundle mainBundle] pathForResource:@"nb"   ofType:@"lproj"];
    else if (selectedIndex == 5) path = [[NSBundle mainBundle] pathForResource:@"pl"   ofType:@"lproj"];
    else if (selectedIndex == 6) path = [[NSBundle mainBundle] pathForResource:@"ru"   ofType:@"lproj"];
    else if (selectedIndex == 7) path = [[NSBundle mainBundle] pathForResource:@"es"   ofType:@"lproj"];
    else if (selectedIndex == 8) path = [[NSBundle mainBundle] pathForResource:@"sv"   ofType:@"lproj"];
    else if (selectedIndex == 9) path = [[NSBundle mainBundle] pathForResource:@"ko"   ofType:@"lproj"];
    
    NSBundle* languageBundle = [NSBundle bundleWithPath:path];
    NSString* str = [languageBundle localizedStringForKey:key value:@"" table:nil];
    
    /////////// 임시 /////////////
//    if      (selectedIndex == 0) str = [NSString stringWithFormat:@"%@", str];
//    else if (selectedIndex == 1) str = [NSString stringWithFormat:@"%@ de", str];
//    else if (selectedIndex == 2) str = [NSString stringWithFormat:@"%@ pl", str];
//    else if (selectedIndex == 3) str = [NSString stringWithFormat:@"%@ sv", str];
//    else if (selectedIndex == 4) str = [NSString stringWithFormat:@"%@ fr", str];
//    else if (selectedIndex == 5) str = [NSString stringWithFormat:@"%@ es", str];
    /////////// 임시 /////////////
    
    return str;
}

@end
