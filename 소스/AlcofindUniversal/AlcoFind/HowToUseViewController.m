//
//  HowToUseViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 22..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "HowToUseViewController.h"

@interface HowToUseViewController ()

@end

@implementation HowToUseViewController

@synthesize scrollView;
@synthesize scrollViewPage2;

@synthesize buttonPrev;
@synthesize buttonNext;

@synthesize labelPage1Text1;
@synthesize labelPage1Title;

@synthesize labelPage2Title;
@synthesize labelPage2Text1;
@synthesize labelPage2Text2;
@synthesize labelPage2Text3;
@synthesize labelPage2Text4;
@synthesize labelPage2Text5;
@synthesize labelPage2Text6;
@synthesize labelPage2Text7;
@synthesize labelPage2Text8;
@synthesize labelPage2Text9;

@synthesize imageViewPage2Last;
@synthesize labelMessageInAlertInfo;
@synthesize labelTitleInAlertInfo;
@synthesize buttonOkInAlertInfo;
@synthesize viewAlertInfo;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_21];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:1];
            [tabbarVC setSelectedButtonIndex:1];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    { // 페이지 1
        NSString* text = [UtilLocal localString:@"HowToUsePage1Title"];
        self.labelPage1Title.text = text;
    }{
        NSString* text = [UtilLocal localString:@"HowToUsePage1Text"];
        self.labelPage1Text1.text = text;
    }
    
    { // 페이지 2_1
        NSString* text = [UtilLocal localString:@"HowToUsePage2Title"];
        self.labelPage2Title.text = text;
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText1"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText1_Big"]];
        NSArray* arrayColor = @[[UtilLocal localString:@"HowToUseText1_Color"]];
        [self setAttributeText:self.labelPage2Text1 strOrg:text arrayBig:arrayBig arrayColor:arrayColor];
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText2"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText2_Big"]];
        [self setAttributeText:self.labelPage2Text2 strOrg:text arrayBig:arrayBig arrayColor:nil];
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText3"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText3_Big"]];
        [self setAttributeText:self.labelPage2Text3 strOrg:text arrayBig:arrayBig arrayColor:nil];
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText4"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText4_Big"]];
        NSArray* arrayColor = @[[UtilLocal localString:@"HowToUseText4_Color"]];
        [self setAttributeText:self.labelPage2Text4 strOrg:text arrayBig:arrayBig arrayColor:arrayColor];
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText5"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText5_Big"]];
        NSArray* arrayColor = @[[UtilLocal localString:@"HowToUseText5_Color"]];
        [self setAttributeText:self.labelPage2Text5 strOrg:text arrayBig:arrayBig arrayColor:arrayColor];
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText6"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText6_Big"]];
        NSArray* arrayColor = @[[UtilLocal localString:@"HowToUseText6_Color"]];
        [self setAttributeText:self.labelPage2Text6 strOrg:text arrayBig:arrayBig arrayColor:arrayColor];
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText7"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText7_Big"]];
        NSArray* arrayColor = @[[UtilLocal localString:@"HowToUseText7_Color"], [UtilLocal localString:@"HowToUseText7_Color1"]];
        [self setAttributeText:self.labelPage2Text7 strOrg:text arrayBig:arrayBig arrayColor:arrayColor];
    }{
        NSString* text = [UtilLocal localString:@"HowToUseText8"];
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText8_Big"]];
        NSArray* arrayColor = @[[UtilLocal localString:@"HowToUseText8_Color"]];
        [self setAttributeText:self.labelPage2Text8 strOrg:text arrayBig:arrayBig arrayColor:arrayColor];
    }{
        //AU일 경우 셋팅(19.01.16)
        NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
        NSString* text;
        if ([deviceType isEqualToString:@"AU"]) {
            text = [UtilLocal localString:@"HowToUseText9_AU"];
        }else if([deviceType isEqualToString:@"EN"]){
            text = [UtilLocal localString:@"HowToUseText9_EN"];
        }else{
            text = [UtilLocal localString:@"HowToUseText9"];
        }
        NSArray* arrayBig = @[[UtilLocal localString:@"HowToUseText9_Big"]];
        NSArray* arrayColor = @[[UtilLocal localString:@"HowToUseText9_Color"], [UtilLocal localString:@"HowToUseText9_Color1"], [UtilLocal localString:@"HowToUseText9_Color2"], [UtilLocal localString:@"HowToUseText9_Color3"], [UtilLocal localString:@"HowToUseText9_Color4"], [UtilLocal localString:@"HowToUseText9_Color5"]];
        [self setAttributeText:self.labelPage2Text9 strOrg:text arrayBig:arrayBig arrayColor:arrayColor];
    }
    
    [self.buttonOkInAlertInfo setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.viewAlertInfo setHidden:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGSize size = scrollView.frame.size;
    scrollView.contentSize = CGSizeMake(size.width * 2, size.height);
    
    CGFloat height = self.imageViewPage2Last.frame.size.height;
    CGFloat y = self.imageViewPage2Last.frame.origin.y;
    
    scrollViewPage2.contentSize = CGSizeMake(size.width, height+y+50);
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

// 크기및 컬러 변경
- (void) setAttributeText:(UILabel*)label strOrg:(NSString*)strOrg arrayBig:(NSArray*)arrayBig arrayColor:(NSArray*)arrayColor
{
    NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:strOrg];

    // 크기변경
    for (NSString* strBig in arrayBig)
    {
        UIFont* font = [UIFont fontWithName:@"Roboto-Medium" size:18.0f];
        NSRange rangeFont = [strOrg rangeOfString:strBig];
        [attributed addAttribute:NSFontAttributeName value:font range:rangeFont];
    }

    // 컬러변경
    for (NSString* strColor in arrayColor)
    {
        UIColor* color = [UIColor colorWithRed:0.28f green:0.72f blue:0.84f alpha:1.0f];
        NSRange rangeColor = [strOrg rangeOfString:strColor];
        [attributed addAttribute:NSForegroundColorAttributeName value:color range:rangeColor];
    }

    label.attributedText = attributed;
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트
//////////////////////////////////////////////////

- (IBAction)touchUpButtonPrev:(id)sender
{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)touchUpButtonNext:(id)sender
{
    CGFloat x = self.scrollView.frame.size.width;
    [self.scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (IBAction)touchUpButtonTop1:(id)sender
{
    self.labelTitleInAlertInfo.text = [UtilLocal localString:@"HTU_Title1"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"HTU_Message1"];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonTop2:(id)sender
{
    self.labelTitleInAlertInfo.text = [UtilLocal localString:@"HTU_Title2"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"HTU_Message2"];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonTop3:(id)sender
{
    self.labelTitleInAlertInfo.text = [UtilLocal localString:@"HTU_Title3"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"HTU_Message3"];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonBottom1:(id)sender
{
    self.labelTitleInAlertInfo.text = [UtilLocal localString:@"HTU_Title4"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"HTU_Message4"];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonBottom2:(id)sender
{
    self.labelTitleInAlertInfo.text = [UtilLocal localString:@"HTU_Title5"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"HTU_Message5"];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonBottom3:(id)sender
{
    self.labelTitleInAlertInfo.text = [UtilLocal localString:@"HTU_Title6"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"HTU_Message6"];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonBottom4:(id)sender
{
    self.labelTitleInAlertInfo.text = [UtilLocal localString:@"HTU_Title7"];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"HTU_Message7"];
    [self.viewAlertInfo setHidden:NO];
}

// AlertInfo
- (IBAction)touchUpButtonOkInAlertInfo:(id)sender
{
    [self.viewAlertInfo setHidden:YES];
}
- (IBAction)touchUpButtonCloseInAlertInfo:(id)sender
{
    [self.viewAlertInfo setHidden:YES];
}

@end
