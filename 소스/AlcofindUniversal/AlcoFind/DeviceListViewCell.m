//
//  DeviceListViewCell.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 26..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DeviceListViewCell.h"

@implementation DeviceListViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
