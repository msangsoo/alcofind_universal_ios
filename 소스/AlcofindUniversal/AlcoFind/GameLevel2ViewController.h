//
//  GameLevel2ViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameLevel2ViewCell.h"
#import "GameLevel2ViewHeaderCell.h"
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"

@interface GameLevel2ViewController : UIViewController <TIBLEDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) UITextField* activeField;

@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UIButton *buttonStart;
@property (strong, nonatomic) IBOutlet UILabel *labelGame;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;


@property (retain, nonatomic) NSMutableArray* arrayTextFieldName;
@property (retain, nonatomic) NSMutableArray* arrayTextFieldEstimated;

@end
