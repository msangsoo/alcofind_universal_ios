//
//  UtilColor.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 21..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "UtilColor.h"

@implementation UtilColor

+ (UIColor*)hex2RGB:(NSString*)hexValue
{
    uint rDec;
    uint gDec;
    uint bDec;
    NSString* r = [hexValue substringWithRange:NSMakeRange(0, 2)];
    NSString* g = [hexValue substringWithRange:NSMakeRange(2, 2)];
    NSString* b = [hexValue substringWithRange:NSMakeRange(4, 2)];
    
    NSScanner* scan = [NSScanner scannerWithString:r];
    [scan scanHexInt:&rDec];
    
    scan = [NSScanner scannerWithString:g];
    [scan scanHexInt:&gDec];
    
    scan = [NSScanner scannerWithString:b];
    [scan scanHexInt:&bDec];
    
    return [UtilColor hex2RGB:hexValue];
}


+ (UIColor*)hex2RGB:(NSString*)hexValue andAlpha:(float)alpha
{
    uint rDec;
    uint gDec;
    uint bDec;
    NSString* r = [hexValue substringWithRange:NSMakeRange(0, 2)];
    NSString* g = [hexValue substringWithRange:NSMakeRange(2, 2)];
    NSString* b = [hexValue substringWithRange:NSMakeRange(4, 2)];
    
    NSScanner* scan = [NSScanner scannerWithString:r];
    [scan scanHexInt:&rDec];
    
    scan = [NSScanner scannerWithString:g];
    [scan scanHexInt:&gDec];
    
    scan = [NSScanner scannerWithString:b];
    [scan scanHexInt:&bDec];
    
    return [UIColor colorWithRed:rDec/255.0f green:gDec/255.0f blue:bDec/255.0f alpha:alpha];
}


@end
