//
//  GameLevel1ViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetStringPicker.h"
#import "UIButtonCustom.h"
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UIButtonCustom.h"
#import "UtilLocal.h"

@interface GameLevel1ViewController : UIViewController <TIBLEDelegate>

@property (nonatomic, retain) NSArray* arrayPicker;
@property (assign, nonatomic) NSUInteger seletedIndexPicker;

@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonNext;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonPeople;
@property (nonatomic, retain) ActionSheetStringPicker* actionPicker;

@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

@end
