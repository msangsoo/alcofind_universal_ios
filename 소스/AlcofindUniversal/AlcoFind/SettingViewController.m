//
//  SettingViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

@synthesize viewLanguage;
@synthesize scrollView;

// sliderAlarmLimit
@synthesize sliderAlarmLimit;
@synthesize labelAlarmLimitValue;
@synthesize labelAlarmLimitMinValue;
@synthesize labelAlarmLimitMaxValue;

@synthesize sliderWarningLimit;
@synthesize labelWarningLimitValue;

@synthesize sliderLimitedNumber;
@synthesize labelLimitedNumberValue;

@synthesize buttonWarningMessage;
@synthesize buttonSound;
@synthesize buttonCamera;
@synthesize buttonUnit;
@synthesize buttonLanguage;

@synthesize viewHethcareOnOff;
@synthesize viewAlertDel;

@synthesize labelWarningLimitMinValue;
@synthesize labelWarningLimitMaxValue;

//
@synthesize buttonBack;
@synthesize labelAlarmLimit;
@synthesize labelHealthcare;
@synthesize labelWarningLimit;
@synthesize labelLimitedNumber;
@synthesize labelDeleteAll;
@synthesize labelSound;
@synthesize labelCamera;
@synthesize labelUnit;
@synthesize labelLanguage;
@synthesize labelDeleteInAlert;
@synthesize labelTitleInAlert;
@synthesize buttonOkInAlert;
@synthesize buttonCancelInAlert;

@synthesize arrayUnit;
@synthesize arrayLanguage;
@synthesize labelMessageInAlertInfo;
@synthesize labelTitleInAlertInfo;
@synthesize buttonCloseInAlertInfo;
@synthesize buttonOkInAlertInfo;
@synthesize viewAlertInfo;

@synthesize labelCount;
@synthesize labelDday;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_31];
    
    // 슬라이더 : 기본값 설정
    [self resetSliderAlarmLimit];
    
    [self setButtonInset:self.buttonWarningMessage];
    [self setButtonInset:self.buttonSound];
    [self setButtonInset:self.buttonCamera];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    self.arrayUnit = [[userDefaults arrayForKey:@"UNIT"] mutableCopy];
    self.arrayLanguage = [[userDefaults arrayForKey:@"LANGUAGE"] mutableCopy];
    
    // localizable
    self.labelDeleteInAlert.text = [UtilLocal localString:@"Delete"];
    self.labelTitleInAlert.text = [UtilLocal localString:@"titleInAlertDel"];
    self.labelAlarmLimit.text = [UtilLocal localString:@"Alarmlimit"];
    self.labelHealthcare.text = [UtilLocal localString:@"Healthcare"];
    self.labelWarningLimit.text = [UtilLocal localString:@"WarningLimit"];
    self.labelLimitedNumber.text = [UtilLocal localString:@"LimitedNumber"];
    self.labelDeleteAll.text = [UtilLocal localString:@"DeleteAll"];
    self.labelSound.text = [UtilLocal localString:@"Sound"];
    self.labelCamera.text = [UtilLocal localString:@"Camera"];
    self.labelUnit.text = [UtilLocal localString:@"Unit"];
    self.labelLanguage.text = [UtilLocal localString:@"Language"];
    
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    [self.buttonOkInAlert setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.buttonCancelInAlert setTitle:[UtilLocal localString:@"Cancel"] forState:UIControlStateNormal];
    [self.buttonUnit setTitle:[UtilLocal localString:@"Unit"] forState:UIControlStateNormal];
    [self.buttonLanguage setTitle:[UtilLocal localString:@"Language"] forState:UIControlStateNormal];
    
    // alertInfo
    [self.labelTitleInAlertInfo setText:[UtilLocal localString:@"Info"]];
    [self.labelMessageInAlertInfo setText:[UtilLocal localString:@"HelpMessage1"]];
    [self.buttonOkInAlertInfo setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    
    for (int i=0; i<self.arrayUnit.count; i++)
    {
        NSString* s = [self.arrayUnit objectAtIndex:i];
        [self.arrayUnit replaceObjectAtIndex:i withObject:[UtilLocal localString:s]];
    }
    
    for (int i=0; i<self.arrayLanguage.count; i++)
    {
        NSString* s = [self.arrayLanguage objectAtIndex:i];
        [self.arrayLanguage replaceObjectAtIndex:i withObject:[UtilLocal localString:s]];
    }
    
    // UNIT 값 설정
    {
        NSInteger index = [userDefaults integerForKey:@"UNIT_SELECTED"];
        if (index > -1)
        {
            [self.buttonUnit setTitle:[self.arrayUnit objectAtIndex:index] forState:UIControlStateNormal];
        }
    }
    
    // LANGUAGE 값 설정
    {
        NSInteger index = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
        if (index > -1)
        {
            [self.buttonLanguage setTitle:[self.arrayLanguage objectAtIndex:index] forState:UIControlStateNormal];
        }
    }
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:3];
            [tabbarVC setSelectedButtonIndex:3];
        }
    }
    
    // 네비바 설정 : 카운트, 디데이 정보
    [self setNavBarInfo];
    
    NSString *svr_ip        = [userDefaults objectForKey:@"SERVER_IP"];
    NSString *svr_id        = [userDefaults objectForKey:@"SERVER_ID"];
    NSString *svr_pass      = [userDefaults objectForKey:@"SERVER_PASS"];
    NSString *svr_com       = [userDefaults objectForKey:@"SERVER_COMCODE"];
    NSString *svr_phone     = [userDefaults objectForKey:@"SERVER_PHONE"];
    if (svr_ip) {
        self.txtIP.text = svr_ip;
    }
    if (svr_id) {
        self.txtID.text = svr_id;
    }
    if (svr_pass) {
        self.txtPassword.text = svr_pass;
    }
    if (svr_com) {
        self.txtCompanyCode.text = svr_com;
    }
    if (svr_phone) {
        self.txtPhone.text = svr_phone;
    }
    
#ifdef DEBUG
    self.txtIP.text = @"sql16ssd-004.localnet.kr";
    self.txtID.text = @"lamscom_test";
    self.txtPassword.text = @"lamscom365";
    self.txtCompanyCode.text = @"datech01";
#endif
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //
    [self.viewAlertDel setHidden:YES];
    [self.viewAlertInfo setHidden:YES];
    [self.viewAlertServer setHidden:YES];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self.sliderWarningLimit setValue:[userDefaults floatForKey:@"WARNING_LIMIT_VALUE"]];
    [self.sliderLimitedNumber setValue:[userDefaults floatForKey:@"LIMITED_NUMBER_VALUE"]];

    // 슬라이더 값 설정
    [self changedSliderAlarmLimit: self.sliderAlarmLimit];
    [self changedSliderWarningLimit: self.sliderWarningLimit];
    [self changedSliderLimitedNumber: self.sliderLimitedNumber];
    
    // 버튼 FLAG 값 설정
    BOOL flagWarningMessage = [userDefaults boolForKey:@"WARNING_MESSAGE_FLAG"];
    [self.buttonWarningMessage setSelected:flagWarningMessage];
    [self setStateViewHethcareOnOff];
    
    
    BOOL flagSound = [userDefaults boolForKey:@"SOUND_FLAG"];
    [self.buttonSound setSelected:flagSound];
    
    //AU일 경우 셋팅(18.12.17)
    NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    if ([deviceType isEqualToString:@"AU"]) {
        [self.LineLayout setHidden:YES];
        [self.HealthMessageLayout setHidden:YES];
        [self.WaringLimitLayout setHidden:YES];
        
        
        self.LineConstraintHeight.constant = 0.f;
        self.HealthMessageHeight.constant = 0.f;
        self.WaringLimitHeight.constant = 0.f;
    }else{
        [self.LineLayout setHidden:NO];
        [self.HealthMessageLayout setHidden:NO];
        [self.WaringLimitLayout setHidden:NO];
        
        
        self.LineConstraintHeight.constant = 21.f;
        self.HealthMessageHeight.constant = 22.f;
        self.WaringLimitHeight.constant = 100.f;
    }
    
    if([deviceType isEqualToString:@"ST_SVR"]){
        self.viewServerRow.hidden = NO;
        self.ServerHeight.constant = 41.f;
    }else{
        self.viewServerRow.hidden = YES;
        self.ServerHeight.constant = 0.f;
    }
    
    self.versionLb.text = [NSString stringWithFormat:@"v %@",[self getVersion]];
    
    
    self.txtIP.layer.borderWidth=1.0;
    self.txtID.layer.borderWidth=1.0;
    self.txtPassword.layer.borderWidth=1.0;
    self.txtCompanyCode.layer.borderWidth=1.0;
    self.txtPhone.layer.borderWidth=1.0;
    
    self.txtIP.layer.borderColor=[[UIColor colorWithRed:72/255.0f green:185/255.0f blue:216/255.0f alpha:1.0] CGColor];
    self.txtID.layer.borderColor=[[UIColor colorWithRed:72/255.0f green:185/255.0f blue:216/255.0f alpha:1.0] CGColor];
    self.txtPassword.layer.borderColor=[[UIColor colorWithRed:72/255.0f green:185/255.0f blue:216/255.0f alpha:1.0] CGColor];
    self.txtCompanyCode.layer.borderColor=[[UIColor colorWithRed:72/255.0f green:185/255.0f blue:216/255.0f alpha:1.0] CGColor];
    self.txtPhone.layer.borderColor=[[UIColor colorWithRed:72/255.0f green:185/255.0f blue:216/255.0f alpha:1.0] CGColor];
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleServerPopupTap:)];
    [self.viewIP addGestureRecognizer:singleFingerTap];
    [self.viewAlertServer addGestureRecognizer:singleFingerTap];
    
    // 카메라 버튼 설정
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    // 카메라 승인 했을 경우
    if(authStatus == AVAuthorizationStatusAuthorized) {
        
    } else if(authStatus == AVAuthorizationStatusDenied){
        
        // 유저 데이터 : 카메라 기능 끄기
        BOOL flagCamera = NO;
        [self.buttonCamera setSelected:flagCamera];
        [self.buttonCamera setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 14.0, 0.0, 0.0)];
        
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:flagCamera forKey:@"CAMERA_FLAG"];
        [userDefaults synchronize];
    }
    
    BOOL flagCamera = [userDefaults boolForKey:@"CAMERA_FLAG"];
    [self.buttonCamera setSelected:flagCamera];
    
    [self serverSubmitButtonAble];
}

- (void)viewDidLayoutSubviews
{
    CGFloat w = self.viewLanguage.frame.size.width;
    CGFloat h = self.viewLanguage.frame.size.height;
    CGFloat y = self.viewLanguage.frame.origin.y;
    CGSize size = CGSizeMake(w, h+y);
    [self.scrollView setContentSize:size];
}

- (void)handleServerPopupTap:(UITapGestureRecognizer *)recognizer
{
    [self.txtIP endEditing:YES];
    [self.txtID endEditing:YES];
    [self.txtPassword endEditing:YES];
    [self.txtCompanyCode endEditing:YES];
    [self.txtPhone endEditing:YES];
}

- (void) setButtonInset:(UIButton*)button
{
    if (button.selected){
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 15.0)];
    } else {
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 14.0, 0.0, 0.0)];
    }
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

// 네비바 설정 : 카운트, 디데이 정보
- (void) setNavBarInfo
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
    
}


//////////////////////////////////////////////////
////////// MARK: - 기능
//////////////////////////////////////////////////

- (void) resetSliderAlarmLimit
{
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray* arrayUnitMax = [userDefaults arrayForKey:@"UNIT_MAX"];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    NSArray* array = [arrayUnitMax objectAtIndex:intUnit];
    
    // 슬라이더 : 기본값 설정
    
    // AlarmLimit
    NSUInteger level1 = [userDefaults integerForKey:@"ALARM_LIMIT_VALUE_LEVEL"];
    
    [self.labelAlarmLimitMinValue setText:[array objectAtIndex:1]];
    [self.labelAlarmLimitMaxValue setText:[array objectAtIndex:8]];
    [self.labelAlarmLimitValue setText:[array objectAtIndex:level1]];
    
    [self.sliderAlarmLimit setMinimumValue:1];
    [self.sliderAlarmLimit setMaximumValue:8];
    [self.sliderAlarmLimit setValue:level1];
    
    // WarningLimit
    NSUInteger level2 = [userDefaults integerForKey:@"WARNING_LIMIT_VALUE_LEVEL"];

    [self.labelWarningLimitMinValue setText:[array objectAtIndex:1]];
    [self.labelWarningLimitMaxValue setText:[array objectAtIndex:8]];
    [self.labelWarningLimitValue setText:[array objectAtIndex:level2]];
    
    [self.sliderWarningLimit setMinimumValue:1];
    [self.sliderWarningLimit setMaximumValue:8];
    [self.sliderWarningLimit setValue:level2];
}

// 딤드로 가리기 : healthcareOnOff
- (void) setStateViewHethcareOnOff
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flag = [userDefaults boolForKey:@"WARNING_MESSAGE_FLAG"];
    
    if (flag)
    {
        [self.viewHethcareOnOff setHidden:YES];
    }
    else
    {
        [self.viewHethcareOnOff setHidden:NO];
    }
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 :
//////////////////////////////////////////////////

// 슬라이더 changed: AlarmLimit
- (IBAction)changedSliderAlarmLimit:(UISlider*)sender
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray* arrayUnitMax = [userDefaults arrayForKey:@"UNIT_MAX"];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    NSArray* array = [arrayUnitMax objectAtIndex:intUnit];
    
    NSString* str = [array objectAtIndex:sender.value];
    self.labelAlarmLimitValue.text = str;
}

// 슬라이더 touchUp: AlarmLimit
- (IBAction)touchUpSliderAlarmLimit:(UISlider*)sender
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:sender.value forKey:@"ALARM_LIMIT_VALUE_LEVEL"];
    [userDefaults synchronize];
    
    [self resetSliderAlarmLimit];
    
    //[[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_31];
}


// 슬라이더 changed: WarningLimit
- (IBAction) changedSliderWarningLimit:(UISlider*)sender
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray* arrayUnitMax = [userDefaults arrayForKey:@"UNIT_MAX"];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    NSArray* array = [arrayUnitMax objectAtIndex:intUnit];
    
    NSString* str = [array objectAtIndex:sender.value];
    self.labelWarningLimitValue.text = str;
}

// 슬라이더 touchUp: WarningLimit
- (IBAction) touchUpSliderWarningLimit:(UISlider*)sender
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:sender.value forKey:@"WARNING_LIMIT_VALUE_LEVEL"];
    [userDefaults synchronize];
    
    [self resetSliderAlarmLimit];
}


// 슬라이더 changed: LimitedNumber
- (IBAction) changedSliderLimitedNumber:(UISlider*)sender
{
    NSString* val = [NSString stringWithFormat:@"%.0f", sender.value];
    [self.labelLimitedNumberValue setText:val];
}

// 슬라이더 touchUp: LimitedNumber
- (IBAction) touchUpSliderLimitedNumber:(UISlider*)sender
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setFloat:sender.value forKey:@"LIMITED_NUMBER_VALUE"];
    [userDefaults synchronize];
}

// 버튼 touchUp: buttonWarningMessage
- (IBAction)touchUpButtonWarningMessage:(UIButton*)sender
{
    sender.selected = !sender.selected;
    
    if (sender.selected){
        [sender setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 15.0)];
    } else {
        [sender setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 14.0, 0.0, 0.0)];
    }
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:sender.selected forKey:@"WARNING_MESSAGE_FLAG"];
    [userDefaults synchronize];

    //
    if (sender.selected)
    {
        [userDefaults setObject:[NSDate date] forKey:@"WARNING_MESSAGE_FLAG_DATE"];
        [userDefaults synchronize];
        [self.viewHethcareOnOff setHidden:YES];
    }
    else
    {
        [userDefaults setObject:nil forKey:@"WARNING_MESSAGE_FLAG_DATE"];
        [userDefaults synchronize];
        [self.viewHethcareOnOff setHidden:NO];
    }

}


// 버튼 touchUp: buttonSound
- (IBAction)touchUpButtonSound:(UIButton*)sender
{
    sender.selected = !sender.selected;
    
    if (sender.selected){
        [sender setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 15.0)];
    } else {
        [sender setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 14.0, 0.0, 0.0)];
    }
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:sender.selected forKey:@"SOUND_FLAG"];
    [userDefaults synchronize];
}

// 버튼 touchUp: buttonCamera
- (IBAction)touchUpButtonCamera:(UIButton*)sender
{
    sender.selected = !sender.selected;
    
    if (sender.selected){
        
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            
        // 카메라 승인 했을 경우
        if(authStatus == AVAuthorizationStatusAuthorized) {
            
            [sender setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 15.0)];

        } else if(authStatus == AVAuthorizationStatusDenied){
            
            sender.selected = !sender.selected;
            [sender setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 14.0, 0.0, 0.0)];
        }
    
    } else {
        [sender setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 14.0, 0.0, 0.0)];
    }
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:sender.selected forKey:@"CAMERA_FLAG"];
    [userDefaults synchronize];
}

// 버튼 touchUp: Unit
- (IBAction)pressedButtonUnit:(UIButton*)sender
{
    
    //AU일 경우 셋팅(18.12.17)
    NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    if (![deviceType isEqualToString:@"AU"]) {
        // 유저 데이터
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        
        // 액션시트 버튼 클릭: 확인
        ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
        {
            [userDefaults setInteger:selectedIndex forKey:@"UNIT_SELECTED"];
            [userDefaults synchronize];
            
            [sender setTitle:[self.arrayUnit objectAtIndex:selectedIndex] forState:UIControlStateNormal];
            
            // 리셋 : SliderAlarmLimit
            [self resetSliderAlarmLimit];
        };
        // 액션시트 버튼 클릭: 취소
        ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker)
        {
        };
        
        // 액션시트
        NSInteger selectedIndex = [userDefaults integerForKey:@"UNIT_SELECTED"];
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:self.arrayUnit initialSelection:selectedIndex doneBlock:done cancelBlock:cancel origin:sender];
    }
}

// 버튼 touchUp: Language
- (IBAction)pressedButtonLanguage:(UIButton*)sender
{
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    // 액션시트 버튼 클릭: 확인
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
    {
        [userDefaults setInteger:selectedIndex forKey:@"LANGUAGE_SELECTED"];
        [userDefaults synchronize];
        
        [sender setTitle:[self.arrayLanguage objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        
        [self viewWillAppear:NO];
    };
    // 액션시트 버튼 클릭: 취소
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker)
    {
    };
    
    // 액션시트
    NSInteger selectedIndex = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:self.arrayLanguage initialSelection:selectedIndex doneBlock:done cancelBlock:cancel origin:sender];
}

// 버튼 touchUp: Server
- (IBAction)pressedButtonServer:(UIButton*)sender
{
    [self.viewAlertServer setHidden:NO];
}

- (IBAction)touchUpButtonDel:(id)sender
{
    [self.viewAlertDel setHidden:NO];
}

// AlertDel : 버튼 Close
- (IBAction)touchUpButtonCloseInAlertDel:(id)sender
{
    [self.viewAlertDel setHidden:YES];
}

// AlertDel : 버튼 Close
- (IBAction)touchUpButtonCancelInAlertDel:(id)sender
{
    [self.viewAlertDel setHidden:YES];
}

// AlertDel : 버튼 OK
- (IBAction)touchUpButtonOkInAlertDel:(id)sender
{
    [self.viewAlertDel setHidden:YES];
    
    NSArray* array = [DataDensity selectAllObject];
    
    for (NSManagedObject* obj in array) {
        [DataDensity deleteObject:obj];
    }
//    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Delete all data" message:@"Date deleted." delegate:self cancelButtonTitle:@"Done" otherButtonTitles: nil];
//    [alert show];
}
- (IBAction)touchUpButtonInAlertInfo:(id)sender
{
    [self.viewAlertInfo setHidden:YES];
}

- (IBAction)touchUpButtonHelp1:(id)sender
{
    [self.labelMessageInAlertInfo setText:[UtilLocal localString:@"HelpMessage1"]];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonHelp2:(id)sender
{
    [self.labelMessageInAlertInfo setText:[UtilLocal localString:@"HelpMessage2"]];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonHelp3:(id)sender
{
    [self.labelMessageInAlertInfo setText:[UtilLocal localString:@"HelpMessage3"]];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonHelp4:(id)sender
{
    [self.labelMessageInAlertInfo setText:[UtilLocal localString:@"HelpMessage4"]];
    [self.viewAlertInfo setHidden:NO];
}
- (IBAction)touchUpButtonHelp5:(id)sender
{
    [self.labelMessageInAlertInfo setText:[UtilLocal localString:@"HelpMessage5"]];
    [self.viewAlertInfo setHidden:NO];
}


// AlertServer : 버튼 Close
- (IBAction)touchUpButtonCloseInAlertServer:(id)sender
{
    [self handleServerPopupTap:nil];
    [self.viewAlertServer setHidden:YES];
}

- (IBAction)touchUpButtonSubmitInAlertServer:(id)sender
{
    [self handleServerPopupTap:nil];
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.txtIP.text forKey:@"SERVER_IP"];
    [userDefaults setObject:self.txtID.text forKey:@"SERVER_ID"];
    [userDefaults setObject:self.txtPassword.text forKey:@"SERVER_PASS"];
    [userDefaults setObject:self.txtCompanyCode.text forKey:@"SERVER_COMCODE"];
    [userDefaults setObject:self.txtPhone.text forKey:@"SERVER_PHONE"];
    [userDefaults synchronize];
    
    NSString *address = [NSString stringWithFormat:@"%@:1433",self.txtIP.text];
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM TB_COMPANY_%@",self.txtCompanyCode.text];
    
    SQLClient* client = [SQLClient sharedInstance];
    [client connect:address username:self.txtID.text password:self.txtPassword.text database:self.txtID.text completion:^(BOOL success) {
        if (success)
        {
            [client execute:query completion:^(NSArray* results) {
                for (NSArray* table in results)
                {
                    for (NSDictionary* row in table)
                    {
                        for (NSString* column in row)
                        {
                            NSLog(@"결과 %@=%@", column, row[column]);
                            if ([column isEqualToString:@"col_company_db_ip"]) {
                                [userDefaults setObject:row[column] forKey:@"SERVER_RESULT_IP"];
                                
                            }else if ([column isEqualToString:@"col_company_db_name"]) {
                                [userDefaults setObject:row[column] forKey:@"SERVER_RESULT_DBNAME"];
                                
                            }else if ([column isEqualToString:@"col_company_db_id"]) {
                                [userDefaults setObject:row[column] forKey:@"SERVER_RESULT_ID"];
                                
                            }else if ([column isEqualToString:@"col_company_db_pw"]) {
                                [userDefaults setObject:row[column] forKey:@"SERVER_RESULT_PASS"];
                                
                            }
                        }
                    }
                }
                [userDefaults synchronize];
                [client disconnect];
                [self.viewAlertServer setHidden:YES];
                return;
            }];
        }
    }];
    [self.viewAlertServer setHidden:YES];
}

#pragma mark - SQLClientErrorNotification
- (void)error:(NSNotification*)notification
{
    NSNumber* code = notification.userInfo[SQLClientCodeKey];
    NSString* message = notification.userInfo[SQLClientMessageKey];
    NSNumber* severity = notification.userInfo[SQLClientSeverityKey];
    
    NSLog(@"Error #%@: %@ (Severity %@)", code, message, severity);
}

#pragma mark - SQLClientMessageNotification
- (void)message:(NSNotification*)notification
{
    NSString* message = notification.userInfo[SQLClientMessageKey];
    NSLog(@"Message: %@", message);
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        [self serverSubmitButtonAble];
        return YES;
    }else if([self.txtIP.text length] > 50){
        return NO;
    }else if([self.txtID.text length] > 20){
        return NO;
    }else if([self.txtPassword.text length] > 20){
        return NO;
    }else if([self.txtCompanyCode.text length] > 20){
        return NO;
    }else if([self.txtPhone.text length] > 20){
        return NO;
    }
    [self serverSubmitButtonAble];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self serverSubmitButtonAble];
}

- (void)serverSubmitButtonAble {
    
    [NSTimer scheduledTimerWithTimeInterval:0.12 repeats:NO block:^(NSTimer * _Nonnull timer) {
        if ([self.txtIP.text length] > 0
            && [self.txtID.text length] > 0
            && [self.txtPassword.text length] > 0
            && [self.txtCompanyCode.text length] > 0
            && [self.txtPhone.text length] > 0) {
            
            self.buttonSvrOK.userInteractionEnabled = YES;
            self.buttonSvrOK.enabled = YES;
            self.buttonSvrOK.backgroundColor = [UIColor colorWithRed:72/255.0f green:185/255.0f blue:216/255.0f alpha:1.0];
        }else {
            self.buttonSvrOK.userInteractionEnabled = NO;
            self.buttonSvrOK.enabled = NO;
            self.buttonSvrOK.backgroundColor = [UIColor grayColor];
        }
    }];
}

- (NSString *)getVersion {
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *latestVersion =[infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return latestVersion;
}

@end
