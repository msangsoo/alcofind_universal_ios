//
//  DataGraphView.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 30..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilDensity.h"
#import "UtilDate.h"
#import "UtilScreen.h"
#import "DataDensity.h"

@interface DataGraphView : UIView

@property (assign, nonatomic) CGFloat pointX;
@property (assign, nonatomic) CGFloat lineWidth;
@property (assign, nonatomic) CGFloat lineHeightGap;
@property (assign, nonatomic) CGPoint pivotSize;

@property (retain, nonatomic) NSArray* arrayData;
@property (assign, nonatomic) NSUInteger level;
@property (assign, nonatomic) NSUInteger spacingY;
@property (assign, nonatomic) NSUInteger type;
@property (assign, nonatomic) BOOL flagLast;
@property (assign, nonatomic) BOOL flagDay;

- (void) drawClear;

@end
