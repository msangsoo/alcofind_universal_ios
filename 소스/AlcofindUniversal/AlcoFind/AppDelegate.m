//
//  AppDelegate.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 25..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//
// AlcoFind EN 1.0.0
//

#import "AppDelegate.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // 구글맵스
    //[GMSServices provideAPIKey:@"AIzaSyDs50OLZhzE0E1evNaGK2_32SGMiS7enWk"];
    [GMSServices provideAPIKey:@"AIzaSyAM-Paekx7AMCy-4JQBci-OQ84eoEpKPsI"];
    
    [self.window setBackgroundColor:[UIColor whiteColor]];
    
    // 기본 데이터 저장
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    // 0
    // DEVICE_UUID
    if (![userDefaults objectForKey:@"DEVICE_UUID"])
    {
        [userDefaults setObject:@"" forKey:@"DEVICE_UUID"];
    }
    
    // 1
    // ALARM_LIMIT_VALUE_LEVEL
    if (![userDefaults objectForKey:@"ALARM_LIMIT_VALUE_LEVEL"])
    {
        [userDefaults setInteger:2 forKey:@"ALARM_LIMIT_VALUE_LEVEL"];
    }

    // 2
    // WARNING_MESSAGE_FLAG
    if (![userDefaults objectForKey:@"WARNING_MESSAGE_FLAG"])
    {
        [userDefaults setBool:NO forKey:@"WARNING_MESSAGE_FLAG"];
    }
    
    // 2-1
    // WARNING_MESSAGE_FLAG_DATE
    if (![userDefaults objectForKey:@"WARNING_MESSAGE_FLAG_DATE"])
    {
        [userDefaults setObject:nil forKey:@"WARNING_MESSAGE_FLAG_DATE"];
    }
    
    // WARNING_LIMIT_VALUE_LEVEL
    if (![userDefaults objectForKey:@"WARNING_LIMIT_VALUE_LEVEL"])
    {
        [userDefaults setInteger:1 forKey:@"WARNING_LIMIT_VALUE_LEVEL"];
    }

    // LIMITED_NUMBER_VALUE
    if (![userDefaults objectForKey:@"LIMITED_NUMBER_VALUE"])
    {
        [userDefaults setInteger:1 forKey:@"LIMITED_NUMBER_VALUE"];
    }
    
    // 3
    // SOUND_FLAG
    if (![userDefaults objectForKey:@"SOUND_FLAG"])
    {
        [userDefaults setBool:NO forKey:@"SOUND_FLAG"];
    }
    
    // 4
    // CAMERA_FLAG
    if (![userDefaults objectForKey:@"CAMERA_FLAG"])
    {
        [userDefaults setBool:NO forKey:@"CAMERA_FLAG"];
    }
    
    // BAC      - 0.200    (density /= 10000)                  %0.3f
    // g/L(%)   - 2.000    (density /= 1000)                   %0.3f
    // mg/L     - 1.000    (density = (density /100)/21 )      %0.3f
    // ug/L     - 1000    (density /= @ )                      %3.0f
    // mg/100ml - 200     (density /= 10)                      %3.0f
    
    // 5
    // UNIT
    if (![userDefaults objectForKey:@"UNIT"])
    {
        //NSArray *array = @[@"BAC", @"g/L(%)", @"mg/L", @"ug/L", @"mg/100mL"];
        NSArray *arrayUnit = @[@"BAC", @"Promille", @"ug/L", @"mg/L", @"mg/100mL"];
        [userDefaults setValue:arrayUnit forKey:@"UNIT"];
    }
    
    // UNIT_MAX
    if (![userDefaults objectForKey:@"UNIT_MAX"])
    {
        NSArray *arrayUnitMax =
        @[
          @[@"0.00", @"0.01", @"0.02", @"0.03", @"0.04", @"0.05", @"0.06", @"0.07", @"0.08", @"0.09", @"0.10",
            @"0.11", @"0.12", @"0.13", @"0.14", @"0.15", @"0.16", @"0.17", @"0.18", @"0.19", @"0.20"],
          @[@"0.0", @"0.1", @"0.2", @"0.3", @"0.4", @"0.5", @"0.6", @"0.7", @"0.8", @"0.9", @"1.0",
            @"1.1", @"1.2", @"1.3", @"1.4", @"1.5", @"1.6", @"1.7", @"1.8", @"1.9", @"2.0"],
          @[@"0", @"50", @"100", @"150", @"200", @"250", @"300", @"350", @"400", @"450", @"500",
            @"550", @"600", @"650", @"700", @"750", @"800", @"850", @"900", @"950", @"1000"],
          @[@"0.00", @"0.05", @"0.10", @"0.15", @"0.20", @"0.25", @"0.30", @"0.35", @"0.40", @"0.45", @"0.50",
            @"0.55", @"0.60", @"0.65", @"0.70", @"0.75", @"0.80", @"0.85", @"0.90", @"0.95", @"1.00"],
          @[@"0", @"10", @"20", @"30", @"40", @"50", @"60", @"70", @"80", @"90", @"100",
            @"110", @"120", @"130", @"140", @"150", @"160", @"170", @"180", @"190", @"200"],
          ];
        [userDefaults setValue:arrayUnitMax forKey:@"UNIT_MAX"];
    }
    
    // UNIT_FORMAT
    if (![userDefaults objectForKey:@"UNIT_FORMAT"])
    {
        NSArray *arrayUnitFormat = @[@"%0.3f", @"%0.3f", @"%0.3f", @"%3.0f", @"%3.0f"];
        [userDefaults setValue:arrayUnitFormat forKey:@"UNIT_FORMAT"];
    }
    
    // UNIT_SELECTED
    if (![userDefaults objectForKey:@"UNIT_SELECTED"])
    {
        [userDefaults setInteger:-1 forKey:@"UNIT_SELECTED"];
    }
    
    // 6
    // LANGUAGE
    //if (![userDefaults objectForKey:@"LANGUAGE"])
    //{
        NSArray *array = @[@"English", @"Finnish", @"Franch", @"German", @"Norwegian", @"Polish", @"Russian", @"Spanish", @"Swedish", @"한국어"];
        [userDefaults setValue:array forKey:@"LANGUAGE"];
    //}
    
    // LANGUAGE_SELECTED
    if (![userDefaults objectForKey:@"LANGUAGE_SELECTED"])
    {
        [userDefaults setInteger:-1 forKey:@"LANGUAGE_SELECTED"];
    }

    [userDefaults synchronize];
    
    
    // 엔트리 포인트 설정
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    // 유저 데이터
    NSInteger intLanguageSelected = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    NSInteger intUnitSelected = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    // 초기 설정 화면으로
    if (intLanguageSelected > -1 && intUnitSelected > -1)
    {
        UIViewController* viewController = [storyboard instantiateViewControllerWithIdentifier:@"main"];
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
    }
    // 메인화면으로
    else
    {
        UIViewController* viewController = [storyboard instantiateViewControllerWithIdentifier:@"intro"];
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.greamsoft.AlcoFind" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AlcoFind" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AlcoFind.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
