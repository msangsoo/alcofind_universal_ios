//
//  SettingSlider.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 24..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "SettingSlider.h"

@implementation SettingSlider

// 초기화 생성자 : 스토리보드
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        // 컨트롤러 이미지 교체 (동그라미)
        UIImage* imageThumb = [UIImage imageNamed:@"slider03.png"];
        [self setThumbImage:imageThumb forState:UIControlStateNormal];
    }
    return self;
}


// 슬라이더 높이 조절
- (CGRect)trackRectForBounds:(CGRect)bounds
{
    CGRect rect1 = bounds;
    rect1.size = CGSizeMake(bounds.size.width, 6.0f);
    
    return rect1;
}


@end
