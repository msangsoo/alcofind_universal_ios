//
//  DeviceListViewController.swift
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 26..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

import UIKit

@objc class DeviceListViewController1: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    var arrayDevice: NSMutableArray = NSMutableArray();
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var noti = NSNotificationCenter.defaultCenter();
        noti.addObserver(self, selector: Selector("firstMethod:"), name: "notiDevice", object: nil);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "selectionCell") {
            
            let row = self.tableView.indexPathForSelectedRow()!.row;
            var dic:Dictionary<String,String>! = self.arrayDevice.objectAtIndex(row) as! Dictionary<String,String>;
            var noti = NSNotificationCenter.defaultCenter();
            noti.postNotificationName("connectDevice", object:self, userInfo:dic);
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrayDevice.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("deviceCell", forIndexPath: indexPath) as! DeviceListViewCell
        
        //cell.imageView?.image = UIImage(named: carImageArray[row] as! String);
        var dic:AnyObject! = self.arrayDevice.objectAtIndex(indexPath.row);
        cell.labelDeviceName.text = dic.objectForKey("NAME") as? String;
        cell.labelDeviceUUID.text = dic.objectForKey("UUID") as? String;
        
        return cell;

    }
    
    func firstMethod(noti:NSNotification) {
        var dic:AnyObject! = noti.userInfo;
        self.arrayDevice.addObject(dic);
        self.tableView.reloadData();
    }
    
    
    
    
}
