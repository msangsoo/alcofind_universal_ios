//
//  UITextViewCustom.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 16..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface UITextViewCustom : UITextView

@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;
@property (assign, nonatomic) IBInspectable UIColor* borderColor;

@end
