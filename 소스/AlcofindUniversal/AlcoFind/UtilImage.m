//
//  UtilImage.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 16..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "UtilImage.h"

@implementation UtilImage

// 캡쳐이미지 반환
+ (UIImage*) captureView:(UIView*)view
{
    //UIGraphicsBeginImageContext(self.viewBlowing.bounds.size); // 320 사이즈
    
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

// 이미지 Document 폴더에 저장
+ (void) saveImage:(UIImage*)image fileName:(NSString*)fileName
{
    NSArray* aryPath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString* path = [NSString stringWithFormat:@"%@/%@",[aryPath objectAtIndex:0], fileName];
    NSData* imageData = [NSData dataWithData:UIImageJPEGRepresentation(image,80)];
    [imageData writeToFile:path atomically:YES];
}

// 이미지 Document 폴더에서 불러오기
+ (UIImage*) loadImage:(NSString*)fileName
{
    NSArray* aryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString* path = [NSString stringWithFormat:@"%@/%@",[aryPath objectAtIndex:0], fileName];
    UIImage* image = [[UIImage alloc] initWithContentsOfFile:path];
    
    return image;
}

// 이미지 Document 폴더에서 삭제
+ (void) deleteImage:(NSString*)fileName
{
    NSArray* aryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString* path = [NSString stringWithFormat:@"%@/%@",[aryPath objectAtIndex:0], fileName];
    
    NSError *error = nil;
    [[NSFileManager defaultManager]removeItemAtPath:path error:&error];
    if (error) {
        NSLog(@"삭제실패: %@", error);
    } else {
        NSLog(@"삭제 완료: %@", error);
    }
}

// 이미지 크롭하기
+ (UIImage *)cropImage:(UIImage *)image cropRect:(CGRect)cropRect
{
    //다시 그리기(잘라줄 사이즈에 맞게)
    UIGraphicsBeginImageContext(cropRect.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // 높이에 맞게 스케일 조절
    CGContextTranslateCTM(ctx, 0.0, cropRect.size.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    
    // context에 뷰 그려주기
    CGFloat y = cropRect.origin.y - (image.size.height - cropRect.size.height);
    CGRect drawRect = CGRectMake(-cropRect.origin.x,  y, image.size.width, image.size.height);
    CGContextDrawImage(ctx, drawRect, image.CGImage);
    
    

    // context에서 새 이미지 생성
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 그리기 끝내기
    UIGraphicsEndImageContext();
    
    //새이미지 반영
    return newImage;
}


+ (UIImage *) cropImageCenter:(UIImage *)image withOrientation:(UIImageOrientation)imageOrientation

{
    CGSize centerSquareSize;
    double oriImgWid = CGImageGetWidth(image.CGImage);
    double oriImgHgt = CGImageGetHeight(image.CGImage);
    //NSLog(@"oriImgWid==[%.1f], oriImgHgt==[%.1f]", oriImgWid, oriImgHgt);
    if(oriImgHgt <= oriImgWid) {
        centerSquareSize.width = oriImgHgt;
        centerSquareSize.height = oriImgHgt;
    }else {
        centerSquareSize.width = oriImgWid;
        centerSquareSize.height = oriImgWid;
    }
    
    //NSLog(@"squareWid==[%.1f], squareHgt==[%.1f]", centerSquareSize.width, centerSquareSize.height);
    
    double x = (oriImgWid - centerSquareSize.width) / 2.0;
    double y = (oriImgHgt - centerSquareSize.height) / 2.0;
    //NSLog(@"x==[%.1f], x==[%.1f]", x, y);
    
    CGRect cropRect = CGRectMake(x, y, centerSquareSize.height, centerSquareSize.width);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:imageOrientation];
    CGImageRelease(imageRef);
    
    
    return cropped;
}

@end
