//
//  StartViewController.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 1..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "StartViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger intLanguageSelected = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    NSInteger intUnitSelected = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    if (intLanguageSelected > -1 && intUnitSelected > -1)
    {
        [self performSelector:@selector(segueMainViewController) withObject:nil afterDelay:0];
    }
    else
    {
        [self performSelector:@selector(segueIntroViewController) withObject:nil afterDelay:0];
    }
}

//////////////////////////////////////////////////
////////// MARK: - 기능
//////////////////////////////////////////////////

- (void) segueIntroViewController
{
    [self performSegueWithIdentifier:@"ToIntro" sender:self];
}


- (void) segueMainViewController
{
    [self performSegueWithIdentifier:@"ToMain" sender:self];
}

//////////////////////////////////////////////////
////////// MARK: - 메모리 관련
//////////////////////////////////////////////////

// 메모리 부족시
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
