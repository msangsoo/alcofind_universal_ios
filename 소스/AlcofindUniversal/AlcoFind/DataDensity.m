//
//  DataDensity.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 28..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataDensity.h"
#import "AppDelegate.h"

@implementation DataDensity

// 오브젝트 추가
+ (NSManagedObject*) addObject: (NSDictionary*)dic
{
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    NSManagedObject* obj = [NSEntityDescription insertNewObjectForEntityForName:@"Density" inManagedObjectContext:context];
  
    for (NSString* key in [dic allKeys])
    {
        [obj setValue:[dic valueForKey:key] forKey:key];
    }
    
    [appDelegate saveContext];
    
    return obj;
}

// 오브젝트 수정
+ (void) modifyObject: (NSManagedObject*)obj
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    
    [appDelegate saveContext];
}

// 오브젝트 삭제
+ (void) deleteObject: (NSManagedObject*)obj
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    // 사진 삭제 
    NSString* name = [obj valueForKey:@"photo_url"];
    [UtilImage deleteImage:name];
    
    [context deleteObject:obj];
    [appDelegate saveContext];
}

// 오브젝트 리스트 :
+ (NSArray*) selectAllObject
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    // 데이터 - 쿼리실행
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Density"];
    request.returnsObjectsAsFaults = false;
    
    // 데이터 결과
    NSArray* arrayResult = [NSArray array];
    arrayResult = [context executeFetchRequest:request error:nil];
    
    // 데이터 없을 경우
    if (arrayResult.count == 0)
    {
        //NSLog("데이터 없음.");
    }
    
    return arrayResult;
}



// 오브젝트 리스트 : 날짜 조건
+ (NSArray*) selectObject:(NSString*)type startDate:(NSDate*)startDate endDate:(NSDate*)endDate
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    // 데이터 - 쿼리실행
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Density"];
    request.returnsObjectsAsFaults = false;
    
    // 데이터 WHERE
    request.predicate = [NSPredicate predicateWithFormat:@"type=%@ AND (reg_date >= %@) AND (reg_date <= %@)", type, startDate, endDate];
    
    // 데이터 ORDER BY
    NSSortDescriptor* sort = [NSSortDescriptor sortDescriptorWithKey:@"reg_date" ascending:false];
    request.sortDescriptors = [NSArray arrayWithObject:sort];
    
    // 데이터 결과
    NSArray* arrayResult = [NSArray array];
    arrayResult = [context executeFetchRequest:request error:nil];
    
    // 데이터 없을 경우
    if (arrayResult.count == 0)
    {
        //NSLog("데이터 없음.");
    }
    
    return arrayResult;
}

// 오브젝트 리스트 :
+ (NSArray*) selectObject:(NSString*)type typeDate:(NSUInteger)typeDate
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    // 데이터 - 쿼리실행
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Density"];
    request.returnsObjectsAsFaults = false;
    
    // 데이터 WHERE : 일, 주, 월, 모두
    NSString* stringDate;
    
    // 데이터 : 오늘
    if (typeDate == 0)
    {
        stringDate = [UtilDate stringFormat:@"yyyy-MM-dd" date:[NSDate date]];
        request.predicate = [NSPredicate predicateWithFormat:@"type=%@ AND group_date BEGINSWITH %@", type, stringDate];
    }
    // 데이터 : 일주일전부터
    else if (typeDate == 1)
    {
//        NSDate* date = [UtilDate firstdayInWeek]; // 이번주 첫째날
        NSDate* date = [UtilDate day:-7];  // 7일전
        request.predicate = [NSPredicate predicateWithFormat:@"type=%@ AND reg_date >= %@", type, date];
    }
    // 데이터 : 이번달
    else if (typeDate == 2)
    {
        stringDate = [UtilDate stringFormat:@"yyyy-MM" date:[NSDate date]];
        request.predicate = [NSPredicate predicateWithFormat:@"type=%@ AND group_date BEGINSWITH %@", type, stringDate];
    }
    // 데이터 : 모두
    else if (typeDate == 3)
    {
        request.predicate = [NSPredicate predicateWithFormat:@"type=%@", type];
    }
    // 데이터 : 20일전부터
    else if (typeDate == 4)
    {
        //        NSDate* date = [UtilDate firstdayInWeek]; // 이번주 첫째날
        NSDate* date = [UtilDate day:-20];  // 7일전
        request.predicate = [NSPredicate predicateWithFormat:@"type=%@ AND reg_date >= %@", type, date];
    }

    
    // 데이터 ORDER BY
    NSSortDescriptor* sort = [NSSortDescriptor sortDescriptorWithKey:@"reg_date" ascending:true];
    request.sortDescriptors = [NSArray arrayWithObject:sort];
    
    // 데이터 결과
    NSArray* arrayResult = [NSArray array];
    arrayResult = [context executeFetchRequest:request error:nil];
    
    // 데이터 없을 경우
    if (arrayResult.count == 0)
    {
        //NSLog("데이터 없음.");
    }
    
    return arrayResult;
}


// 오브젝트 리스트 : 최근 몇개
+ (NSArray*) selectObject:(NSString*)type limit:(NSUInteger)limit 
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    // 데이터 - 쿼리실행
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Density"];
    request.returnsObjectsAsFaults = false;
    request.fetchLimit = limit;
    
    // 데이터 - 조건
    request.predicate = [NSPredicate predicateWithFormat:@"type=%@", type];
    
    // 데이터 ORDER BY
    NSSortDescriptor* sort = [NSSortDescriptor sortDescriptorWithKey:@"reg_date" ascending:false];
    request.sortDescriptors = [NSArray arrayWithObject:sort];
    
    // 데이터 결과
    NSArray* arrayResult = [NSArray array];
    arrayResult = [context executeFetchRequest:request error:nil];
    
    // 데이터 없을 경우
    if (arrayResult.count == 0)
    {
        //NSLog("데이터 없음.");
    }
    
    return arrayResult;
}

// 오브젝트 그룹 리스트
+ (NSArray*) selectGroupObject:(NSString*)type
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    // 데이터 - 쿼리실행
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Density"];
    
    // min, max, sum 사용
//    NSExpression* ex = [NSExpression expressionWithFormat:@"min(reg_date)"];
//    NSExpressionDescription* desc = [[NSExpressionDescription alloc] init];
//    [desc setName:@"reg_date"];
//    [desc setExpression:ex];
//    request.propertiesToFetch = @[@"group_date", desc];
    
    // 데이터 : 쿼리 조건
    request.propertiesToFetch = @[@"group_date"];
    request.propertiesToGroupBy = @[@"group_date"];
    request.returnsObjectsAsFaults = false;
    request.resultType = NSDictionaryResultType;
    
    // 데이터 WHERE
    request.predicate = [NSPredicate predicateWithFormat:@"type = %@", type];
    
    // 데이터 ORDER BY
    NSSortDescriptor* sort = [NSSortDescriptor sortDescriptorWithKey:@"group_date" ascending:false];
    request.sortDescriptors = [NSArray arrayWithObject:sort];
    
    // 데이터 : 그룹 쿼리 결과
    NSMutableArray* arrayGroup = [NSMutableArray array];
    arrayGroup = [[context executeFetchRequest:request error:nil] mutableCopy];
    
    // 데이터 : 최종 반환 데이터
    NSMutableArray* arrayResult = [NSMutableArray array];
    
    // for문
    for (int i=0; i<arrayGroup.count; i++)
    {
        // 결과 : GROUP BY
        NSDictionary* dicTemp = [arrayGroup objectAtIndex:i];
        NSString* stringGroupDate = [dicTemp valueForKey:@"group_date"];
        
        NSDate* date = [UtilDate dateFromString:stringGroupDate format:@"yyyy-MM-dd"];
        NSString* stringGroupDateTitle = [UtilDate stringFormat:@"MMM dd, yyyy" date:date]; //reg_date
        
        NSMutableDictionary* dicGroup = [NSMutableDictionary dictionary];
        [dicGroup setValue:stringGroupDate forKey:@"section"];
        [dicGroup setValue:stringGroupDateTitle forKey:@"sectionDate"];
        
        // 데이터 - 쿼리실행
        NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Density"];
        request.returnsObjectsAsFaults = false;

        // 데이터 WHERE
        request.predicate = [NSPredicate predicateWithFormat:@"type = %@ AND group_date = %@", type, stringGroupDate];
        
        // 데이터 ORDER BY
        NSSortDescriptor* sort = [NSSortDescriptor sortDescriptorWithKey:@"reg_date" ascending:false];
        request.sortDescriptors = [NSArray arrayWithObject:sort];
        
        // 데이터 결과
        NSArray* arrayRow = [context executeFetchRequest:request error:nil];
        [dicGroup setObject:arrayRow forKey:@"row"];
        [arrayResult addObject:dicGroup];
    }
    
    // 데이터 없을 경우
    if (arrayResult.count == 0)
    {
        //NSLog("데이터 없음.");
    }
    return arrayResult;
}

// 오브젝트 리스트 : 조건에 맞는 데이터 갯수
+ (NSUInteger) selectCount:(NSString*)type density:(NSUInteger)density date:(NSDate*)date
{
    // 컨택스트
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [appDelegate managedObjectContext];
    
    // 데이터 - 쿼리실행
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"Density"];
    request.returnsObjectsAsFaults = false;
    
    // 데이터 - 조건 : 이번달
   // NSString* stringDate = [UtilDate stringFormat:@"yyyy-MM" date:[NSDate date]];
    
    // 임시 날짜 설정
    // date = [UtilDate day:-31];
    
    //
    NSDate* dayPrev30 = [UtilDate day:-29];
    int compare = [UtilDate compareDate:dayPrev30 date2:date];

    if (compare < 0) {
        date = dayPrev30;
    }
    
    request.predicate = [NSPredicate predicateWithFormat:@"type=%@ AND reg_date >= %@ AND value >=%d", type, date, density];
    
    NSArray* arrayResult = [NSArray array];
    arrayResult = [context executeFetchRequest:request error:nil];
    
    // 데이터 결과
    NSUInteger count = [context countForFetchRequest:request error:nil];
    
    return count;
}

@end
