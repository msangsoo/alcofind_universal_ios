//
//  UtilScreen.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 22..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "UtilScreen.h"

@implementation UtilScreen

// 해상도 얻어오기
+ (NSUInteger) getInch
{
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    
    if      (height == 480.0f) return INCH_3_5;
    else if (height == 568.0f) return INCH_4_0;
    else if (height == 667.0f) return INCH_4_7;
    else if (height == 736.0f) return INCH_5_5;
    else if (height >= 812.0f) return INCH_5_8;
    else                       return INCH_5_5;
}


@end
