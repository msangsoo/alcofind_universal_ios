//
//  DataDensity.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 28..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "UtilDate.h"
#import "UtilImage.h"

@interface DataDensity : NSObject

+ (NSManagedObject*) addObject: (NSDictionary*)dic; // 오브젝트 추가
+ (void) modifyObject: (NSManagedObject*)obj; // 오브젝트 수정
+ (void) deleteObject: (NSManagedObject*)obj; // 오브젝트 삭제

// 오브젝트 리스트
+ (NSArray*) selectAllObject;
+ (NSArray*) selectObject:(NSString*)type typeDate:(NSUInteger)typeDate;
+ (NSArray*) selectObject:(NSString*)type startDate:(NSDate*)startDate endDate:(NSDate*)endDate;
+ (NSArray*) selectGroupObject:(NSString*)type;
+ (NSArray*) selectObject:(NSString*)type limit:(NSUInteger)limit;
+ (NSUInteger) selectCount:(NSString*)type density:(NSUInteger)density date:(NSDate*)date;

@end
