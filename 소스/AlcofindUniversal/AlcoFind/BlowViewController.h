//
//  BlowViewController.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 6..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "ArcView.h"
#import "TIBLECBKeyfob.h"
#import "DataDensity.h"
#import "UtilImage.h"
#import "UtilDate.h"
#import "UITextViewCustom.h"
#import "DataGraphView.h"
#import "DataGraphLabelY.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"
#import "UIButtonCustom.h"
#import "TestResultViewController.h"
#import "SQLClient.h"


@interface BlowViewController : UIViewController
    <TIBLEDelegate, UIImagePickerControllerDelegate,
    UINavigationControllerDelegate, CLLocationManagerDelegate, TestResultViewDelegate>
{
    UIImage *svrImage;
}
// 변수
@property (assign, nonatomic) NSUInteger number;
@property (assign, nonatomic) NSUInteger density;
@property (retain, nonatomic) NSManagedObject* managedObjectCurrent;
@property (retain, nonatomic) CLLocationManager *locationManager;
@property (retain, nonatomic) UIImagePickerController* imagePicker;
@property (retain, nonatomic) UITextView* avtiveTextView;
@property (retain, nonatomic) AVAudioPlayer* audioPlayer;

// 애니메이션
@property (strong, nonatomic) IBOutlet ArcView *arcView;
@property (strong, nonatomic) IBOutlet UILabel *labelNumber;
@property (strong, nonatomic) IBOutlet UILabel *labelText;

// 네비바
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UIView *viewDimmedNavBar;

// 컨텐츠 영역
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewContents;
@property (strong, nonatomic) IBOutlet UIView *viewContents;

// 그래프
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet DataGraphView *dataGraphView;
@property (strong, nonatomic) IBOutlet DataGraphLabelY *dataGraphLabelY;
//그래프 화살표 데이터 있을 시 표시(2019.01.07)
@property (strong, nonatomic) IBOutlet UIButton *leftBtn;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;

// 블루투스 통신
@property (retain, nonatomic) NSTimer* timerSend5;
@property (assign, nonatomic) NSUInteger timerCount;

// 컨테이너 뷰
@property (strong, nonatomic) IBOutlet UIView *viewBlowing;
@property (strong, nonatomic) IBOutlet UIView *viewAnalyzing;
@property (strong, nonatomic) IBOutlet UIView *viewLoading;
@property (strong, nonatomic) IBOutlet UIView *viewAlertInfo;

// Analyzing
@property (strong, nonatomic) IBOutlet UILabel *labelAnalyzing;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewGuage;

// 로딩창
@property (strong, nonatomic) IBOutlet UIImageView *imageViewLoading;

// 알림창 : Info
@property (strong, nonatomic) IBOutlet UILabel *labelInfoInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertInfo;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertInfo;

@property (assign, nonatomic) BOOL flagCameara;
@property (assign, nonatomic) BOOL flagTestResult;
@property (assign, nonatomic) BOOL flagBecome;

@property(retain, nonatomic) TabbarViewController* tabbarVC;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

@end
