//
//  UtilScreen.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 22..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilScreen : NSObject

// 5.5inch:11.0f / 4.7inch:10.0f  / 4inch : 9.0f

enum {
    INCH_3_5 = 0,
    INCH_4_0 = 1,
    INCH_4_7 = 2,
    INCH_5_5 = 3,
    INCH_5_8 = 4,
};
typedef NSUInteger INCH;

// 해상도 얻어오기
+ (NSUInteger) getInch;

@end
