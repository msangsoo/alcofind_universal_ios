//
//  GameLevel4ViewHeaderCell.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameLevel4ViewHeaderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelRanking;
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *labelEstimated;
@property (strong, nonatomic) IBOutlet UILabel *labelResult;

@end
