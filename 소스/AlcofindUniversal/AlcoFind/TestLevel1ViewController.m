//
//  TestLevel1ViewController.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 13..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "TestLevel1ViewController.h"

@interface TestLevel1ViewController ()

@end

@implementation TestLevel1ViewController

@synthesize labelTitle;
@synthesize buttonBack;
@synthesize buttonFriend;
@synthesize buttonMe;
@synthesize labelDday;
@synthesize labelCount;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

// viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = self;
    
    // 현재 상태 보내기(2019.01.16)
    keyfob = [TIBLECBKeyfob keyfob];
    [keyfob sendData:ALCOFIND_SEND_0B];
    
    self.labelTitle.text = [UtilLocal localString:@"TestTitle"];
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    [self.buttonMe setTitle:[UtilLocal localString:@"Me"] forState:UIControlStateNormal];
    [self.buttonFriend setTitle:[UtilLocal localString:@"Friend"] forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setSelectedButtonIndex:2];
        }
    }
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }

}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 값 초기화
    [TIBLECBKeyfob keyfob].dicTestInfo = [NSMutableDictionary dictionary];

    // 현재 상태 보내기
//    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
//    [keyfob sendData:ALCOFIND_SEND_0B];
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

// 블루투스 장치 연결 해제
- (void) disconnectedDevice
{
    //    NSString* s1 = [UtilLocal localString:@"DISCONNECT_TITLE"];
    //    NSString* s2 = [UtilLocal localString:@"DISCONNECT_MESSAGE"];
    //    NSString* s3 = [UtilLocal localString:@"Done"];
    //
    //    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:s1 message:s2 delegate:self cancelButtonTitle:s3 otherButtonTitles: nil];
    //    alert.alertViewStyle = UIAlertViewStyleDefault;
    //    [alert show];
    
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// 블루투스 Off
- (void) statePoweredOff
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트:
/////////////////////////////////////////////////

- (IBAction)touchUpButtonMe:(id)sender
{
    [self performSegueWithIdentifier:@"segueShowBlow" sender:self];
    
    // 정보 저장
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    [dic setValue:@"ME" forKey:@"type"];
}

- (IBAction)touchUpButtonFriend:(id)sender
{
    [self performSegueWithIdentifier:@"segueShowBlow" sender:self];
    
    // 정보 저장
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    [dic setValue:@"FR" forKey:@"type"];
}

//////////////////////////////////////////////////
////////// MARK: - 메모리 관련
//////////////////////////////////////////////////

// 메모리 부족시
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
