//
//  DataIntroViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"

@interface DataIntroViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UIButton *buttonGraph;
@property (strong, nonatomic) IBOutlet UIButton *buttonDetail;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;

@end
