//
//  DataGraphLabelY.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 30..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilScreen.h"

@interface DataGraphLabelY : UIView

@property (assign, nonatomic) NSUInteger level;
@property (assign, nonatomic) NSUInteger spacingY;

- (void) drawClear;

@end
