//
//  DataGraphViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataGraphViewController.h"

@interface DataGraphViewController ()

@end

@implementation DataGraphViewController

@synthesize scrollView;
@synthesize dataGraphView;
@synthesize dataGraphLabelY;
@synthesize type;
@synthesize buttonMe;
@synthesize buttonFriend;
@synthesize buttonDate;
@synthesize buttonBack;
@synthesize pickerDateSelectedIndex;
@synthesize arrayDate;
@synthesize labelCount;
@synthesize labelDday;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

// viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    NSString* s1 = [UtilLocal localString:@"Day"];
    NSString* s2 = [UtilLocal localString:@"Week"];
    NSString* s3 = [UtilLocal localString:@"Month"];
    NSString* s4 = [UtilLocal localString:@"All"];
    
    self.arrayDate = [@[s1, s2, s3, s4] mutableCopy];
    
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    [self.buttonMe setTitle:[UtilLocal localString:@"Mine"] forState:UIControlStateNormal];
    [self.buttonMe setTitle:[UtilLocal localString:@"Mine"] forState:UIControlStateSelected];
    [self.buttonMe setTitle:[UtilLocal localString:@"Mine"] forState:UIControlStateHighlighted];
    [self.buttonFriend setTitle:[UtilLocal localString:@"Friend"] forState:UIControlStateNormal];
    [self.buttonFriend setTitle:[UtilLocal localString:@"Friend"] forState:UIControlStateSelected];
    [self.buttonFriend setTitle:[UtilLocal localString:@"Friend"] forState:UIControlStateHighlighted];
    
    NSString* strTitle = [self.arrayDate objectAtIndex:0];
    [self.buttonDate setTitle:strTitle forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:0];
            [tabbarVC setSelectedButtonIndex:0];
        }
    }
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
    
    [self performSelector:@selector(initGraphwidth) withObject:nil afterDelay:0.1f];
    [self performSelector:@selector(setContentOffsetScrollView) withObject:nil afterDelay:0.1f];

}

// viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 기본 값
    self.type = @"ME";

    //
    NSArray* arrayData = [DataDensity selectObject:self.type typeDate:self.pickerDateSelectedIndex];
    self.dataGraphView.arrayData = arrayData;
    self.dataGraphView.flagLast = NO;
    self.dataGraphView.flagDay = YES;
    self.dataGraphView.level = 21;
    self.dataGraphLabelY.level = 21;

    // 3.5 inch
    if ([UtilScreen getInch] == INCH_3_5)
    {
        self.dataGraphView.spacingY = 10.0f;
        self.dataGraphLabelY.spacingY = 10.0f;
    }
    // 4 inch
    else if ([UtilScreen getInch] == INCH_4_0)
    {
        self.dataGraphView.spacingY = 14.0f;
        self.dataGraphLabelY.spacingY = 14.0f;
    }
    // 4.7 inch
    else if ([UtilScreen getInch] == INCH_4_7)
    {
        self.dataGraphView.spacingY = 17.0f;
        self.dataGraphLabelY.spacingY = 17.0f;
    }
    // 5.5 inch
    else if ([UtilScreen getInch] == INCH_5_5)
    {
        self.dataGraphView.spacingY = 20.0f;
        self.dataGraphLabelY.spacingY = 20.0f;
    }
    // 5.8 inch 1812 ko
    else if ([UtilScreen getInch] == INCH_5_8)
    {
        self.dataGraphView.spacingY = 20.0f;
        self.dataGraphLabelY.spacingY = 20.0f;
    }
    
    
}

- (void)initGraphwidth {
    CGSize sizeScrollView = self.scrollView.frame.size;
    CGSize size = CGSizeMake(self.dataGraphView.lineWidth, sizeScrollView.height);
    [self.scrollView setContentSize:size];
    
    //그래프 화살표 데이터 있을 시 표시(2019.01.14)
    
    CGFloat widthScrollView = scrollView.frame.size.width;
    CGFloat widthContent = scrollView.contentSize.width;
    
    if(widthContent <= widthScrollView){
        [self.leftBtn setHidden:YES];
        [self.rightBtn setHidden:YES];
    }else{
        [self.leftBtn setHidden:NO];
        [self.rightBtn setHidden:NO];
    }
}

// viewDidLayoutSubviews
- (void)viewDidLayoutSubviews
{
    
}

- (void) drawGraphAll
{
    [self.dataGraphLabelY drawClear];
    [self.dataGraphLabelY setNeedsDisplay];
    
    NSArray* arrayData = [DataDensity selectObject:self.type typeDate:self.pickerDateSelectedIndex];
    self.dataGraphView.arrayData = arrayData;
    
    [self.dataGraphView drawClear];
    [self.dataGraphView setNeedsDisplay];
    
    [self performSelector:@selector(initGraphwidth) withObject:nil afterDelay:0.1f];
    [self performSelector:@selector(setContentOffsetScrollView) withObject:nil afterDelay:0.1f];
}

// 스크롤뷰 : 맨 마지막으로 이동
- (void) setContentOffsetScrollView
{
    float offset = self.scrollView.contentSize.width-self.scrollView.frame.size.width;
    CGPoint point = CGPointMake(offset, 0.0f);
    [self.scrollView setContentOffset:point animated:NO];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 : 콜백
//////////////////////////////////////////////////

- (IBAction)touchUpButtonDate:(id)sender
{
    
    
    // 액션시트 버튼 클릭: 확인
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
    {
        self.pickerDateSelectedIndex = selectedIndex;
        NSString* strTitle = [self.arrayDate objectAtIndex:selectedIndex];
        [self.buttonDate setTitle:strTitle forState:UIControlStateNormal];
        
        if (selectedIndex == 0) {
            self.dataGraphView.flagDay = YES;
        } else {
            self.dataGraphView.flagDay = NO;
        }
        [self drawGraphAll];
    };
    // 액션시트 버튼 클릭: 취소
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker)
    {
    };
    
    // 액션시트
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:self.arrayDate initialSelection:self.pickerDateSelectedIndex doneBlock:done cancelBlock:cancel origin:self.buttonDate];
    
}

- (IBAction)touchButtonMe:(UIButton*)button
{
    if (button.selected) {
        return;
    }
    
    button.selected = YES;
    self.buttonFriend.selected = NO;
    
    self.type = @"ME";
    [self drawGraphAll];
}

- (IBAction)touchButtonFriend:(UIButton*)button
{
    if (button.selected) {
        return;
    }
    
    button.selected = YES;
    self.buttonMe.selected = NO;
    
    self.type = @"FR";
    [self drawGraphAll];
}

// 스크롤 : 왼쪽
- (IBAction)touchUpButtonLeft:(id)sender
{
    CGPoint point = scrollView.contentOffset;
    point.x = point.x - scrollView.frame.size.width;
    
    if (point.x < 0) {
        point.x = 0;
    }
    
    [self.scrollView setContentOffset:point animated:YES];
}

// 스크롤 : 오른쪽
- (IBAction)touchUpButtonRight:(id)sender
{
    CGFloat widthScrollView = scrollView.frame.size.width;
    CGFloat widthContent = scrollView.contentSize.width;
    
    CGPoint point = scrollView.contentOffset;
    point.x = point.x + widthScrollView;
    
    if (point.x > widthContent- widthScrollView) {
        point.x = widthContent- widthScrollView;
    }
    
    [self.scrollView setContentOffset:point animated:YES];
}

//////////////////////////////////////////////////
////////// MARK: - 메모리 관련
//////////////////////////////////////////////////

// 메모리 부족시
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
