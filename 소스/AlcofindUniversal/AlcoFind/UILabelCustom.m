//
//  UILabelCustom.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 25..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "UILabelCustom.h"

@implementation UILabelCustom

@synthesize cornerRadius;
@synthesize borderWidth;
@synthesize borderColor;

- (CGFloat)cornerRadius
{
    return self.layer.cornerRadius;
}

- (void)setCornerRadius:(CGFloat)newValue
{
    self.layer.cornerRadius = newValue;
    self.layer.masksToBounds = newValue > 0 ;
}

- (void)setBorderWidth:(CGFloat)newValue
{
    self.layer.borderWidth = newValue;
}

- (void)setBorderColor:(UIColor *)newValue
{
    self.layer.borderColor = newValue.CGColor;
}

@end
