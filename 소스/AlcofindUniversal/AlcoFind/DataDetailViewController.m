//
//  DataDetailViewController.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 22..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataDetailViewController.h"

@interface DataDetailViewController ()

@end

@implementation DataDetailViewController

@synthesize tableView;
@synthesize buttonFriend;
@synthesize buttonMe;
@synthesize flagEdit;
@synthesize arrayData;
@synthesize type;
@synthesize viewAlertMemo;
@synthesize textViewMemo;
@synthesize viewAlertPhoto;
@synthesize imageViewPhoto;

@synthesize labelMemoInAlertMemo;
@synthesize buttonOkInAlertMemo;
@synthesize labelPhotoInAlertPhoto;
@synthesize buttonOkInAlertPhoto;
@synthesize arrayUnit;

@synthesize buttonBack;
@synthesize buttonEdit;
@synthesize labelDeleteInAlertDel;
@synthesize labelTitleInAlertDel;
@synthesize buttonCancelInAlertDel;
@synthesize buttonOkInAlertDel;
@synthesize viewAlertDel;
@synthesize selectedCell;
@synthesize labelCount;

//////////////////////////////////////////////////
////////// MARK: - Segue
//////////////////////////////////////////////////

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueShowMap"])
    {
    
        DataMapViewController* vc = segue.destinationViewController;
        
//        NSIndexPath* indexPath = self.tableView.indexPathForSelectedRow;
        //선택 cell 값을 불러오기 위해 cell값 전달(2019.01.09)
        UITableViewCell* cell = (UITableViewCell*)sender;
        NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
        
        NSDictionary* dic = [self.arrayData objectAtIndex:indexPath.section];
        NSArray* arrayRow = [dic valueForKey:@"row"];
        NSManagedObject* obj = [arrayRow objectAtIndex:indexPath.row];
        
        // 좌표
        vc.geo_lon = [[obj valueForKey:@"geo_lon"] floatValue];
        vc.geo_lat = [[obj valueForKey:@"geo_lat"] floatValue];
    }
}

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

// viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    self.arrayData = [DataDensity selectGroupObject:type];
    [self.tableView reloadData];
    
    // 지역별 언어
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];

    [self.buttonMe setTitle:[UtilLocal localString:@"Mine"] forState:UIControlStateNormal];
    [self.buttonMe setTitle:[UtilLocal localString:@"Mine"] forState:UIControlStateSelected];
    [self.buttonMe setTitle:[UtilLocal localString:@"Mine"] forState:UIControlStateHighlighted];
    [self.buttonFriend setTitle:[UtilLocal localString:@"Friend"] forState:UIControlStateNormal];
    [self.buttonFriend setTitle:[UtilLocal localString:@"Friend"] forState:UIControlStateSelected];
    [self.buttonFriend setTitle:[UtilLocal localString:@"Friend"] forState:UIControlStateHighlighted];
    
    [self.buttonEdit setTitle:[UtilLocal localString:@"Edit"] forState:UIControlStateNormal];
    [self.buttonEdit setTitle:[UtilLocal localString:@"Edit"] forState:UIControlStateSelected];
    [self.buttonEdit setTitle:[UtilLocal localString:@"Edit"] forState:UIControlStateHighlighted];

    [self.buttonOkInAlertMemo setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.buttonOkInAlertPhoto setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    labelMemoInAlertMemo.text = [UtilLocal localString:@"Memo"];
    labelPhotoInAlertPhoto.text = [UtilLocal localString:@"Photo"];
    
    // 지역별 언어 : 얼럿 Delete
    [self.buttonOkInAlertDel setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.buttonCancelInAlertDel setTitle:[UtilLocal localString:@"Cancel"] forState:UIControlStateNormal];
    labelDeleteInAlertDel.text = [UtilLocal localString:@"Delete"];
    labelTitleInAlertDel.text = [UtilLocal localString:@"titleInAlertDelData"];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    self.arrayUnit = [[userDefaults arrayForKey:@"UNIT"] mutableCopy];
    for (int i=0; i<self.arrayUnit.count; i++)
    {
        NSString* s = [self.arrayUnit objectAtIndex:i];
        [self.arrayUnit replaceObjectAtIndex:i withObject:[UtilLocal localString:s]];
    }
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:0];
            [tabbarVC setSelectedButtonIndex:0];
        }
    }
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
}

// viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 기본 값
    self.type = @"ME";
    self.viewAlertMemo.hidden = YES;
    self.viewAlertPhoto.hidden = YES;
    [self.viewAlertDel setHidden:YES];
    
    self.buttonEdit.titleLabel.minimumScaleFactor = 0.5f;
    self.buttonEdit.titleLabel.numberOfLines = 1;
    self.buttonEdit.titleLabel.adjustsFontSizeToFitWidth = YES;
}

// viewDidLayoutSubviews
- (void)viewDidLayoutSubviews
{
    [self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 :
//////////////////////////////////////////////////

// 버튼 touchUp: Edit
- (IBAction)touchUpButtonEdit:(UIButton*)button
{
    button.selected = !button.selected;
    
    NSArray* array = [self.tableView indexPathsForVisibleRows];
    
    for (NSIndexPath* path in array) {
    
        DataDetailViewCell *cell = [self.tableView cellForRowAtIndexPath:path];
 
        if (button.selected)
        {
            [cell.buttonDelTrailling setConstant:0.0];
            [cell.buttonDel setHidden:NO];
        }
        else
        {
            [cell.buttonDelTrailling setConstant:-53.0];
            [cell.buttonDel setHidden:YES];
        }
    }
}

// 임시: 데이터 추가
- (IBAction)tuchUpNewData:(id)sender
{
    // 랜덤 데이터
    NSArray* arrayDate = @[[UtilDate day:-31], [UtilDate day:-30], [UtilDate day:-29], [UtilDate day:-28], [UtilDate day:-27]];
    NSArray* arrayValue = @[@"500", @"500", @"500", @"500", @"500"];
    NSArray* arrayType = @[@"ME", @"ME", @"ME", @"ME", @"ME"];
    
    for (int i=0; i<5; i++)
    {
        CGFloat random1 = drand48() * 5 -1;
        CGFloat random2 = drand48() * 5 -1;
        CGFloat random3 = drand48() * 5 -1;
        
        //
        NSUInteger num = [[arrayValue objectAtIndex:random1] integerValue];
        NSString* strType = [arrayType objectAtIndex:random2];
        NSDate* date = [arrayDate objectAtIndex:i];
        NSString* strDate = [UtilDate stringFormat:@"yyyy-MM-dd" date:date];
        
        //
        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
        [dic setObject:[NSNumber numberWithInteger:num] forKey:@"value"];
        [dic setObject:@"memo메모" forKey:@"memo"];
        [dic setObject:strType forKey:@"type"];
        [dic setObject:date forKey:@"reg_date"];
        [dic setObject:strDate forKey:@"group_date"];
        
        [DataDensity addObject:dic];
    }
    
    for (int i=0; i<5; i++)
    {
        CGFloat random1 = drand48() * 5 -1;
        CGFloat random2 = drand48() * 5 -1;
        CGFloat random3 = drand48() * 5 -1;
        
        //
        NSUInteger num = [[arrayValue objectAtIndex:random1] integerValue];
        NSString* strType = [arrayType objectAtIndex:random2];
        NSDate* date = [arrayDate objectAtIndex:i];
        NSString* strDate = [UtilDate stringFormat:@"yyyy-MM-dd" date:date];
        
        //
        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
        [dic setObject:[NSNumber numberWithInteger:num] forKey:@"value"];
        [dic setObject:@"memo메모" forKey:@"memo"];
        [dic setObject:strType forKey:@"type"];
        [dic setObject:date forKey:@"reg_date"];
        [dic setObject:strDate forKey:@"group_date"];
        
        [DataDensity addObject:dic];
    }
    
    NSDate* dday = [UtilDate day:-30];
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"WARNING_MESSAGE_FLAG"];
    [userDefaults setObject:dday forKey:@"WARNING_MESSAGE_FLAG_DATE"];
    [userDefaults synchronize];
    
    
    [self viewWillAppear:NO];
}

- (IBAction)touchButtonMe:(UIButton*)button
{
    button.selected = YES;
    self.buttonFriend.selected = NO;
    
    self.type = @"ME";
    [self viewWillAppear:NO];
}

- (IBAction)touchButtonFriend:(UIButton*)button
{
    button.selected = YES;
    self.buttonMe.selected = NO;
    
    self.type = @"FR";
    [self viewWillAppear:NO];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 : AlertMemo
//////////////////////////////////////////////////

- (IBAction)touchUpButtonCloseInAlertMemo:(id)sender
{
    self.viewAlertMemo.hidden = YES;
}

- (IBAction)touchUpButtonOkInAlertMemo:(id)sender
{
    self.viewAlertMemo.hidden = YES;
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 : AlertPhoto
//////////////////////////////////////////////////

- (IBAction)touchUpButtonCloseInAlertPhoto:(id)sender
{
    self.viewAlertPhoto.hidden = YES;
}

- (IBAction)touchUpButtonOkInAlertPhoto:(id)sender
{
    self.viewAlertPhoto.hidden = YES;
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 : TableViewCell
//////////////////////////////////////////////////
- (IBAction)touchUpButtonMap:(UIButton*)sender
{
    //선택 cell 값을 불러오기 위해 cell값 전달(2019.01.09)
    [self performSegueWithIdentifier:@"segueShowMap" sender:(UITableViewCell*)sender.superview.superview];
}

- (IBAction)touchUpButtonPhoto:(UIButton*)sender
{
    self.viewAlertPhoto.hidden = NO;
    
    // 인덱스 패스
    UITableViewCell* cell = (UITableViewCell*)sender.superview.superview;
    NSIndexPath* index = [self.tableView indexPathForCell:cell];
    
    NSDictionary* dic = [self.arrayData objectAtIndex:index.section];
    NSArray* arrayRow = [dic valueForKey:@"row"];
    
    NSManagedObject* obj = [arrayRow objectAtIndex:index.row];
    
    UIImage* image = [UtilImage loadImage:[obj valueForKey:@"photo_url"]];
    self.imageViewPhoto.image = image;
}

- (IBAction)touchUpButtonMemo:(UIButton*)sender
{
    self.viewAlertMemo.hidden = NO;
    
    // 인덱스 패스
    UITableViewCell* cell = (UITableViewCell*)sender.superview.superview;
    NSIndexPath* index = [self.tableView indexPathForCell:cell];
    
    NSDictionary* dic = [self.arrayData objectAtIndex:index.section];
    NSArray* arrayRow = [dic valueForKey:@"row"];
    
    NSManagedObject* obj = [arrayRow objectAtIndex:index.row];
    
    self.textViewMemo.text = [obj valueForKey:@"memo"];
}

- (IBAction)touchUpButtonDel:(UIButton*)sender
{
    self.selectedCell = (UITableViewCell*)sender.superview.superview;
    [self.viewAlertDel setHidden:NO];
}

- (IBAction)touchButtonOkInAlertDel:(id)sender
{
    NSIndexPath* index = [self.tableView indexPathForCell:self.selectedCell];
    NSDictionary* dic = [self.arrayData objectAtIndex:index.section];
    NSArray* arrayRow = [dic valueForKey:@"row"];
    
    NSManagedObject* obj = [arrayRow objectAtIndex:index.row];
    
    [DataDensity deleteObject:obj];
    [self viewWillAppear:NO];
    
    self.selectedCell = nil;
    [self.viewAlertDel setHidden:YES];
}

- (IBAction)touchButtonCloseInAlertDel:(id)sender
{
    self.selectedCell = nil;
    [self.viewAlertDel setHidden:YES];
}

- (IBAction)touchButtonCancelInAlertDel:(id)sender
{
    self.selectedCell = nil;
    [self.viewAlertDel setHidden:YES];
}

//////////////////////////////////////////////////
////////// MARK: - 테이블뷰 DataSource Delegate
//////////////////////////////////////////////////

//DataDetailViewHeaderCell

// 테이블뷰 : 섹션 개수 반환
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.arrayData.count;
}

// 테이블뷰 : 섹션당 셀 개수 반환
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary* dic = [self.arrayData objectAtIndex:section];
    NSArray* arrayRow = [dic valueForKey:@"row"];
    return arrayRow.count;
}

// 테이블뷰 : 헤더 높이
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}

// 테이블뷰 : 푸터 높이
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

// 테이블뷰 : 데이터 설정된 헤더 반환
- (UIView *)tableView:(UITableView *)tableView1 viewForHeaderInSection:(NSInteger)section
{
    DataDetailViewHeaderCell* headerCell = [tableView1 dequeueReusableCellWithIdentifier:@"DataDetailViewHeaderCell"];
    
    NSDictionary* dic = [self.arrayData objectAtIndex:section];
    NSString* strTitle = [dic valueForKey:@"sectionDate"];
    headerCell.labelDate.text = strTitle;
 
    return headerCell;
}

// 테이블뷰 : 데이터 설정된 셀 반환
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DataDetailViewCell";
    DataDetailViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[DataDetailViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    // 레이아웃 변경 : 수정모드
    if (self.buttonEdit.selected)
    {
        [cell.buttonDelTrailling setConstant:0.0];
        [cell.buttonDel setHidden:NO];
    }
    // 레이아웃 변경 : 일반모드
    else
    {
        [cell.buttonDelTrailling setConstant:-53.0];
        [cell.buttonDel setHidden:YES];
    }
    
    // 데이터 설정
    NSDictionary* dic = [self.arrayData objectAtIndex:indexPath.section];
    NSArray* arrayRow = [dic valueForKey:@"row"];
    
    NSManagedObject* obj = [arrayRow objectAtIndex:indexPath.row];
    
    // 데이터 : 날짜
    NSDate* date = [obj valueForKey:@"reg_date"];
    NSString* strDate = [UtilDate stringFormat:@"HH:mm" date:date];
    
    // 데이터 : 숫자
    int val = [[obj valueForKey:@"value"] intValue];
    
    NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    // EN 버전 / 84이하:0 1050이상:1050
    //1812ko
    
//    기존 EN버전에서 사용됬고, 최형배과장에 요구로 주석처리함(2018.12.14)
//    if ([deviceType isEqualToString:@"EN"]) {
//        if (val <= 84) {
//            val = 0;
//        }
//    }
    
    NSString* strValue = [UtilDensity convertDensity:val];
    
    // EN 버전 / 84이하:0 1050이상:1050
    // 1812ko
    if ([deviceType isEqualToString:@"EN"]) {
        if (val >= 1050) {
            strValue = [UtilLocal localString:@"HIGH"];
        }
    }else if([deviceType isEqualToString:@"AU"]) {
        if (val >= 4000) {
            strValue = [UtilLocal localString:@"HIGH"];
        }
    }
    
    
    // 데이터 : 메모
    NSString* strMemo = [obj valueForKey:@"memo"];
    if (strMemo == nil || [@"" isEqualToString:strMemo]) {
        [cell.buttonMemo setEnabled:NO];
    } else {
        [cell.buttonMemo setEnabled:YES];
    }
    
    // 데이터 : 사진
    NSString* strPhotoUrl = [obj valueForKey:@"photo_url"];
    if (strPhotoUrl == nil || [@"" isEqualToString:strPhotoUrl]) {
        [cell.buttonPhoto setEnabled:NO];
    } else {
        [cell.buttonPhoto setEnabled:YES];
    }
    
    // 데이터 : 지도
    float geo_lon = [[obj valueForKey:@"geo_lon"] floatValue];
    float geo_lat = [[obj valueForKey:@"geo_lat"] floatValue];
    if (geo_lon == 0.0f || geo_lat == 0.0f) {
        [cell.buttonMap setEnabled:NO];
    } else {
        [cell.buttonMap setEnabled:YES];
    }
    
    // 데이터 : 단독
    BOOL breathalyzer = [[obj valueForKey:@"breathalyzer"] boolValue];
    if (breathalyzer) {
        cell.labelBreath.text = [UtilLocal localString:@"TestedWithout"];
        cell.viewBreathanylazer.hidden = NO;
        cell.viewTime.hidden = YES;
    } else {
        cell.viewBreathanylazer.hidden = YES;
        cell.viewTime.hidden = NO;
    }
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger selectedIndex = [userDefaults integerForKey:@"UNIT_SELECTED"];
    NSString* unit = [self.arrayUnit objectAtIndex:selectedIndex];
    cell.labelUnit.text = unit;
    
    // 데이터 : 셀
    cell.labelTime.text = strDate;
    cell.labelValue.text = strValue;
    //cell.labelValue.text = [NSString stringWithFormat:@"%d:%@", val, strValue];
    
    return cell;
}

@end
