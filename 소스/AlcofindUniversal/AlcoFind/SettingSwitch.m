//
//  SettingSwitch.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 25..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "SettingSwitch.h"

@implementation SettingSwitch

// 초기화 생성자 : 스토리보드
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}


// 스위치 폭 높이 조절
- (CGRect)trackRectForBounds:(CGRect)bounds
{
    CGRect rect1 = bounds;
    rect1.size = CGSizeMake(bounds.size.width, 7.0f);
    
    return rect1;
}

@end
