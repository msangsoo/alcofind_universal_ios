//
//  DeviceListViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 25..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) NSMutableArray* arrayDevice;



@end
