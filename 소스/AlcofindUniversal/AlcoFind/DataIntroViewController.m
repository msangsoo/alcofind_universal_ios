//
//  DataIntroViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataIntroViewController.h"

@interface DataIntroViewController ()

@end

@implementation DataIntroViewController

@synthesize buttonBack;
@synthesize buttonGraph;
@synthesize buttonDetail;
@synthesize labelCount;
@synthesize labelDday;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob sendData:ALCOFIND_SEND_11];

    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    [self.buttonGraph setTitle:[UtilLocal localString:@"Graph"] forState:UIControlStateNormal];
    [self.buttonDetail setTitle:[UtilLocal localString:@"Detail"] forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:0];
            [tabbarVC setSelectedButtonIndex:0];
        }
    }
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


@end
