//
//  NSData+HexRepresentation.h
//  AlcoFind
//
//  Created by Daesun moon on 12/08/2019.
//  Copyright © 2019 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSData (HexRepresentation)
- (NSString *)hexString;
-(NSString*)hexRepresentationWithSpaces_AS:(BOOL)spaces;
@end
