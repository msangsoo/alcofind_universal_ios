//
//  DataGraphView.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 30..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataGraphView.h"

@implementation DataGraphView

@synthesize pointX;
@synthesize lineWidth;
@synthesize lineHeightGap;
@synthesize pivotSize;

@synthesize arrayData;
@synthesize level;
@synthesize flagLast;
@synthesize spacingY;
@synthesize type;
@synthesize flagDay;

- (void) drawClear
{
    [[self subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
}

- (void) drawGraph:(CGRect)rect
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSUInteger intLevel = [userDefaults integerForKey:@"ALARM_LIMIT_VALUE_LEVEL"];
    
    // 가이드 : UI 관련 변수
    float startY = 0.0f;
    
    // 라벨 : UI 변수
    float startXLabel = 0.0f;
    float spacingXLabel = 0.0f;
    float fontsizeLabel = 0.0f;
    
    // 데이터 : UI 변수
    float dataWidth = 0.0f;
    float startX = 0.0f;
    float spacingX = 0.0f;
    float dataHeightRate = 0.0f;
   
    // 5.5inch:11.0f / 4.7inch:10.0f  / 4inch : 9.0f
    // 3.5 inch
    if ([UtilScreen getInch] == INCH_3_5)
    {
        startY = 5.0f;
        
        // 라벨 : UI 변수
        startXLabel = -2.5f;
        spacingXLabel = 22.0f;
        fontsizeLabel = 8.0f;
        
        // 데이터 : UI 변수
        dataWidth = 3.0f;
        startX = 7.0f;
        spacingX = 18.0f;
        dataHeightRate = 1.0f;
    }
    // 4 inch
    else if ([UtilScreen getInch] == INCH_4_0)
    {
        startY = 7.0f;
        
        // 라벨 : UI 변수
        startXLabel = -2.5f;
        spacingXLabel = 22.0f;
        fontsizeLabel = 8.0f;
        
        // 데이터 : UI 변수
        dataWidth = 3.0f;
        startX = 7.0f;
        spacingX = 18.0f;
        dataHeightRate = 1.4f;
    }
    // 4.7 inch
    else if ([UtilScreen getInch] == INCH_4_7)
    {
        startY = 9.0f;
        
        // 라벨 : UI 변수
        startXLabel = -1.0f;
        spacingXLabel = 25.0f;
        fontsizeLabel = 9.0f;
        
        // 데이터 : UI 변수
        dataWidth = 3.0f;
        startX = 10.0f;
        spacingX = 21.0f;
        dataHeightRate = 1.7f;
    }
    // 5.5 inch
    else if ([UtilScreen getInch] == INCH_5_5)
    {
        startY = 10.0f;
        
        // 라벨 : UI 변수
        startXLabel = -1.5f;
        spacingXLabel = 26.0f;
        fontsizeLabel = 11.0f;
        
        // 데이터 : UI 변수
        dataWidth = 3.0f;
        startX = 10.0f;
        spacingX = 26.0f;
        dataHeightRate = 2.0f;
    }
    // 5.8 inch 1812 ko
    else if ([UtilScreen getInch] == INCH_5_8)
    {
        startY = 11.0f;
        
        // 라벨 : UI 변수
        startXLabel = -1.5f;
        spacingXLabel = 26.0f;
        fontsizeLabel = 11.0f;
        
        // 데이터 : UI 변수
        dataWidth = 3.0f;
        startX = 10.0f;
        spacingX = 31.0f;
        dataHeightRate = 2.3f;
    }
    
    dataHeightRate = self.spacingY/10.0f;
    
    // 피벗 구하기
    CGPoint point = CGPointMake(0.0f, startY+(spacingY*(level-1)));
    self.pivotSize = point;
    
    // 그래프 가로 구하기
    self.lineWidth = rect.size.width;
    
    // 버그수정: count가 -일 경우 실수가 너무 커짐 방지
    int count = arrayData.count-1;
    if(count < 0){ count = 0; }
    
    CGRect frame = CGRectMake(startXLabel+(spacingX*count), 0.0f, spacingXLabel, 0.0f);
    if (self.lineWidth < frame.origin.x + frame.size.width) {
        self.lineWidth = frame.origin.x + frame.size.width;
    }
    
    // 가이드 : 그리기
    for (int i=0; i<level; i++)
    {
        // 가이드 : 사용자 기준
        if (i == level-intLevel-1)
        {
            CGRect frame = CGRectMake(0.0f, startY+(spacingY*i), self.lineWidth, 1.0f);
            UIView* view = [[UIView alloc] initWithFrame:frame];
            [view setBackgroundColor:[UIColor colorWithRed:0.28f green:0.72f blue:0.84f alpha:1.0f]];
            [self addSubview:view];
        }
        // 가이드 : 일반
        else
        {
            CGRect frame = CGRectMake(0.0f, startY+(spacingY*i), self.lineWidth, 1.0f);
            UIView* view = [[UIView alloc] initWithFrame:frame];
            [view setBackgroundColor:[UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:1.0f]];
            [self addSubview:view];
        }
    }
    
    NSString* dayCurrent = @"";
    NSString* monthCurrent = @"";
    NSString* hourCurrent = @"";
    NSString* minCurrent = @"";
    
    NSInteger index = [userDefaults integerForKey:@"LANGUAGE_SELECTED"];
    NSString* locale = @"en";

    // 영어로 통일
//    if      (index == 0) { locale = @"en"; }
//    else if (index == 1) { locale = @"de"; }
//    else if (index == 2) { locale = @"pl"; }
//    else if (index == 3) { locale = @"sv"; }
//    else if (index == 4) { locale = @"fr"; }
//    else if (index == 5) { locale = @"es"; }
    
    
    // 라벨 : 그리기
    for (int i = 0; i<self.arrayData.count; i++)
    {
        NSManagedObject* obj = [self.arrayData objectAtIndex:i];
        NSDate* date = [obj valueForKey:@"reg_date"];
        
        //
        NSString* strDate = @"";
        NSString* strDay = @"";
        NSString* strMonth = @"";
        
        NSString* strHour = @"";
        NSString* strMin = @"";
        
        
        if (self.flagDay == YES) {
        
            strDate = [UtilDate stringFormat:@"HH\nmm" date:date];
            strMin = [UtilDate stringFormat:@"\nmm" date:date];
            strHour = [UtilDate stringFormat:@"HH\n" date:date];
            
            // 같은시간
            if ([strHour isEqualToString:hourCurrent] && ![hourCurrent isEqualToString:@""]) {
                strDate = strMin;
            }
            hourCurrent = strHour;
            minCurrent = strMin;
            
            // 단독 데이터일 경우
            BOOL breath = [[obj valueForKey:@"breathalyzer"] boolValue];
            if (breath) {
                strDate = @"D\n";
                hourCurrent = @"";
            } else
            {
                NSLog(@"");
            }
            
            
        } else {
            
            strDate = [UtilDate stringFormat:@"d\nMMM" date:date locale:locale];
            strDay = [UtilDate stringFormat:@"d\n" date:date];
            strMonth = [UtilDate stringFormat:@"MMM" date:date locale:locale];
            
            // 같은달
            if ([strMonth isEqualToString:monthCurrent] && ![monthCurrent isEqualToString:@""]) {
                strDate = strDay;
            }
            
            dayCurrent = strDay;
            monthCurrent = strMonth;
            
            // 단독 데이터일 경우
            BOOL breath = [[obj valueForKey:@"breathalyzer"] boolValue];
            if (breath) {
                strDate = @"D\n";
                monthCurrent = @"";
            }else
            {
                NSLog(@"");
            }
        }
        
        CGRect frame = CGRectMake(startXLabel+(spacingX*i), self.pivotSize.y+1.0f, spacingXLabel, spacingXLabel);
        UILabel* label = [[UILabel alloc] initWithFrame:frame];
        
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont fontWithName:@"Roboto-Medium" size:fontsizeLabel]];
        
        if (flagLast && i == arrayData.count-1) {
            [label setTextColor:[UIColor colorWithRed:0.17 green:0.86 blue:1.0 alpha:1.0]];
        } else {
            [label setTextColor:[UIColor whiteColor]];
        }
        
        [label setNumberOfLines:2];
        [label setText:strDate];
        
        [self addSubview:label];
    }
    
    
    
    NSLog(@"dataHeightRate:%f", dataHeightRate);
          
    // 데이터 : 그리기
    for (int i=0; i<arrayData.count; i++)
    {
        // 데이터
        NSManagedObject* obj = [arrayData objectAtIndex:i];
        float value = [[obj valueForKey:@"value"] floatValue];
        
        //NSLog(@"value:%f %f %f", value, [UtilDensity convertDensityGraphFloat:value], [UtilDensity convertDensityGraphFloat:value]*dataHeightRate);
        
        // EN 버전 / 84이하:0 1050이상:1050
        // 1812 ko
        
//        기존 EN버전에서 사용됬고, 최형배과장에 요구로 주석처리함(2018.12.14)
//        NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
//        if ([deviceType isEqualToString:@"EN"]) {
//            if (value <= 84) {
//                value = 0;
//            }
//            if (value >= 1000) {
//                value = 1050;
//            }
//        }
        
        
        value = [UtilDensity convertDensityGraphFloat:value];
        value *= dataHeightRate;
        
        
        //////////// 임시 /////////////
        //value *= 10000;
        //////////// 임시 /////////////
        
        CGRect frame = CGRectMake(startX, self.pivotSize.y-value, dataWidth, value);
        UIView* view = [[UIView alloc] initWithFrame:frame];
        if (flagLast && i == arrayData.count-1) {
            [view setBackgroundColor:[UIColor colorWithRed:0.17 green:0.86 blue:1.0 alpha:1.0]];
        } else {
            [view setBackgroundColor:[UIColor whiteColor]];
        }
        [self addSubview:view];
        
        startX += spacingX;
    }
}

- (void)drawRect:(CGRect)rect
{
    [self drawGraph:rect];
}

@end
