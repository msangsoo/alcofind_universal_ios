//
//  DeviceListViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 25..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DeviceListViewController.h"
#import "MainViewController.h"
#import "DeviceListViewCell.h"

@interface DeviceListViewController ()

@end

@implementation DeviceListViewController
@synthesize arrayDevice;

// 뷰 화면에 보여질 때
- (void)viewDidLoad
{
    [super viewDidLoad];

    // 노티피케이션 이벤트 -> 메소드 실행
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addDeviceList:) name:@"findDevice" object: nil];
    
    self.arrayDevice = [NSMutableArray array];
}


// 노티피케이션 이벤트 -> 디바이스 목록 테이블뷰에 추가
- (void)addDeviceList:(NSNotification *)notification
{
    NSDictionary* dic = [notification userInfo];
    [self.arrayDevice addObject:dic];
    [self.tableView reloadData];
}

// 테이블뷰 : 섹션당 셀 개수 반환
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayDevice count];
}

// 테이블뷰 : 데이터 설정된 셀 반환
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"deviceCell";
    DeviceListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[DeviceListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // 데이터 설정
    NSDictionary* dic = [self.arrayDevice objectAtIndex:[indexPath row]];
    cell.labelDeviceName.text = [dic objectForKey:@"NAME"];
    cell.labelDeviceUUID.text = [dic objectForKey:@"UUID"];
    
    return cell;
}

// 다른 뷰로 이동하기 전 실행
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // 디바이스 선택시
    if ([segue.identifier isEqualToString:@"selectionCell"])
    {
        NSDictionary* dic = [self.arrayDevice objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"connectDevice" object:self userInfo:dic];
    }
}

// 메모리 부족시 실행
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 메모리 해제시
- (void)dealloc {
    
    // 노티피케이션 설정해제
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
