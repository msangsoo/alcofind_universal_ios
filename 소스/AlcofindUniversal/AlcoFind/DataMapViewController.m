//
//  DataMapViewController.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 18..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "DataMapViewController.h"

@interface DataMapViewController ()

@end

@implementation DataMapViewController

@synthesize geo_lat;
@synthesize geo_lon;
@synthesize mapView;
@synthesize buttonBack;
@synthesize labelCount;
@synthesize labelDday;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:0];
            [tabbarVC setSelectedButtonIndex:0];
        }
    }
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
}

// viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];

    GMSCameraPosition* camera = [GMSCameraPosition cameraWithLatitude:self.geo_lat longitude:self.geo_lon zoom:14];
    
    self.mapView.camera = camera;
    self.mapView.myLocationEnabled = YES;
    
    // 마커
    GMSMarker* marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(self.geo_lat, self.geo_lon);
    //    marker.title = @"타이틀";
    //    marker.snippet = @"오스트리아";
    marker.map = mapView;
    NSLog(@"1:%f", self.view.frame.size.width);
    NSLog(@"1:%@", self.mapView);
    [self.view addSubview:mapView];
}


//////////////////////////////////////////////////
////////// MARK: - 메모리 관련
//////////////////////////////////////////////////

// 메모리 부족시
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
