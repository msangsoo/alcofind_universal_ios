//
//  UtilColor.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 21..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilColor : NSObject

+ (UIColor*)hex2RGB:(NSString*)hexValue;
+ (UIColor*)hex2RGB:(NSString*)hexValue andAlpha:(float)alpha;

@end
