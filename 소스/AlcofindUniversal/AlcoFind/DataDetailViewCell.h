//
//  DataDetailViewCell.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataDetailViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *vLine2Trailling;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonMemoTrailling;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonDelTrailling;

@property (strong, nonatomic) IBOutlet UIButton *buttonMap;
@property (strong, nonatomic) IBOutlet UIButton *buttonPhoto;
@property (strong, nonatomic) IBOutlet UIButton *buttonMemo;
@property (strong, nonatomic) IBOutlet UIButton *buttonDel;

@property (strong, nonatomic) IBOutlet UILabel *labelTime;
@property (strong, nonatomic) IBOutlet UILabel *labelValue;
@property (strong, nonatomic) IBOutlet UILabel *labelUnit;
@property (strong, nonatomic) IBOutlet UILabel *labelBreath;

@property (strong, nonatomic) IBOutlet UIView *viewBreathanylazer;
@property (strong, nonatomic) IBOutlet UIView *viewTime;

@end
