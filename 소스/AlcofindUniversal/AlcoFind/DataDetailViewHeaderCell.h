//
//  DataDetailViewHeaderCell.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 29..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataDetailViewHeaderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelDate;

@end
