//
//  RootSegue.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 21..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "RootSegue.h"

@implementation RootSegue

- (void)perform
{
    [self.sourceViewController.navigationController popToRootViewControllerAnimated:YES];
}

@end
