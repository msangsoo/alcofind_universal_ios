//
//  TestResultViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 11. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "TestResultViewController.h"

@interface TestResultViewController ()

@end

@implementation TestResultViewController

@synthesize density;
@synthesize labelNumber;
@synthesize labelText;
@synthesize managedObjectCurrent;
@synthesize viewPhoto;
@synthesize viewBUttonSNS;
@synthesize imageViewPhoto;
@synthesize viewCapture;
@synthesize textViewMemo;
@synthesize avtiveTextView;
@synthesize scrollView;
@synthesize dataGraphView;
@synthesize dataGraphLabelY;
@synthesize scrollViewContents;
@synthesize labelResultDate;
@synthesize viewContents;
@synthesize viewAlertWarning;
@synthesize buttonBack;
@synthesize labelMemoInAlertMemo;
@synthesize buttonOkInAlertMemo;
@synthesize labelTitleInAlertWarning;
@synthesize labelMessageInAlertWarning;
@synthesize buttonOkInAlertWarning;
@synthesize viewResult;
@synthesize delegate;
@synthesize labelDday;
@synthesize labelCount;
@synthesize viewDimmedNavBar;
@synthesize tabbarVC;
@synthesize flagMove;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //NSLog(@"123");
    
    if ([segue.identifier isEqualToString:@"popSegueBlow"])
    {
      //  NSLog(@"456");
        [delegate setStateBlowing];
    }
    
}

// viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = self;

    //
    self.labelMemoInAlertMemo.text = [UtilLocal localString:@"Memo"];
    self.labelTitleInAlertWarning.text = [UtilLocal localString:@"WarningTitle"];
    self.labelMessageInAlertWarning.text = [UtilLocal localString:@"WarningMessage"];

    [self.textViewMemo setPlaceholder:[UtilLocal localString:@"hintMemo"]];
    [self.buttonOkInAlertMemo setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.buttonOkInAlertWarning setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    [self.buttonBack setTitle:[UtilLocal localString:@"Retest"] forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            self.tabbarVC = (TabbarViewController*)vc;
            [self.tabbarVC setSelectedButtonIndex:2];
        }
    }

    // 네비바 설정 : 카운트, 디데이 정보
    [self setNavBarInfo];
    
    //EN 추가본
    NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    if ([deviceType isEqualToString:@"EN"]) {
        self.flagMove = NO;
        [self performSelector:@selector(setEnableControls) withObject:nil afterDelay:10.5f];
    }else if([deviceType isEqualToString:@"AU"]){
        //AU일경우 대기시간 15초 (18.12.17)
        self.flagMove = NO;
        [self performSelector:@selector(setEnableControls) withObject:nil afterDelay:15.0f];
        
    }else {
        [self performSelector:@selector(setEnableControls) withObject:nil afterDelay:0];
    }
    
    [self performSelector:@selector(initGraphwidth) withObject:nil afterDelay:0.1f];
    [self performSelector:@selector(setContentOffsetScrollView) withObject:nil afterDelay:0.1f];
}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    // 백버튼 SNS 딤드 2019.01.03
    [self.viewDimmedNavBar setHidden:NO];
    [self.viewDimmendSNS setHidden:NO];
    [self.viewDimmedRestart setEnabled:NO];
    // 탭바 딤드 2019.01.07
    NSDictionary *notiDic = [[NSDictionary alloc] initWithObjectsAndKeys:@"NO",@"dimmed", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"tabbarDimmed" object:nil userInfo:notiDic];
//    [self.tabbarVC.viewDimmed setHidden:NO];
    
    
    [self.viewPhoto setHidden:YES];
    [self.viewAlertMemo setHidden:YES];
    [self.viewAlertWarning setHidden:YES];

    
    self.buttonBack.titleLabel.minimumScaleFactor = 0.5f;
    self.buttonBack.titleLabel.numberOfLines = 1;
    self.buttonBack.titleLabel.adjustsFontSizeToFitWidth = YES;


    self.labelResultDate.text = [UtilDate stringFormat:@"MMM dd, yyyy"];
    self.textViewMemo.delegate = self;
    
    // 유저 데이터 : 카메라 설정
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagCamera = [userDefaults boolForKey:@"CAMERA_FLAG"];
    
    if (flagCamera){
        [self.labelText setHidden:YES];
        [self.viewPhoto setHidden:NO];
    } else {
        [self.labelText setHidden:NO];
        [self.viewPhoto setHidden:YES];
    }
    
    //
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    NSString* photo_url = [dic valueForKey:@"photo_url"];
    UIImage* image = [UtilImage loadImage:photo_url];
    self.imageViewPhoto.image = image;
    
    // 결과 값
    NSArray* arrayUnit = [userDefaults arrayForKey:@"UNIT"];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    NSString* unit  = [arrayUnit objectAtIndex:intUnit];
    NSString* strTest = [UtilLocal localString:@"TestResult"];
    
    NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    // EN 버전 / 84이하:0 1050이상:1050
    // 1812 ko
    CGFloat val = self.density;
    
    //임시
//    val = 500;
    
//    기존 EN버전에서 사용됬고, 최형배과장에 요구로 주석처리함(2018.12.14)
//    if ([deviceType isEqualToString:@"EN"]) {
//        if (val <= 84) {
//            val = 0;
//        }
//    }
    NSString* strValue = [UtilDensity convertDensity:val];
        
    // EN 버전 / 84이하:0 1050이상:1050
    // 1812 ko
    if ([deviceType isEqualToString:@"EN"]) {
        if (val >= 1050) {
            strValue = [UtilLocal localString:@"HIGH"];
        }
    }else if([deviceType isEqualToString:@"AU"]) {
        if (val >= 4000) {
            strValue = [UtilLocal localString:@"HIGH"];
        }
    }
    
    self.labelNumber.text = strValue;
    self.labelText.text = [NSString stringWithFormat:@"[%@]\n%@", [UtilLocal localString:unit], strTest];
    
    // 그래프
    NSString* type = [dic valueForKey:@"type"];
    NSArray* arrayData = [DataDensity selectObject:type limit:20];
    arrayData = [[arrayData reverseObjectEnumerator] allObjects];
    
    // 그래프 : 설정값
    self.dataGraphView.arrayData = arrayData;
    self.dataGraphView.flagLast = NO;
    
    // STD일 경우 그래프 0.08까지 표시(18.12.17)
    NSString *DEVICE_TYPE = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    if ([DEVICE_TYPE isEqualToString:@"ST"] || [DEVICE_TYPE isEqualToString:@"ST_SVR"]) {
        self.dataGraphView.level = 9;
        self.dataGraphLabelY.level = 9;
    }else{
        self.dataGraphView.level = 11;
        self.dataGraphLabelY.level = 11;
    }

    // 3.5 inch
    if ([UtilScreen getInch] == INCH_3_5)
    {
        self.dataGraphView.spacingY = 9.0f;
        self.dataGraphLabelY.spacingY = 9.0f;
    }
    // 4 inch
    else if ([UtilScreen getInch] == INCH_4_0)
    {
        self.dataGraphView.spacingY = 10.0f;
        self.dataGraphLabelY.spacingY = 10.0f;
    }
    // 4.7 inch
    else if ([UtilScreen getInch] == INCH_4_7)
    {
        self.dataGraphView.spacingY = 12.0f;
        self.dataGraphLabelY.spacingY = 12.0f;
    }
    // 5.5 inch
    else if ([UtilScreen getInch] == INCH_5_5)
    {
        self.dataGraphView.spacingY = 13.0f;
        self.dataGraphLabelY.spacingY = 13.0f;
    }
    // 5.8 inch 1812 ko
    else if ([UtilScreen getInch] == INCH_5_8)
    {
        self.dataGraphView.spacingY = 13.0f;
        self.dataGraphLabelY.spacingY = 13.0f;
    }
    
    // 그래프 : 그래프 마지막 데이터 색 변경
    self.dataGraphView.flagLast = YES;
    [self drawGraphAll];
    
    
    // 헬스케어 메세지
    BOOL flag = [userDefaults boolForKey:@"WARNING_MESSAGE_FLAG"];
    if (flag)
    {
        // 설정한 기준치값
        NSUInteger level = [userDefaults integerForKey:@"LIMITED_NUMBER_VALUE"];
        NSUInteger levelLimit = [userDefaults integerForKey:@"WARNING_LIMIT_VALUE_LEVEL"];
        
        NSArray* arrayUnitMax = [userDefaults arrayForKey:@"UNIT_MAX"];
        NSArray* array = [arrayUnitMax objectAtIndex:intUnit];
        float limit = [[array objectAtIndex:levelLimit] floatValue];
        
        NSUInteger result = [UtilDensity convertDensityToInt:limit];
        
        // 현재 결과가 기준치를 넘었을때
        if (self.density >= result) {
            
            
            // 헬스케어 메세지 on 날짜
            NSDate* date = [userDefaults objectForKey:@"WARNING_MESSAGE_FLAG_DATE"];
            //NSString* strDate = [UtilDate stringFormat:@"yyyy-MM-dd" date:date];
            
            if (date != nil)
            {
                NSInteger count = [DataDensity selectCount:@"ME" density:result date:date];
                if (count >= level) {
                    [self.viewAlertWarning setHidden:NO];
                }
            }
        } 
    }
}

- (void)initGraphwidth {
    CGSize sizeScrollView = self.scrollView.frame.size;
    CGSize size = CGSizeMake(self.dataGraphView.lineWidth, sizeScrollView.height);
    [self.scrollView setContentSize:size];
    
    //그래프 화살표 데이터 있을 시 표시(2019.01.14)
    CGFloat widthScrollView = scrollView.frame.size.width;
    CGFloat widthContent = scrollView.contentSize.width;
    
    if(widthContent <= widthScrollView){
        [self.leftBtn setHidden:YES];
        [self.rightBtn setHidden:YES];
    }else{
        [self.leftBtn setHidden:NO];
        [self.rightBtn setHidden:NO];
    }
}

// viewDidLayoutSubviews
- (void)viewDidLayoutSubviews
{

    CGSize sizeScrollViewContents = self.scrollViewContents.frame.size;
    float height = viewResult.frame.size.height + dataGraphView.frame.size.height + viewBUttonSNS.frame.size.height;
    CGSize sizeContents = CGSizeMake(sizeScrollViewContents.width, height);
    [self.scrollViewContents setContentSize:sizeContents];
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

// 그래프 : 지우고 다시 그리기
- (void) drawGraphAll
{
    [self.dataGraphLabelY drawClear];
    [self.dataGraphLabelY setNeedsDisplay];
    
    [self.dataGraphView drawClear];
    [self.dataGraphView setNeedsDisplay];
    
//    CGPoint point = CGPointMake(self.scrollView.contentSize.width, 0.0f) ;
//    [self.scrollView setContentOffset:point animated:NO];
}

// 스크롤뷰 : 맨 마지막으로 이동
- (void) setContentOffsetScrollView
{
    float offset = self.scrollView.contentSize.width-self.scrollView.frame.size.width;
    CGPoint point = CGPointMake(offset, 0.0f);
    [self.scrollView setContentOffset:point animated:NO];
}

// 네비바 설정 : 카운트, 디데이 정보
- (void) setNavBarInfo
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
}

// 버튼 비활성화 해제
- (void) setEnableControls
{
    // 백버튼 탭바 SNS 딤드 2019.01.03
    [self.viewDimmedNavBar setHidden:YES];
    [self.tabbarVC.viewDimmed setHidden:YES];
    [self.viewDimmendSNS setHidden:YES];
    [self.viewDimmedRestart setEnabled:YES];
    
    self.flagMove = YES;
}


//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

- (void)receiveData10:(NSDictionary*)dic
{
    if (self.viewAlertWarning.hidden && self.flagMove)
    {
        [self touchUpButtonRetest:nil];
    }
}


//////////////////////////////////////////////////
////////// MARK: - 이벤트:
////////////////////////0//////////////////////////

- (IBAction)resignOnTop:(id)sender
{
    [self.avtiveTextView resignFirstResponder];
}

- (IBAction)touchUpButtonOkInAlert:(id)sender
{
    [self.viewAlertMemo setHidden:YES];
    
    [self.managedObjectCurrent setValue:self.textViewMemo.text forKey:@"memo"];
    [DataDensity modifyObject:self.managedObjectCurrent];
    [self resignOnTop:sender];
    
    /////////// 임시:데이터 확인 /////////////
    //NSArray* array = [DataDensity selectAllObject];
    //NSLog(@"array:%@", array);
    /////////// 임시:데이터 확인 /////////////
}

- (IBAction)touchUpButtonCloseInAlert:(id)sender
{
    [self.viewAlertMemo setHidden:YES];
    [self resignOnTop:sender];
}

- (IBAction)touchUpButtonInAlertWarning:(id)sender
{
    [self.viewAlertWarning setHidden:YES];
}

// Retest
- (IBAction)touchUpButtonRetest:(id)sender
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    if (keyfob.activePeripheral == nil || keyfob.activePeripheral.state == CBPeripheralStateDisconnected)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];

    } else
    {
        //[self.navigationController popViewControllerAnimated:YES];
        
        [self performSegueWithIdentifier:@"popSegueBlow" sender:self];
        
//        NSUInteger index = self.navigationController.viewControllers.count - 3;
//        UIViewController* vc = [self.navigationController.viewControllers objectAtIndex:index];
//        [self.navigationController popToViewController:vc animated:YES];
    }
}

// 공유 : 메모
- (IBAction)touchUpButtonMemo:(id)sender
{
    [self.viewAlertMemo setHidden:NO];
}

// 공유 : SMS
- (IBAction)touchUpButtonSMS:(id)sender
{
    UIImage* imageCapture = [UtilImage captureView:self.viewCapture];
    
    MFMessageComposeViewController* composer = [[MFMessageComposeViewController alloc] init];
    
    if (composer != nil) {
        
        composer.messageComposeDelegate = self;
        
        // MMS
        if([MFMessageComposeViewController respondsToSelector:@selector(canSendAttachments)] && [MFMessageComposeViewController canSendAttachments])
        {
            NSData* attachment = UIImageJPEGRepresentation(imageCapture, 1.0);
            NSString* uti = (NSString*)kUTTypeMessage;
            [composer addAttachmentData:attachment typeIdentifier:uti filename:@"capture.jpg"];
        }
        [self presentViewController:composer animated:YES completion:nil];
    }
}

// 공유 : 페이스북
- (IBAction)touchUpButtonFacebook:(id)sender
{
    UIImage* imageCapture = [UtilImage captureView:self.viewCapture];
    SLComposeViewController* compose = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [compose addImage:imageCapture];
    [self presentViewController:compose animated:YES completion:nil];
}

// 공유 : 트위터
- (IBAction)touchUpButtonTwitter:(id)sender
{
    UIImage* imageCapture = [UtilImage captureView:self.viewCapture];
    SLComposeViewController* compose = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [compose addImage:imageCapture];
    [self presentViewController:compose animated:YES completion:nil];
}

// 스크롤 : 왼쪽
- (IBAction)touchUpButtonLeft:(id)sender
{
    CGPoint point = scrollView.contentOffset;
    point.x = point.x - scrollView.frame.size.width;
    
    if (point.x < 0) {
        point.x = 0;
    }
    
    [self.scrollView setContentOffset:point animated:YES];
}

// 스크롤 : 오른쪽
- (IBAction)touchUpButtonRight:(id)sender
{
    CGFloat widthScrollView = scrollView.frame.size.width;
    CGFloat widthContent = scrollView.contentSize.width;
    
    CGPoint point = scrollView.contentOffset;
    point.x = point.x + widthScrollView;
    
    if (point.x > widthContent- widthScrollView) {
        point.x = widthContent- widthScrollView;
    }
    
    [self.scrollView setContentOffset:point animated:YES];
}

//////////////////////////////////////////////////
////////// MARK: - UITextViewDelegate
//////////////////////////////////////////////////

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.avtiveTextView = textView;
    self.avtiveTextView.delegate = self;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    self.avtiveTextView = nil;
}

- (BOOL)textViewShouldReturn:(UITextView *)textView
{
    [self.avtiveTextView resignFirstResponder];
    return NO;
}

//////////////////////////////////////////////////
////////// MARK: - MFMESSAGECOMPOSEVIEWCONTROLLERDELEGATE
//////////////////////////////////////////////////

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            //            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            //            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
