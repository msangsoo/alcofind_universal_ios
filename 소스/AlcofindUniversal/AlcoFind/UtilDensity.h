//
//  UitilDensity.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 2..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilDensity : NSObject

// Density 변환
+ (float) convertDensityToInt:(float)density;
+ (NSString*) convertDensity:(float)density;
+ (float) convertDensityFloat:(float)density;
+ (float) convertDensityGraphFloat:(float)density;

@end
