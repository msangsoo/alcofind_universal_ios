//
//  TabbarViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 26..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "TabbarViewController.h"

@interface TabbarViewController ()

@end

@implementation TabbarViewController

@synthesize buttonMenu1;
@synthesize buttonMenu2;
@synthesize buttonMenu3;
@synthesize buttonMenu4;
@synthesize viewDimmed;

//////////////////////////////////////////////////
////////// MARK: - Segue
//////////////////////////////////////////////////

// 세그웨이 실행시 콜백 : 리턴 NO할때 세그웨이 실행안됨
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{    
    UIButton* button = (UIButton*)sender;
    if (button.tag == 0) {
        return NO;
    }
    return YES;
}

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegateTabbar = self;
    
    /////////// 임시 /////////////
    //self.buttonMenu5.enabled = YES;
    /////////// 임시 /////////////
}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegateTabbar = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"tabbarDimmed" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.buttonMenu1.tag = 1;
    self.buttonMenu2.tag = 1;
    self.buttonMenu3.tag = 1;
    self.buttonMenu4.tag = 1;
    
    [self.viewDimmed setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tabbarDimmed:)
                                                 name:@"tabbarDimmed"
                                               object:nil];
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

// 블루투스 장치 연결 성공
- (void)connectedDevice:(NSDictionary *)dic
{
}

// 블루투스 장치 연결 해제
- (void) disconnectedDevice
{
}

//////////////////////////////////////////////////
////////// MARK: - 기능
//////////////////////////////////////////////////

// 버튼 상태 설정
- (void) setNoActionButtonIndex:(NSUInteger)index
{
    self.buttonMenu1.tag = 1;
    self.buttonMenu2.tag = 1;
    self.buttonMenu3.tag = 1;
    self.buttonMenu4.tag = 1;
    
    //그래프 메뉴 클릭 시 그래프 메인으로 이동(2019.01.04)
    NSString * currentView = self.navigationController.visibleViewController.title;

    if (index == 0) {
        if([currentView isEqualToString:@"Detail_View"]){
            self.buttonMenu1.tag = 0;
        }else{
            self.buttonMenu1.tag = 1;
        }
    } else if (index == 1) {
        self.buttonMenu2.tag = 0;
    } else if (index == 2) {
        self.buttonMenu3.tag = 0;
    } else if (index == 3) {
        self.buttonMenu4.tag = 0;
    }
}

// 버튼 상태 설정
- (void) setSelectedButtonIndex:(NSUInteger)index
{
    [self.buttonMenu1 setSelected:NO];
    [self.buttonMenu2 setSelected:NO];
    [self.buttonMenu3 setSelected:NO];
    [self.buttonMenu4 setSelected:NO];
    
    if (index == 0) {
        [self.buttonMenu1 setSelected:YES];
    } else if (index == 1) {
        [self.buttonMenu2 setSelected:YES];
    } else if (index == 2) {
        [self.buttonMenu3 setSelected:YES];
    } else if (index == 3) {
        [self.buttonMenu4 setSelected:YES];
    }
}

#pragma mark - push로 인한 탭메뉴 블락
- (void)tabbarDimmed:(NSNotification *)noti  {
    NSDictionary *notiDic = [noti userInfo];
    NSString *dimmed = [notiDic objectForKey:@"dimmed"];
    if([dimmed isEqualToString:@"NO"]){
        [self.viewDimmed setHidden:NO];
    }else{
        [self.viewDimmed setHidden:YES];
    }
    
}



@end
