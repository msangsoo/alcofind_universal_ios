//
//  ViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 25..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation ViewController

@synthesize arrayUnit;
@synthesize arrayLanguage;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.arrayUnit = [NSArray arrayWithObjects:@"Language", @"English", @"Korean", nil];
    self.arrayLanguage = [NSArray arrayWithObjects:@"Unit", @"Mode", nil];
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (pickerView == self.pickerViewLanguage) {
        return [self.arrayLanguage count];
        
    } else if (pickerView == self.pickerViewUnit) {
        return [self.arrayUnit count];
    }

    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView == self.pickerViewLanguage) {
        return [self.arrayLanguage objectAtIndex:row];
        
    } else if (pickerView == self.pickerViewUnit) {
        return [self.arrayUnit objectAtIndex:row];
    }

    return @"";
}


@end
