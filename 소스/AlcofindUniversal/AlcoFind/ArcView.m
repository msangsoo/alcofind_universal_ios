//
//  ArcView.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 6..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "ArcView.h"

@implementation ArcView

@synthesize strokeColor = _strokeColor;
@synthesize gauge = _gauge;
@synthesize angleStart = _angleStart;
@synthesize angleEnd = _angleEnd;
@synthesize caLayer;

- (void) animateArcTo100:(float)second
{
    // #2cdbff     RGB	44	219	255
    // 변수
    CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2+10);
    
    CGFloat radius = 105.0;
    
    // 5.5inch:11.0f / 4.7inch:10.0f  / 4inch : 9.0f
    // 3.5 inch
    if ([UtilScreen getInch] == INCH_3_5)
    {
        radius = 105.0;
        self.angleStart = 138.0f;
        self.gauge = 266.0f;
    }
    // 4 inch
    else if ([UtilScreen getInch] == INCH_4_0)
    {
        radius = 105.0;
        self.angleStart = 138.0f;
        self.gauge = 266.0f;
    }
    // 4.7 inch
    else if ([UtilScreen getInch] == INCH_4_7)
    {
        radius = 123.0;
        self.angleStart = 137.0f;
        self.gauge = 267.0f;
    }
    // 5.5 inch
    else if ([UtilScreen getInch] == INCH_5_5)
    {
        radius = 136.0;
        self.angleStart = 137.0f;
        self.gauge = 268.0f;
    }
    // 5.8 inch 1812 ko
    else if ([UtilScreen getInch] == INCH_5_8)
    {
        radius = 123.0;
        self.angleStart = 137.0f;
        self.gauge = 267.0f;
    }
    
    CGFloat start = (CGFloat)[self angleStart] * M_PI / 180.0;
    CGFloat end = (CGFloat)([self angleStart]+[self gauge]) * M_PI / 180.0;
    
    // 원호 그리기 : 패스 생성
    UIBezierPath* ovalPath = [[UIBezierPath alloc] init];
    [ovalPath addArcWithCenter:center radius:radius startAngle:start endAngle:end clockwise:YES];

    // 레이어
    self.caLayer = [[shape alloc] init];
    caLayer.path = ovalPath.CGPath;
    caLayer.strokeColor = [[self strokeColor] CGColor];
    caLayer.fillColor = [[UIColor clearColor] CGColor];
    
    // 5.5inch:11.0f / 4.7inch:10.0f  / 4inch : 9.0f
    // 3.5 inch 
    if ([UtilScreen getInch] == INCH_3_5)
    {
        caLayer.lineWidth = 20;
    }
    // 4 inch
    else if ([UtilScreen getInch] == INCH_4_0)
    {
        caLayer.lineWidth = 20;
    }
    // 4.7 inch
    else if ([UtilScreen getInch] == INCH_4_7)
    {
        caLayer.lineWidth = 21;
    }
    // 5.5 inch
    else if ([UtilScreen getInch] == INCH_5_5)
    {
        caLayer.lineWidth = 23;
    }
    // 5.8 inch 1812 ko
    else if ([UtilScreen getInch] == INCH_5_8)
    {
        caLayer.lineWidth = 21;
    }

    caLayer.lineCap = kCALineCapButt;   //kCALineCapButt, kCALineCapRound, kCALineCapSquare
    NSDictionary *actions = @{@"strokeStart": [NSNull null], @"strokeEnd": [NSNull null]};
    caLayer.actions = actions;
    [self.layer addSublayer:caLayer];
    
    // 글씨 그리기
//    CATextLayer *label = [[CATextLayer alloc] init];
//    [label setFont:@"Helvetica-Bold"];
//    [label setFontSize:20];
//    [label setFrame:CGRectMake(0, 0, 100, 100)];
//    [label setString:@"Hello"];
//    [label setAlignmentMode:kCAAlignmentCenter];
//    [label setForegroundColor:[[UIColor whiteColor] CGColor]];
//    [caLayer addSublayer:label];
    
    // 애니메이션
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    //anim.timingFunction = CAMediaTimingFunction(controlPoints: 0.58,1.0, 0.42,0.0);
    anim.duration = second;
    anim.fromValue = [NSNumber numberWithFloat:0.0f];
    anim.toValue = [NSNumber numberWithFloat:1.0f];
    anim.removedOnCompletion = NO;

    [CATransaction begin];
    [self.caLayer addAnimation:anim forKey:@"arcAnimation"];
    [CATransaction commit];
}

- (void) animateArcTo0:(float)second
{
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    anim.duration = second;
    anim.fromValue = [NSNumber numberWithFloat:1.0f];
    anim.toValue = [NSNumber numberWithFloat:0.0f];
    anim.removedOnCompletion = NO;
    anim.fillMode = kCAFillModeForwards;
    
    [CATransaction begin];
    [self.caLayer addAnimation:anim forKey:@"arcAnimationStart"];
    [CATransaction commit];
}

- (void) animateArcTo0
{
    self.caLayer.strokeEnd = 0.0f;
}

- (void)drawRect:(CGRect)rect
{
    // 스토리보드에서 보이게
    ////////////////// 임시 //////////////////////
    //[self animateArcTo100:1.0f];
    ////////////////// 임시 //////////////////////
}

@end
