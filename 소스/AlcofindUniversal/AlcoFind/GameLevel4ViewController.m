//
//  GameLevel4ViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "GameLevel4ViewController.h"

@interface GameLevel4ViewController ()

@end

@implementation GameLevel4ViewController

@synthesize tableView;
@synthesize buttonRestart;
@synthesize buttonResult;
@synthesize labelCount;
@synthesize labelDday;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

// viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = self;
    
    [self.buttonRestart setTitle:[UtilLocal localString:@"Restart"] forState:UIControlStateNormal];
    [self.buttonResult setTitle:[UtilLocal localString:@"Result"] forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:4];
            [tabbarVC setSelectedButtonIndex:4];
        }
    }
    
    [self setNavBarInfo];
}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = nil;
}

// viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setSortData];
}

// viewDidLayoutSubviews
- (void)viewDidLayoutSubviews
{
    //[self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

// 네비바 설정 : 카운트, 디데이 정보
- (void) setNavBarInfo
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
    
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

// 디바이스에서 버튼 눌렀을때
- (void)receiveData10:(NSDictionary*)dic
{
    [self touchUpButtonRestart:nil];
}

//////////////////////////////////////////////////
////////// MARK: - 기능
//////////////////////////////////////////////////

- (void) setSortData
{
    /////////////// 임시 //////////////
//    [[TIBLECBKeyfob keyfob] controlSetup];
//    
//    NSMutableArray* array1 = [[TIBLECBKeyfob keyfob] arrayGameInfo];
//    
//    for (int i=0; i<5; i++)
//    {
//        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
//        
//        NSString* strEstimated = [NSString stringWithFormat:@"%d", 100];
//        NSString* strDensity = [NSString stringWithFormat:@"%d", 10];
//        NSString* strName = [NSString stringWithFormat:@"이름%d", i];
//        
//        if (i == 2) { strDensity = @"9"; }
//        if (i == 3) { strDensity = @"9"; }
//        
//        // 입력한 값 저장
//        [dic setValue:strName forKey:@"Name"];
//        [dic setValue:strEstimated forKey:@"Estimated"];
//        [dic setValue:strDensity forKey:@"Density"];
//        
//        [array1 addObject:dic];
//    }
    /////////////// 임시 //////////////
    
    // 데이터
    NSMutableArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
    for (int i=0; i<array.count; i++)
    {
        NSMutableDictionary* dic = [array objectAtIndex:i];
        
        // 차이 계산
        float yourguess = [[dic valueForKey:@"Estimated"] floatValue];
        
        float density   = [[dic valueForKey:@"Density"] floatValue];
        
        /////////////////// 임시 //////////////////////
        //density = 100;
        /////////////////// 임시 //////////////////////
        
        density = [UtilDensity convertDensityFloat:density];
        
        float gap = yourguess - density;
        gap = fabsf(gap);
        [dic setValue:[NSNumber numberWithFloat:gap] forKey:@"Gap"];
    }
    
    // 정렬
    NSSortDescriptor *nameSort = [[NSSortDescriptor alloc] initWithKey:@"Gap" ascending:YES];  //배열을 기준으로 할 값을 설정 (오름차순)
    NSMutableArray *sortArr = [NSMutableArray arrayWithObject:nameSort];    //정렬의 기준을 담은(?) 배열을 생성
    [array sortUsingDescriptors:sortArr];    // 정렬
    
    // 데이터 : 순위 정하기
    int num = 1;
    float tempGap = -999.0f;
    for (int i=0; i<array.count; i++)
    {
        NSMutableDictionary* dic = [array objectAtIndex:i];
        if (tempGap != [[dic valueForKey:@"Gap"] floatValue]) {
            num = i+1;
        }
        tempGap = [[dic valueForKey:@"Gap"] floatValue];
        [dic setValue:[NSNumber numberWithInt:num] forKey:@"Rank"];
    }
    
    [self.tableView reloadData];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 :
//////////////////////////////////////////////////

- (IBAction)touchUpButtonRestart:(id)sender
{
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    if (keyfob.activePeripheral == nil || keyfob.activePeripheral.state == CBPeripheralStateDisconnected)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    } else
    {
        NSUInteger index = self.navigationController.viewControllers.count - 4;
        UIViewController* vc = [self.navigationController.viewControllers objectAtIndex:index];
        [self.navigationController popToViewController:vc animated:YES];
    }
}

//////////////////////////////////////////////////
////////// MARK: - 테이블뷰 DataSource Delegate
//////////////////////////////////////////////////

//DataDetailViewHeaderCell

// 테이블뷰 : 섹션 개수 반환
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// 테이블뷰 : 섹션당 셀 개수 반환
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
    return array.count;
}

// 테이블뷰 : 헤더 높이
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}

// 테이블뷰 : 푸터 높이
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

// 테이블뷰 : 데이터 설정된 헤더 반환
- (UIView *)tableView:(UITableView *)tableView1 viewForHeaderInSection:(NSInteger)section
{
    GameLevel4ViewHeaderCell* headerCell = [tableView1 dequeueReusableCellWithIdentifier:@"GameLevel4ViewHeaderCell"];

    headerCell.labelRanking.text = [UtilLocal localString:@"Ranking"];
    headerCell.labelName.text = [UtilLocal localString:@"name"];
    headerCell.labelEstimated.text = [UtilLocal localString:@"estimated"];
    headerCell.labelResult.text = [UtilLocal localString:@"result"];

    return headerCell;
}

// 테이블뷰 : 데이터 설정된 셀 반환
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GameLevel4ViewCell";
    GameLevel4ViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[GameLevel4ViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // 데이터
    // 이전단계에서 입력한 값 확인
    NSMutableArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
    NSMutableDictionary* dic = [array objectAtIndex:indexPath.row];
    
    int row = [[dic valueForKey:@"Rank"] intValue];
    if (row == 1) {
        cell.imageViewNo.hidden = NO;
        cell.labelNo.hidden = YES;
    } else {
        cell.imageViewNo.hidden = YES;
        cell.labelNo.hidden = NO;
    }
    
    // 데이터 : 셀
    cell.labelNo.text = [[dic valueForKey:@"Rank"] stringValue];
    cell.labelName.text = [dic valueForKey:@"Name"];
    cell.labelEstimated.text = [dic valueForKey:@"Estimated"];

    float density = [[dic valueForKey:@"Density"] floatValue];
    NSString* strDensity = [UtilDensity convertDensity:density];
    cell.labelResult.text = strDensity;
    
    return cell;
}

@end
