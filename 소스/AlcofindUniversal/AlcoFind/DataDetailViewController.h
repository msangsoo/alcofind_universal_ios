//
//  DataDetailViewController.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 22..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataDetailViewCell.h"
#import "DataDetailViewHeaderCell.h"
#import "DataDensity.h"
#import "UITextViewCustom.h"
#import "UtilDate.h"
#import "UtilDensity.h"
#import "UtilLocal.h"
#import "UtilImage.h"
#import "DataMapViewController.h"
#import "TabbarViewController.h"
#import "UIButtonCustom.h"

@interface DataDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *buttonMe;
@property (strong, nonatomic) IBOutlet UIButton *buttonFriend;
@property (strong, nonatomic) IBOutlet UIButton *buttonEdit;
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;

@property (assign, nonatomic) BOOL flagEdit;
@property (retain, nonatomic) NSString* type;
@property (retain, nonatomic) NSArray* arrayData;

@property (strong, nonatomic) IBOutlet UIView *viewAlertMemo;
@property (strong, nonatomic) IBOutlet UIView *viewAlertPhoto;
@property (strong, nonatomic) IBOutlet UITextViewCustom *textViewMemo;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewPhoto;

@property (strong, nonatomic) IBOutlet UILabel *labelMemoInAlertMemo;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertMemo;
@property (strong, nonatomic) IBOutlet UILabel *labelPhotoInAlertPhoto;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertPhoto;

@property (strong, nonatomic) IBOutlet UILabel *labelDeleteInAlertDel;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleInAlertDel;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertDel;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonCancelInAlertDel;

@property (retain, nonatomic) NSMutableArray* arrayUnit;
@property (strong, nonatomic) IBOutlet UIView *viewAlertDel;

@property (retain, nonatomic) UITableViewCell* selectedCell;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;

@end
