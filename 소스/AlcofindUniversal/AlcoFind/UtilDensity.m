//
//  UitilDensity.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 2..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "UtilDensity.h"

@implementation UtilDensity

// Density 변환
+ (float) convertDensityToInt:(float)density
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    // BAC
    if (intUnit == 0)
    {
        return density*10000;
    }
    // Promille
    else if (intUnit == 1)
    {
        return density*1000;
    }
    // g/L(%)
    else if (intUnit == 2)
    {
        return density*2;
    }
    // mg/L
    else if (intUnit == 3)
    {
        return density*2100;
    }
    // mg/100ml
    else if (intUnit == 4)
    {
        return density*10;
    }
    else
    {
        return 0;
    }
}

// Density 변환
+ (NSString*) convertDensity:(float)density
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    // BAC
    if (intUnit == 0)
    {
        float f1 = density/10000;
        float f2  = floor(f1*1000);
        float f3  = f2/1000;
        return [NSString stringWithFormat:@"%0.3f", f3];
    }
    // Promille
    else if (intUnit == 1)
    {
        float f1 = density/1000;
        float f2  = floor(f1*100);
        float f3  = f2/100;
        return [NSString stringWithFormat:@"%0.2f", f3];
    }
    // g/L(%)
    else if (intUnit == 2)
    {
        int f1  = floor(density/2);
        return [NSString stringWithFormat:@"%d", f1];
    }
    // mg/L
    else if (intUnit == 3)
    {
        float f1 = density/2100;
        float f2  = floor(f1*100);
        float f3  = f2/100;
        return [NSString stringWithFormat:@"%0.2f", f3];
    }
    // mg/100ml
    else if (intUnit == 4)
    {
        int f1  = floor(density/10);
        return [NSString stringWithFormat:@"%d", f1];
    }
    else
    {
        return @"";
    }
}

// Density 변환
+ (float) convertDensityFloat:(float)density
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    // BAC
    if (intUnit == 0)
    {
        float f1 = density/10000;
        float f2  = floor(f1*1000);
        float f3  = f2/1000;
        return f3;
    }
    // Promille
    else if (intUnit == 1)
    {
        float f1 = density/1000;
        float f2  = floor(f1*100);
        float f3  = f2/100;
        return f3;
    }
    // g/L(%)
    else if (intUnit == 2)
    {
        int f1  = floor(density/2);
        return f1;
    }
    // mg/L
    else if (intUnit == 3)
    {
        float f1 = density/2100;
        float f2  = floor(f1*100);
        float f3  = f2/100;
        return f3;
    }
    // mg/100ml
    else if (intUnit == 4)
    {
        int f1  = floor(density/10);
        return f1;
    }
    else
    {
        return 0.0;
    }
}

// Density 변환
+ (float) convertDensityGraphFloat:(float)density
{
    // 유저 데이터 변수
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSUInteger intUnit = [userDefaults integerForKey:@"UNIT_SELECTED"];
    
    // BAC
    if (intUnit == 0)
    {
        float f1 = density/10000;
        float f2  = floor(f1*1000);
        float f3  = f2/1000;
        float f4  = f3*1000;
        return f4;
    }
    // Promille
    else if (intUnit == 1)
    {
        float f1 = density/1000;
        float f2  = floor(f1*100);
        float f3  = f2/100;
        float f4  = f3*100;
        return f4;
    }
    // g/L(%)
    else if (intUnit == 2)
    {
        int f1  = floor(density/2);
        int f2 = f1/5;
        return f2;
    }
    // mg/L
    else if (intUnit == 3)
    {
        float f1 = density/2100;
        float f2  = floor(f1*100);
        float f3  = f2/100;
        float f4  = f3*200;
        return f4;
    }
    // mg/100ml
    else if (intUnit == 4)
    {
        int f1  = floor(density/10);
        return f1;
    }
    else
    {
        return 0.0;
    }
}

@end
