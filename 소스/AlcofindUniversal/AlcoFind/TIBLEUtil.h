//
//  TIBLEUtil.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 27..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIBLEUtil : NSObject

// Byte 배열 int 변환
+ (NSInteger) byteToInt:(NSData*)data;
+ (NSInteger) byteToInt:(Byte[])bytes length:(int)length;
+ (NSString*) byteToString:(Byte[])bytes length:(int)length;
@end
