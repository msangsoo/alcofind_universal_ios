//
//  UITextView+Placeholder.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2016. 2. 23..
//  Copyright © 2016년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Placeholder)


@property (nonatomic, readonly) UILabel *placeholderLabel;

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) NSAttributedString *attributedPlaceholder;
@property (nonatomic, strong) UIColor *placeholderColor;

+ (UIColor *)defaultPlaceholderColor;

@end
