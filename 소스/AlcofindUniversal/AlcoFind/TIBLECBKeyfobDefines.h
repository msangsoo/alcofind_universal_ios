//
//  TIBLECBKeyfobDefines.h
//  TI-BLE-Demo
//
//  Created by Ole Andreas Torvmark on 10/31/11.
//  Copyright (c) 2011 ST alliance AS. All rights reserved.
//

#ifndef TI_BLE_Demo_TIBLECBKeyfobDefines_h
#define TI_BLE_Demo_TIBLECBKeyfobDefines_h

// Defines for the TI CC2540 keyfob peripheral

#define TI_KEYFOB_PROXIMITY_ALERT_UUID                      0x1802
#define TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID             0x2a06
#define TI_KEYFOB_PROXIMITY_ALERT_ON_VAL                    0x01
#define TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL                   0x00
#define TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN                 1
#define TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID             0x1804
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID        0x2A07
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_READ_LEN    1

#define TI_KEYFOB_BATT_SERVICE_UUID                         0x180F
#define TI_KEYFOB_LEVEL_SERVICE_UUID                        0x2A19
#define TI_KEYFOB_LEVEL_SERVICE_READ_LEN                    1

#define TI_KEYFOB_ACCEL_SERVICE_UUID                        0xFFA0
#define TI_KEYFOB_ACCEL_ENABLER_UUID                        0xFFA1
#define TI_KEYFOB_ACCEL_RANGE_UUID                          0xFFA2
#define TI_KEYFOB_ACCEL_READ_LEN                            1
#define TI_KEYFOB_ACCEL_X_UUID                              0xFFA3
#define TI_KEYFOB_ACCEL_Y_UUID                              0xFFA4
#define TI_KEYFOB_ACCEL_Z_UUID                              0xFFA5

#define TI_KEYFOB_KEYS_SERVICE_UUID                         0xFFE0
#define TI_KEYFOB_KEYS_NOTIFICATION_UUID                    0xFFE1
#define TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN                1   

#define TI_KEYFOB_RECEIVE_NOTIFICATION_UUID                 0xFFE4

// AlcoFind - UUID:0xDA01
#define ALCOFIND_RECIEVE_01      0x01   //  0x01 : 페어링 시작
#define ALCOFIND_RECIEVE_05      0x05   //  0x05 : 대기 시간(Warm up time) 표시
#define ALCOFIND_RECIEVE_06      0x06   //  0x06 : Blow 대기
#define ALCOFIND_RECIEVE_07      0x07   //  0x07 : Blow 상태
#define ALCOFIND_RECIEVE_08      0x08   //  0x08 : 센서 반응
#define ALCOFIND_RECIEVE_09      0x09   //  0x09 : 농도 표시
#define ALCOFIND_RECIEVE_81      0x81   //  0x81 : ERROR 코드 (0x07:blow 시간부족 / 0x01:battery 부족 / 0x06:blow 시간초과)

// AlcoFind - UUID:0xDA04
#define ALCOFIND_RECIEVE_04     0x04   //  0x04 : 단독 사용 데이터 전송
#define ALCOFIND_SEND_04        0x04   //  0x04 : 단독 사용 데이터 전송

// AlcoFind - UUID:0xDA20
#define ALCOFIND_SEND_01        0x01   //  0x20 : 페어링 요청 (상태 전달)
#define ALCOFIND_SEND_05        0x05   //  0x20 : 대기 상태 요청 (상태 전달)
#define ALCOFIND_SEND_06        0x06   //  0x20 : Blow 대기 수신 확인 (상태 전달)
#define ALCOFIND_SEND_07        0x07   //  0x20 : Blow 상태 수신 확인 (상태 전달)
#define ALCOFIND_SEND_08        0x08   //  0x20 : 센서 반응 수신 확인 (상태 전달)
#define ALCOFIND_SEND_09        0x09   //  0x20 : APP Status (상태 전달)
#define ALCOFIND_SEND_11        0x11   //  0x20 : Data 페이지 (상태 전달)
#define ALCOFIND_SEND_21        0x21   //  0x20 : How To Use 페이지 (상태 전달)
#define ALCOFIND_SEND_31        0x31   //  0x20 : Setting 페이지 (상태 전달)

#define ALCOFIND_SEND_0B        0x0B   //  0x0B : 사용자 선택 화면 (상태 전달)
#define ALCOFIND_SEND_4B        0x4B   //  0x20 : 게임의 사용자 선택 화면 (상태 전달)
#define ALCOFIND_SEND_4C        0x4C   //  0x20 : 게임의 입력 화면 (상태 전달)

#endif

/* 구글 검색 참고 UUID
 * GATT Service UUIDs
 #define IMMEDIATE_ALERT_SERV_UUID       0x1802  // Immediate Alert
 #define LINK_LOSS_SERV_UUID             0x1803  // Link Loss
 #define TX_PWR_LEVEL_SERV_UUID          0x1804  // Tx Power
 #define CURRENT_TIME_SERV_UUID          0x1805  // Current Time Service
 #define REF_TIME_UPDATE_SERV_UUID       0x1806  // Reference Time Update Service
 #define NEXT_DST_CHANGE_SERV_UUID       0x1807  // Next DST Change Service
 #define GLUCOSE_SERV_UUID               0x1808  // Glucose
 #define THERMOMETER_SERV_UUID           0x1809  // Health Thermometer
 #define DEVINFO_SERV_UUID               0x180A  // Device Information
 #define NWA_SERV_UUID                   0x180B  // Network Availability
 #define HEARTRATE_SERV_UUID             0x180D  // Heart Rate
 #define PHONE_ALERT_STS_SERV_UUID       0x180E  // Phone Alert Status Service
 #define BATT_SERV_UUID                  0x180F  // Battery Service
 #define BLOODPRESSURE_SERV_UUID         0x1810  // Blood Pressure
 #define ALERT_NOTIF_SERV_UUID           0x1811  // Alert Notification Service
 #define HID_SERV_UUID                   0x1812  // Human Interface Device
 #define SCAN_PARAM_SERV_UUID            0x1813  // Scan Parameters
 #define RSC_SERV_UUID                   0x1814  // Running Speed and Cadence
 #define CSC_SERV_UUID                   0x1816  // Cycling Speed and Cadence
 #define CYCPWR_SERV_UUID                0x1818  // Cycling Power
 #define LOC_NAV_SERV_UUID               0x1819  // Location and Navigation
*/