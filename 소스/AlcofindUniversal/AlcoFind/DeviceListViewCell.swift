//
//  DeviceListViewCell.swift
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 26..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

import UIKit;

@objc class DeviceListViewCell: UITableViewCell {

    @IBOutlet var labelDeviceName: UILabel!
    @IBOutlet var labelDeviceUUID: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
