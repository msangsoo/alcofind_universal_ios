//
//  HowToUseViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 22..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"
#import "UIButtonCustom.h"

@interface HowToUseViewController : UIViewController<UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewPage2;

// 페이지1
@property (strong, nonatomic) IBOutlet UILabel *labelPage1Title;
@property (strong, nonatomic) IBOutlet UILabel *labelPage1Text1;

// 페이지2
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Title;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text1;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text2;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text3;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text4;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text5;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text6;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text7;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text8;
@property (strong, nonatomic) IBOutlet UILabel *labelPage2Text9;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewPage2Last;

@property (strong, nonatomic) IBOutlet UIButton *buttonPrev;
@property (strong, nonatomic) IBOutlet UIButton *buttonNext;

@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertInfo;
@property (strong, nonatomic) IBOutlet UIView *viewAlertInfo;

@end