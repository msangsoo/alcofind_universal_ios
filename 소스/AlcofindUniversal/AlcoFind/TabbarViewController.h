//
//  TabbarViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 26..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TIBLECBKeyfob.h"

@interface TabbarViewController : UIViewController <TIBLEDelegate>

@property (strong, nonatomic) IBOutlet UIButton *buttonMenu1;
@property (strong, nonatomic) IBOutlet UIButton *buttonMenu2;
@property (strong, nonatomic) IBOutlet UIButton *buttonMenu3;
@property (strong, nonatomic) IBOutlet UIButton *buttonMenu4;
@property (strong, nonatomic) IBOutlet UIView *viewDimmed;

- (void) setNoActionButtonIndex:(NSUInteger)index;
- (void) setSelectedButtonIndex:(NSUInteger)index;

@end
