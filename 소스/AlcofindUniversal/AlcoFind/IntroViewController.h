//
//  IntroViewController.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 24..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "UIButtonCustom.h"
#import "ActionSheetStringPicker.h"
#import "UtilLocal.h"
#import "TIBLECBKeyfob.h"

@interface IntroViewController : UIViewController <UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonLanguage;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonUnit;
@property (strong, nonatomic) IBOutlet UIButton *buttonOK;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;

@property (retain, nonatomic) NSMutableArray* arrayUnit;
@property (retain, nonatomic) NSMutableArray* arrayLanguage;

@end
