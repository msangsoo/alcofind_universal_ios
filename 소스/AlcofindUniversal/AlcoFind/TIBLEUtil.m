//
//  TIBLEUtil.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 27..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "TIBLEUtil.h"

@implementation TIBLEUtil


// NSData int 변환
+ (NSInteger) byteToInt:(NSData*)data
{
    
    Byte bytes[data.length];
    [data getBytes:&bytes];
    
    NSInteger result = 0;
    int temp = 0;
    
    for (int i=0; i<data.length; i++) {
        
        temp = bytes[i] & 0xFF;
        temp = temp << (8*(data.length-1-i));
        result += temp;
    }
    
    return result;
}

// Byte 배열 int 변환
+ (NSInteger) byteToInt:(Byte[])bytes length:(int)length
{
    NSInteger result = 0;
    int temp = 0;
    
    for (int i=0; i<length; i++) {
        
        temp = bytes[i] & 0xFF;
        temp = temp << (8*(length-1-i));
        result += temp;
    }
    
    return result;
}

// Byte 배열 String 변환
+ (NSString*) byteToString:(Byte[])bytes length:(int)length
{
    NSMutableString *str = [NSMutableString stringWithCapacity:length];
    for (int i = 0; i < length; i++)
    {
        [str appendFormat:@"%02x ", bytes[i]];
    }
    return str;
}

@end
