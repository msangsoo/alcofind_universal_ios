//
//  SettingViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 9. 23..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import "UISliderCustom.h"
#import "UILabelCustom.h"
#import "UIButtonCustom.h"
#import "DataDensity.h"
#import "ActionSheetStringPicker.h"
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"
#import "UITextViewCustom.h"
#import "UITextView+Placeholder.h"
#import "SQLClient.h"


@interface SettingViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewLanguage;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UISliderCustom *sliderAlarmLimit;
@property (strong, nonatomic) IBOutlet UISliderCustom *sliderWarningLimit;
@property (strong, nonatomic) IBOutlet UISliderCustom *sliderLimitedNumber;

@property (strong, nonatomic) IBOutlet UILabelCustom *labelAlarmLimitValue;
@property (strong, nonatomic) IBOutlet UILabel *labelAlarmLimitMinValue;
@property (strong, nonatomic) IBOutlet UILabel *labelAlarmLimitMaxValue;

@property (strong, nonatomic) IBOutlet UILabelCustom *labelWarningLimitValue;
@property (strong, nonatomic) IBOutlet UILabel *labelWarningLimitMinValue;
@property (strong, nonatomic) IBOutlet UILabel *labelWarningLimitMaxValue;

@property (strong, nonatomic) IBOutlet UILabelCustom *labelLimitedNumberValue;

@property (strong, nonatomic) IBOutlet UIView *viewHethcareOnOff;
@property (strong, nonatomic) IBOutlet UIView *viewAlertDel;
@property (strong, nonatomic) IBOutlet UIView *viewAlertInfo;
@property (strong, nonatomic) IBOutlet UIView *viewAlertServer;

// Localizable
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UILabel *labelAlarmLimit;
@property (strong, nonatomic) IBOutlet UILabel *labelHealthcare;
@property (strong, nonatomic) IBOutlet UILabel *labelWarningLimit;
@property (strong, nonatomic) IBOutlet UILabel *labelLimitedNumber;
@property (strong, nonatomic) IBOutlet UILabel *labelDeleteAll;
@property (strong, nonatomic) IBOutlet UILabel *labelSound;
@property (strong, nonatomic) IBOutlet UILabel *labelCamera;
@property (strong, nonatomic) IBOutlet UILabel *labelUnit;
@property (strong, nonatomic) IBOutlet UILabel *labelLanguage;
@property (strong, nonatomic) IBOutlet UILabel *labelDeleteInAlert;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleInAlert;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlert;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonCancelInAlert;
@property (strong, nonatomic) IBOutlet UIButton *buttonCloseInAlertInfo;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertInfo;
@property (strong, nonatomic) IBOutlet UILabel *labelServerInAlertInfo;

@property (strong, nonatomic) IBOutlet UIView *viewServerRow;

@property (strong, nonatomic) IBOutlet UIView *viewIP;
@property (strong, nonatomic) IBOutlet UITextField *txtIP;
@property (strong, nonatomic) IBOutlet UITextField *txtID;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtCompanyCode;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonSvrOK;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonSvrCancel;

// 버튼 on off
@property (strong, nonatomic) IBOutlet UIButton *buttonWarningMessage;
@property (strong, nonatomic) IBOutlet UIButton *buttonSound;
@property (strong, nonatomic) IBOutlet UIButton *buttonCamera;

// 셀렉트 박스
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonUnit;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonLanguage;

@property (retain, nonatomic) NSMutableArray* arrayUnit;
@property (retain, nonatomic) NSMutableArray* arrayLanguage;

@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

//헬스케어 경고 메시지 UI
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *LineConstraintHeight;
@property (weak, nonatomic) IBOutlet UIView *LineLayout;
@property (weak, nonatomic) IBOutlet UIView *HealthMessageLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *HealthMessageHeight;
@property (weak, nonatomic) IBOutlet UIView *WaringLimitLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *WaringLimitHeight;

@property (strong, nonatomic) IBOutlet UILabel *versionLb;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ServerHeight;



@end
