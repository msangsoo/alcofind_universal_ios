//
//  GameLevel2TableViewCell.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameLevel2ViewCell : UITableViewCell <UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UILabel *labelNo;
@property (strong, nonatomic) IBOutlet UITextField *textFieldName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldEstimated;

@end
