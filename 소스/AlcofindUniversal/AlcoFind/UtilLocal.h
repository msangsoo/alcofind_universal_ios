//
//  UtilLocal.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 29..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilLocal : NSObject

// 앱 지정 로컬 스트링 반환
+ (NSString*) localString:(NSString*)key;

@end
