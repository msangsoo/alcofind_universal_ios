//
//  DataMapViewController.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 18..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TabbarViewController.h"
#import "UtilLocal.h"

@interface DataMapViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (assign, nonatomic) CGFloat geo_lat;
@property (assign, nonatomic) CGFloat geo_lon;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

@end
