//
//  BlowViewController.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 6..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "BlowViewController.h"
#import "NSData+HexRepresentation.h"

@interface BlowViewController ()

@end

@implementation BlowViewController

@synthesize arcView;
@synthesize number;
@synthesize density;
@synthesize labelNumber;
@synthesize labelText;
@synthesize imageViewGuage;
@synthesize imagePicker;
@synthesize viewBlowing;
@synthesize viewAnalyzing;
@synthesize locationManager;
@synthesize managedObjectCurrent;
@synthesize avtiveTextView;
@synthesize scrollView;
@synthesize dataGraphView;
@synthesize dataGraphLabelY;
@synthesize scrollViewContents;
@synthesize viewLoading;
@synthesize imageViewLoading;
@synthesize timerSend5;
@synthesize timerCount;
@synthesize viewContents;
@synthesize audioPlayer;
@synthesize buttonBack;
@synthesize labelAnalyzing;
@synthesize viewAlertInfo;
@synthesize labelInfoInAlertInfo;
@synthesize labelMessageInAlertInfo;
@synthesize buttonOkInAlertInfo;
@synthesize flagCameara;
@synthesize flagTestResult;
@synthesize viewDimmedNavBar;
@synthesize labelCount;
@synthesize labelDday;
@synthesize flagBecome;

//////////////////////////////////////////////////
////////// MARK: - Segue
//////////////////////////////////////////////////
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"popSegueBlow"])
    {
        NSLog(@"456");
        self.flagTestResult = YES;
    }
    if ([segue.identifier isEqualToString:@"segueShowTestResult"])
    {
        
        TestResultViewController* vc = segue.destinationViewController;
        vc.density = self.density;
        vc.managedObjectCurrent = self.managedObjectCurrent;
        vc.delegate = self;
    }
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

- (void)setStateBlowing
{
    [self viewDidLoad];
}

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////
- (void)viewWillAppear:(BOOL)animated
{
    //
    [[TIBLECBKeyfob keyfob] setDelegate:self];
    
    self.flagBecome = NO;
    
    //
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    self.labelAnalyzing.text = [UtilLocal localString:@"Analyzing"];
    [self.buttonOkInAlertInfo setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    [self.tabbarVC setSelectedButtonIndex:2];
    
    // 백버튼 탭바 딤드
    [self.viewDimmedNavBar setHidden:YES];
    [self.tabbarVC.viewDimmed setHidden:YES];
    
    // 노티 등록
    NSNotificationCenter* noti = [NSNotificationCenter defaultCenter];
    [noti addObserver:self selector:@selector(viewBecome) name:UIApplicationWillEnterForegroundNotification object:nil];
    [noti addObserver:self selector:@selector(viewResign) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    // 네비바 설정 : 카운트, 디데이 정보
    [self setNavBarInfo];
 
    [self drawGraphAll];
    
    [self performSelector:@selector(initGraphwidth) withObject:nil afterDelay:0.1f];
    [self performSelector:@selector(setContentOffsetScrollView) withObject:nil afterDelay:0.1f];
    
    ///////////////// 임시 ////////////////
//    NSMutableDictionary* dicc = [NSMutableDictionary dictionary];
//    [dicc setObject:[NSNumber numberWithInt:10] forKey:@"WARMINGUP_TIME"];
//    [self performSelector:@selector(receiveData5:) withObject:dicc afterDelay:1.0f];
//    [self performSelector:@selector(receiveData6:) withObject:nil afterDelay:11.0f];
//    [self performSelector:@selector(receiveData7:) withObject:nil afterDelay:12.0f];
//    [self performSelector:@selector(receiveData8:) withObject:nil afterDelay:17.0f];
//    [self performSelector:@selector(receiveData9:) withObject:nil afterDelay:18.0f];
    ///////////////// 임시 ////////////////
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (!self.flagCameara)
    {
        TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
        keyfob.delegate = nil;
    }
    
    
    // 노티 해제
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            self.tabbarVC = (TabbarViewController*)vc;
        }
    }
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    self.flagCameara = NO;
    self.flagTestResult = NO;
    
    // 블로잉 상태 설정
    [self.viewBlowing setHidden:NO];
    [self.viewAnalyzing setHidden:YES];
    [self.viewLoading setHidden:NO];
    [self.viewAlertInfo setHidden:YES];
    [self performSelector:@selector(animateLoading) withObject:nil afterDelay:0.5f];
    [self.arcView animateArcTo0];
    self.labelNumber.text = @"00";
    
    //////////////// 임시  //////////////////
    //[self.viewLoading setHidden:YES];
    //////////////// 임시  //////////////////
    
    // 위치 : 현재위치 얻어오기
    self.locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
    // 위치 : 사용중에만 위치 정보 요청
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    // 5번 보내고 받을때 까지 로딩
    timerCount = 0;
    self.timerSend5 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(sendData5:) userInfo:nil repeats:YES];
    
    // 그래프 : 데이터
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    NSString* type = [dic valueForKey:@"type"];
    NSArray* arrayData = [DataDensity selectObject:type limit:20];
    arrayData = [[arrayData reverseObjectEnumerator] allObjects];
    
    // 그래프 : 설정
    self.dataGraphView.arrayData = arrayData;
    self.dataGraphView.flagLast = NO;
    self.dataGraphView.level = 11;
    self.dataGraphLabelY.level = 11;
    
    // 3.5 inch
    if ([UtilScreen getInch] == INCH_3_5)
    {
        self.dataGraphView.spacingY = 9.0f;
        self.dataGraphLabelY.spacingY = 9.0f;
    }
    // 4 inch
    else if ([UtilScreen getInch] == INCH_4_0)
    {
        self.dataGraphView.spacingY = 10.0f;
        self.dataGraphLabelY.spacingY = 10.0f;
    }
    // 4.7 inch
    else if ([UtilScreen getInch] == INCH_4_7)
    {
        self.dataGraphView.spacingY = 12.0f;
        self.dataGraphLabelY.spacingY = 12.0f;
    }
    // 5.5 inch
    else if ([UtilScreen getInch] == INCH_5_5)
    {
        self.dataGraphView.spacingY = 13.0f;
        self.dataGraphLabelY.spacingY = 13.0f;
    }
    // 5.8 inch 1812 ko
    else if ([UtilScreen getInch] == INCH_5_8)
    {
        self.dataGraphView.spacingY = 13.0f;
        self.dataGraphLabelY.spacingY = 13.0f;
    }
    

}

- (void)initGraphwidth {
    NSLog(@"initGraphwidth");
    CGSize sizeScrollView = self.scrollView.frame.size;
    CGSize size = CGSizeMake(self.dataGraphView.lineWidth, sizeScrollView.height);
    [self.scrollView setContentSize:size];
    
    //그래프 화살표 데이터 있을 시 표시(2019.01.14)
    
    CGFloat widthScrollView = scrollView.frame.size.width;
    CGFloat widthContent = scrollView.contentSize.width;
    
    if(widthContent <= widthScrollView){
        [self.leftBtn setHidden:YES];
        [self.rightBtn setHidden:YES];
    }else{
        [self.leftBtn setHidden:NO];
        [self.rightBtn setHidden:NO];
    }
}


// viewDidLayoutSubviews
- (void)viewDidLayoutSubviews
{

    CGSize sizeScrollViewContents = self.scrollViewContents.frame.size;
    float height = viewBlowing.frame.size.height + dataGraphView.frame.size.height;
    CGSize sizeContents = CGSizeMake(sizeScrollViewContents.width, height+38);
    [self.scrollViewContents setContentSize:sizeContents];
}

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

- (void) viewBecome
{
    self.flagBecome = YES;
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Info"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"TestMessageInAlert"];
    [self.viewAlertInfo setHidden:NO];
}

// UIApplicationDidBecomeActiveNotification
- (void) viewResign
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Info"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"TestMessageInAlert"];
    [self.viewAlertInfo setHidden:NO];
}

// 스크롤뷰 : 맨 마지막으로 이동
- (void) setContentOffsetScrollView
{
    NSLog(@"setContentOffsetScrollView");
    float offset = self.scrollView.contentSize.width-self.scrollView.frame.size.width;
    CGPoint point = CGPointMake(offset, 0.0f);
    [self.scrollView setContentOffset:point animated:NO];

}

- (void) drawGraphAll
{
    [self.dataGraphLabelY drawClear];
    [self.dataGraphLabelY setNeedsDisplay];
    
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    NSString* type = [dic valueForKey:@"type"];
    NSArray* arrayData = [DataDensity selectObject:type limit:20];
    arrayData = [[arrayData reverseObjectEnumerator] allObjects];
    
    // 그래프 : 설정 1812 ko
    self.dataGraphView.arrayData = arrayData;
    self.dataGraphView.flagLast = NO;
    
    // STD일 경우 그래프 0.08까지 표시(18.12.17)
    NSString *DEVICE_TYPE = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    if ([DEVICE_TYPE isEqualToString:@"ST"] || [DEVICE_TYPE isEqualToString:@"ST_SVR"]) {
        self.dataGraphView.level = 9;
        self.dataGraphLabelY.level = 9;
    }
    
    [self.dataGraphView drawClear];
    [self.dataGraphView setNeedsDisplay];
    
//    CGPoint point = CGPointMake(self.scrollView.contentSize.width, 0.0f) ;
//    [self.scrollView setContentOffset:point animated:NO];
}

// 네비바 설정 : 카운트, 디데이 정보
- (void) setNavBarInfo
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [[TIBLECBKeyfob keyfob] setDelegate:self];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }

}

//////////////////////////////////////////////////
////////// MARK: - 기능: 블루투스 통신
//////////////////////////////////////////////////

- (void) sendData5:(NSTimer *)timer
{
    timerCount++;
    if (timerCount <=5)
    {
        [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_05];
        NSLog(@"5번 보내기");
    } else {
        [self.timerSend5 invalidate];
        self.timerSend5 = nil;
        timerCount = 0;
    }
}

// 5. 워밍업 시작
- (void)receiveData5:(NSDictionary *)dic
{
    NSLog(@"5. 워밍업 시작");
    
    if (self.flagBecome) return;
    
    [self.timerSend5 invalidate];
    self.timerSend5 = nil;
    timerCount = 0;
    
    // 5번 대기 로딩
    [self.viewBlowing setHidden:NO];
    [self.viewAnalyzing setHidden:YES];
    [self.viewLoading setHidden:YES];
    [self.imageViewLoading.layer removeAllAnimations];
    
    NSInteger time = [[dic objectForKey:@"WARMINGUP_TIME"] integerValue];
    self.number =  0;
    float second = time;
    [self.arcView animateArcTo100:second];
    [self animateNumberTo100:second];
    
    self.labelText.text = [UtilLocal localString:@"WarmingUp"];
    self.labelText.textColor = [UIColor whiteColor];
    
    // 워밍업 95% : Breath deep
    [self performSelector:@selector(setBreathDeep) withObject:nil afterDelay:second*0.95f];
}

// 5_1. 워밍업 95% : Breath deep
- (void) setBreathDeep
{
    self.labelText.text = [UtilLocal localString:@"BreathDeep"];
    self.labelText.textColor = [UIColor whiteColor];
}

// 5_2. 워밍업 종료
- (void)receiveData5_1:(NSDictionary *)dic
{
    NSLog(@"5_2. 워밍업 종료");
    if (self.flagBecome) return;
    
    float second = 0.25f;
    //[self animateNumberTo0:second];
    labelNumber.text = @"00";
    
    [self.arcView animateArcTo0:second];
    [self performSelector:@selector(animateBlickText) withObject:nil afterDelay:second];
    
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_06];
}

// 6. 블로우 대기
- (void)receiveData6:(NSDictionary *)dic
{
    NSLog(@"6. 블로우 대기");
    
//    if (self.flagBecome) return;
//    
//    float second = 0.25f;
//    //[self animateNumberTo0:second];
//    labelNumber.text = @"00";
//    
//    [self.arcView animateArcTo0:second];
//    [self performSelector:@selector(animateBlickText) withObject:nil afterDelay:second];
//    
//    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_06];
}

// 7. 블로우 시작
- (void)receiveData7:(NSDictionary *)dic
{
    NSLog(@"7. 블로우 시작");
    
    if (self.flagBecome) return;
    
    // 백버튼 탭바 딤드
    [self.viewDimmedNavBar setHidden:NO];
    [self.tabbarVC.viewDimmed setHidden:NO];
    
    //1812 ko
    float second = 8.5f;
    NSString *type = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    if ([type isEqualToString:@"ST"] || [type isEqualToString:@"ST_SVR"]) {
        second = 5.0f;
    }
    
    NSLog(@"Blowtime : %f",second);
    
    [self.arcView animateArcTo100:second];
    [self animateNumberTo100:second];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagSound = [userDefaults boolForKey:@"SOUND_FLAG"];
    
    if (flagSound)
    {
        // beep 5초간 재생
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        NSURL* url = [[NSBundle mainBundle] URLForResource:@"beep-11" withExtension:@"mp3"];
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        self.audioPlayer.volume = 0.5;
        [self.audioPlayer play];
    }
    //
//    for (int i =0 ; i<5; i++)
//    {
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
//    }
    
    
    
    
    // 깜빡임 애니메이션 종료
    [self.labelText.layer removeAllAnimations];
    self.labelText.text = [UtilLocal localString:@"KeepBlowing"];
    self.labelText.textColor = [UIColor whiteColor];
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_07];
}

// 7_1. 블로우 : 카메라
- (void)receiveData7_1:(NSDictionary *)dic
{
    NSLog(@"7_1. 블로우 : 카메라");
    
    if (self.flagBecome) return;
    
    // 카메라 승인 안했을 경우
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusDenied){
        
        // 유저 데이터 : 카메라 기능 끄기
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:NO forKey:@"CAMERA_FLAG"];
        [userDefaults synchronize];
    }
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagCamera = [userDefaults boolForKey:@"CAMERA_FLAG"];
    
    if (flagCamera)
    {
        // 디바이스 지원 확인 : 카메라 존재 여부
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            self.flagCameara = YES;
            imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.allowsEditing = YES;
            imagePicker.showsCameraControls = NO;
            imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
            [self presentViewController:imagePicker animated:YES completion:^{
                self.flagCameara = NO;
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self.imagePicker selector:@selector(takePicture) userInfo:nil repeats:NO];
                [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(rotateImageView) userInfo:nil repeats:NO];
            }];
        }
    }
}

// 8. 분석중 : 센서반응
- (void)receiveData8:(NSDictionary *)dic
{
    NSLog(@"8. 분석중 : 센서반응");
    
    if (self.flagBecome) return;
    
    if (self.audioPlayer != nil && self.audioPlayer.playing)
    {
        [self.audioPlayer stop];
    }
    
    [self.viewBlowing setHidden:YES];
    [self.viewAnalyzing setHidden:NO];
    
    // 유저 데이터
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagCamera = [userDefaults boolForKey:@"CAMERA_FLAG"];
    
    if (!flagCamera)
    {
        [self rotateImageView];    // 애니메이션 : 로딩 360도
    }
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_08];
}

- (void)receiveData9:(NSDictionary *)dic
{
    NSLog(@"9. 결과 : 농도 표시");
    
    if (self.flagBecome) return;
    
    //이미지,사이즈 저장
    [dic setValue:svrImage forKey:@"col_img_data"];
    NSData *imgData = UIImageJPEGRepresentation(svrImage, 1.0);
    NSLog(@"Size of Image(bytes):%lu",(unsigned long)[imgData length]);
    NSString *col_img_size =[NSString stringWithFormat:@"%lu", (unsigned long)[imgData length]];
    [dic setValue:col_img_size forKey:@"col_img_size"];
    
    
    // 데이터 디비에 저장
    self.density = [[dic objectForKey:@"density"] integerValue];
    
    //////////// 임시 ///////////  MIN:84 MAX:1050
//    self.density = 2000;
    
    //////////// 임시 ///////////
    
    [self addCoreDataDensity:self.density];
    
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    // 카운트
    NSInteger count = [[dic objectForKey:@"count"] integerValue];
    [keyfob.dicNavInfo setObject:[NSNumber numberWithInteger:count] forKey:@"count"];
    
    // 디데이
    NSString* strDate = [dic objectForKey:@"date"];
    NSDate* date = [UtilDate dateFromString:strDate];
    
    // 디데이 : 날짜 초기화 되지 않았을때
    if ([@"00000000" isEqualToString:strDate]) {
        date = [NSDate date];
    } else {
        date = [UtilDate dateFromString:strDate];
    }
    
    NSInteger gap = [UtilDate compareDate:date date2:[NSDate date]];
    gap = 365 - gap;
    [keyfob.dicNavInfo setObject:[NSNumber numberWithInteger:gap] forKey:@"gap"];
    
    // 다음 교정일까지 남은 기간 (일)    
    [dic setValue:[NSNumber numberWithInteger:gap] forKey:@"gap"];
    
    //서버에 데이터 저장.
    NSString *deviceType = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TYPE"];
    if([deviceType isEqualToString:@"ST_SVR"]){
        [self serverSend:dic];
    }

    // 블루투스 통신 : 데이터 보내기
    [[TIBLECBKeyfob keyfob] sendData:ALCOFIND_SEND_09];
    
    // segue 호출
    [self performSegueWithIdentifier:@"segueShowTestResult" sender:self];
}

- (void)serverSend:(NSDictionary*)dic {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *rst_ip    = [userDefaults objectForKey:@"SERVER_RESULT_IP"];
    NSString *rst_db    = [userDefaults objectForKey:@"SERVER_RESULT_DBNAME"];
    NSString *rst_id    = [userDefaults objectForKey:@"SERVER_RESULT_ID"];
    NSString *rst_pass  = [userDefaults objectForKey:@"SERVER_RESULT_PASS"];
    NSString *svr_phone = [userDefaults objectForKey:@"SERVER_PHONE"];
    
    if (rst_ip==nil || rst_db==nil || rst_id==nil || rst_pass==nil) {
        NSLog(@"Error 서버 접속정보가 누락됨.");
        return;
    }
    
    NSData *imageData = nil;
    // hax로 변경
    if ([dic objectForKey:@"col_img_data"]) {
        imageData   = UIImageJPEGRepresentation((UIImage*)[dic objectForKey:@"col_img_data"], 1.0);
    }
    
//    NSData *imageData   = UIImagePNGRepresentation((UIImage*)[dic objectForKey:@"col_img_data"]);
//    NSString *hexStr    = [imageData hexString];
//
//    NSData *imageData = UIImagePNGRepresentation(myImage.image);
//    NSString* hexStr = [imageData hexRepresentationWithSpaces_AS:YES];

    NSString *address = [NSString stringWithFormat:@"%@:1433", rst_ip];
    NSString *query = [NSString stringWithFormat:@"INSERT INTO TB_RESULT (col_number, col_id_type, col_id_length, col_id, col_img_type, col_img_size, col_img_data, col_result, col_result_type, col_time, col_location, col_service_date, col_count_quick, col_count_normal, col_remain_date, col_error_code, col_empty1, col_empty2, col_empty3) VALUES ('%@', '%@', '%ld', '%@', '%@', '%@', %@, '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",
                       [dic objectForKey:@"col_number"],                            //col_number 고유키
                       @"1",                                                        //col_id_type
                       [svr_phone length],                                          //col_id_length
                       svr_phone,                                                   //col_id
                       imageData==nil?@"":@"JPG",                                 //col_img_type
                       imageData==nil?@"":[dic objectForKey:@"col_img_size"],     //col_img_size
                       imageData==nil?@"''":[self hexStringWithData:imageData],     //col_img_data 이미지
                       [dic objectForKey:@"density"],           //col_result 측정수치
                       @"N",                                    //col_result_type
                       [dic objectForKey:@"col_time"],          //col_time
                       @"",                                     //col_location
                       [dic objectForKey:@"col_service_date"],  //col_service_date
                       @"",                                     //col_count_quick
                       [dic objectForKey:@"count"],             //col_count_normal
                       [dic objectForKey:@"gap"],               // TODO: col_remain_date
                       [dic objectForKey:@"col_error_code"],    //col_error_code
                       @"",                                     //
                       @"",                                     //
                       @""                                      //
                       ];
    
    SQLClient* client = [SQLClient sharedInstance];
    
    [client connect:address username:rst_id password:rst_pass database:rst_db completion:^(BOOL success) {
        if (success)
        {
            [client execute:query completion:^(NSArray* results) {
                
                for (NSArray* table in results)
                {
                    for (NSDictionary* row in table)
                    {
                        for (NSString* column in row)
                        {
                            NSLog(@"측정정보 저장결과 %@=%@", column, row[column]);
                        }
                    }
                }
                [client disconnect];
                return;
            }];
        }
    }];
}


- (NSString*)hexStringWithData:(NSData*)data
{
    const unsigned char* dataBuffer = (const unsigned char*)[data bytes];
    if (!dataBuffer) {
        return [NSString string];
    }
    
    NSMutableString* output = [NSMutableString stringWithCapacity:(data.length * 2)];
    for (int i = 0; i < data.length; ++i) {
        [output appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    }
    return [NSString stringWithFormat:@"0x%@", output];
}

- (void)receiveError5:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle5"];
    [self.viewAlertInfo setHidden:NO];
}
- (void)receiveError6:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle6"];
    [self.viewAlertInfo setHidden:NO];
}
- (void)receiveError7:(NSDictionary *)dic
{
    // 소리 꺼짐
    if (self.audioPlayer != nil && self.audioPlayer.playing)
    {
        [self.audioPlayer stop];
    }
    
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle7"];
    [self.viewAlertInfo setHidden:NO];    
}
- (void)receiveError9:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Error"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle9"];
    [self.viewAlertInfo setHidden:NO];
}

- (void)receiveError11:(NSDictionary *)dic
{
    [self.labelInfoInAlertInfo setText:[UtilLocal localString:@"Temperature"]];
    self.labelMessageInAlertInfo.text = [UtilLocal localString:@"ErrorTitle11"];
    [self.viewAlertInfo setHidden:NO];
    
    [self.timerSend5 invalidate];
    self.timerSend5 = nil;
}

//////////////////////////////////////////////////
////////// MARK: - 기능
//////////////////////////////////////////////////

- (void) addCoreDataDensity:(NSInteger)value
{
    // 정보 저장
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    
    // 데이터 디비에 저장
    NSDate* date = [NSDate date];
    NSString* strDate = [UtilDate stringFormat:@"yyyy-MM-dd" date:date];
    
    ///////////// 임시 //////////////
//    value = 500;
    ///////////// 임시 //////////////
    
    [dic setValue:[NSNumber numberWithInteger:value] forKey:@"value"];
    [dic setValue:date forKey:@"reg_date"];
    [dic setValue:strDate forKey:@"group_date"];
    
    self.managedObjectCurrent = [DataDensity addObject:dic];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트:
////////////////////////0//////////////////////////

- (IBAction)resignOnTop:(id)sender
{
    [self.avtiveTextView resignFirstResponder];
}

// 스크롤 : 왼쪽
- (IBAction)touchUpButtonLeft:(id)sender
{
    CGPoint point = scrollView.contentOffset;
    point.x = point.x - scrollView.frame.size.width;
    
    if (point.x < 0) {
        point.x = 0;
    }
    
    [self.scrollView setContentOffset:point animated:YES];
}

// 스크롤 : 오른쪽
- (IBAction)touchUpButtonRight:(id)sender
{
    CGFloat widthScrollView = scrollView.frame.size.width;
    CGFloat widthContent = scrollView.contentSize.width;
    
    CGPoint point = scrollView.contentOffset;
    point.x = point.x + widthScrollView;
    
    if (point.x > widthContent- widthScrollView) {
        point.x = widthContent- widthScrollView;
    }
    
    [self.scrollView setContentOffset:point animated:YES];
}

- (IBAction)touchUpButtonInAlertInfo:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//////////////////////////////////////////////////
////////// MARK: - 애니메이션
//////////////////////////////////////////////////

// 숫자 카운트 : 0 -> 100
- (void) animateNumberTo100:(float)second
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (int i = 1; i < 101; i ++) {
            usleep(second/103 * 1000000); // sleep in microseconds
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString* str = [NSString stringWithFormat:@"%02d", i];
                labelNumber.text = str;
            });
        }
    });
}

// 숫자 카운트 : 100 -> 0
- (void) animateNumberTo0:(float)second
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (int i = 100; i >= 0; i --) {
            usleep(second/103 * 1000000); // sleep in microseconds
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString* str = [NSString stringWithFormat:@"%02d", i];
                labelNumber.text = str;
            });
        }
    });
}

// 애니메이션 : 로딩 360도
- (void)rotateImageView
{
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        imageViewGuage.transform = CGAffineTransformRotate(imageViewGuage.transform, M_PI_2);
    } completion:^(BOOL finished){
        if(finished){
            [self rotateImageView];
        }
    }];
}

// 애니메이션 : 문자 깜빡임
- (void) animateBlickText
{
    self.labelText.text = [UtilLocal localString:@"StartBlowing"];
    
    [UIView transitionWithView:self.labelText duration:0.25f options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^
     {
         self.labelText.textColor = [UIColor colorWithRed:0.28f green:0.72f blue:0.84f alpha:1.0f];
     }
     completion:^(BOOL finished)
     {
         if (finished) {
             [UIView transitionWithView:self.labelText duration:0.25f options:UIViewAnimationOptionTransitionCrossDissolve
                             animations:^
              {
                  self.labelText.textColor = [UIColor whiteColor];
              }
                             completion:^(BOOL finished)
              {
                  if (finished) {
                      [self animateBlickText];
                  }
              }];
         }
     }];
}

// 애니메이션 : 로딩 360도
- (void) animateLoading
{
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        imageViewLoading.transform = CGAffineTransformRotate(imageViewLoading.transform, M_PI_2);
    } completion:^(BOOL finished){
        if(finished){
            [self animateLoading];
        }
    }];
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

// 블루투스 장치 연결 해제
- (void) disconnectedDevice
{
    //////////// 임시 /////////////////
//    NSString* s1 = [UtilLocal localString:@"DISCONNECT_TITLE"];
//    NSString* s2 = [UtilLocal localString:@"DISCONNECT_MESSAGE"];
//    NSString* s3 = [UtilLocal localString:@"Done"];
//    
//    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:s1 message:s2 delegate:self cancelButtonTitle:s3 otherButtonTitles: nil];
//    alert.alertViewStyle = UIAlertViewStyleDefault;
//    [alert show];
    //////////// 임시 /////////////////
    
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// 블루투스 Off
- (void) statePoweredOff
{
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//////////////////////////////////////////////////
////////// MARK: - CLLocationManagerDelegate
//////////////////////////////////////////////////

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSUInteger lastindex = [locations count]-1;
    CLLocation * currentLocation = [locations objectAtIndex:lastindex];
    float latitude =currentLocation.coordinate.latitude;
    float longitude =currentLocation.coordinate.longitude;
    
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    [dic setObject:[NSNumber numberWithFloat:latitude] forKey:@"geo_lat"];
    [dic setObject:[NSNumber numberWithFloat:longitude] forKey:@"geo_lon"];
}

//////////////////////////////////////////////////
////////// MARK: - UIImagePickerControllerDelegate
//////////////////////////////////////////////////

//
// (showsCameraControls = true)  Use Photo 선택 후 콜백
// (showsCameraControls = NO) 사진 찍은 후 콜백
//
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    // 처음 찍힌 이미지
    UIImage* imageOriginal = [info objectForKey:UIImagePickerControllerOriginalImage];
    imageOriginal = [UtilImage cropImageCenter:imageOriginal withOrientation:UIImageOrientationRight];
    
    svrImage = [self imageWithImage:imageOriginal scaledToSize:CGSizeMake(400.0f, 300.0f)];
    
    // 이미지뷰에 나타내기
    //[self.imageViewPhoto setImage:imageOriginal];
    
    // 파일 저장
    NSTimeInterval seconds = [NSDate timeIntervalSinceReferenceDate];
    NSString* fileName = [NSString stringWithFormat:@"AlcoFind%0.0lf.jpg", seconds];
    [UtilImage saveImage:imageOriginal fileName:fileName];
    
    // 데이터 저장
    NSMutableDictionary* dic =[[TIBLECBKeyfob keyfob] dicTestInfo];
    [dic setObject:fileName forKey:@"photo_url"];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.4);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
