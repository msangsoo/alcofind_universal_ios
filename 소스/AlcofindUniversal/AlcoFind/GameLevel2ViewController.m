//
//  GameLevel2ViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "GameLevel2ViewController.h"

@interface GameLevel2ViewController ()

@end

@implementation GameLevel2ViewController

@synthesize tableView;
@synthesize activeField;
@synthesize labelGame;
@synthesize buttonBack;
@synthesize buttonStart;
@synthesize labelCount;
@synthesize labelDday;
@synthesize arrayTextFieldName;
@synthesize arrayTextFieldEstimated;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

// StatusBar 글씨 흰색
- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registNotification];
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = self;
    
    [self.labelGame setText:[UtilLocal localString:@"Game"]];
    [self.buttonBack setTitle:[UtilLocal localString:@"Back"] forState:UIControlStateNormal];
    [self.buttonStart setTitle:[UtilLocal localString:@"start"] forState:UIControlStateNormal];
    
    // 탭바컨트롤러 설정
    for (UIViewController* vc in self.childViewControllers)
    {
        if ([vc isKindOfClass:TabbarViewController.class])
        {
            TabbarViewController* tabbarVC = (TabbarViewController*)vc;
            [tabbarVC setNoActionButtonIndex:4];
            [tabbarVC setSelectedButtonIndex:4];
        }
    }
    
    [self setNavBarInfo];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegate = nil;
    
    [self unregistNotification];
}

// viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.arrayTextFieldName = [NSMutableArray array];
    self.arrayTextFieldEstimated = [NSMutableArray array];
    
    // 현재 상태 보내기
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob sendData:ALCOFIND_SEND_4C];
}

// viewDidLayoutSubviews
- (void)viewDidLayoutSubviews
{
    [self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
}

//////////////////////////////////////////////////
////////// MARK: - UI
//////////////////////////////////////////////////

// 네비바 설정 : 카운트, 디데이 정보
- (void) setNavBarInfo
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    
    // 네비바 카운트 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelCount setHidden:YES];
    }
    else
    {
        [self.labelCount setHidden:NO];
        NSInteger count = [[keyfob.dicNavInfo objectForKey:@"count"] integerValue];
        self.labelCount.text = [NSString stringWithFormat:@"No. %d", count];
    }
    
    // 네비바 디데이 표시
    if (keyfob.dicNavInfo == nil)
    {
        [self.labelDday setHidden:YES];
    } else {
        [self.labelDday setHidden:NO];
        NSInteger gap = [[keyfob.dicNavInfo objectForKey:@"gap"] integerValue];
        if (gap >= 0) { self.labelDday.text = [NSString stringWithFormat:@"D-%d", gap]; }
        else          { self.labelDday.text = [NSString stringWithFormat:@"D+%d", (NSInteger)fabsf(gap)]; }
    }
    
}

//////////////////////////////////////////////////
////////// MARK: - 기능 :
//////////////////////////////////////////////////

// 텍스트필드 체크
- (BOOL) checkTextFieldBlank
{
    BOOL flag = NO;
    NSMutableArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
    
    
    for (int i=0; i<array.count; i++)
    {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        GameLevel2ViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        NSString* strName = cell.textFieldName.text;
        NSString* strEstimated = cell.textFieldEstimated.text;
        
        // name 체크
        if ([@"" isEqualToString:strName])
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Check" message:@"Name 입력해 주세요" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];

            [cell.textFieldName becomeFirstResponder];
            flag = YES;
            return flag;
        }
        
        // Estimated 체크
        if ([@"" isEqualToString:strEstimated])
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Check" message:@"estimated 입력해 주세요" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];
            
            [cell.textFieldEstimated becomeFirstResponder];
            flag = YES;
            return flag;
        }
    }
    
    return flag;
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트 :
//////////////////////////////////////////////////

- (IBAction)touchUpButtonStart:(id)sender
{
    // 빈칸 없는지 체크
    if (![self checkTextFieldBlank])
    {
        NSMutableArray* array = [[TIBLECBKeyfob keyfob] arrayGameInfo];
        
        for (int i=0; i<array.count; i++)
        {
            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            GameLevel2ViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            NSString* strName = cell.textFieldName.text;
            NSString* strEstimated = cell.textFieldEstimated.text;
            
            // 입력한 값 저장
            NSMutableDictionary* dic = [array objectAtIndex:i];
            [dic setValue:strName forKey:@"Name"];
            [dic setValue:strEstimated forKey:@"Estimated"];
        }
        
        [self performSegueWithIdentifier:@"segueShowGameLevel3" sender:self];
    }
}

- (IBAction)resignOnTop:(id)sender
{
    [self.activeField resignFirstResponder];
}

//////////////////////////////////////////////////
////////// MARK: - 테이블뷰 DataSource Delegate
//////////////////////////////////////////////////

//DataDetailViewHeaderCell

// 테이블뷰 : 섹션 개수 반환
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// 테이블뷰 : 섹션당 셀 개수 반환
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[TIBLECBKeyfob keyfob] arrayGameInfo] count];
}

// 테이블뷰 : 헤더 높이
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}

// 테이블뷰 : 푸터 높이
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

// 테이블뷰 : 데이터 설정된 헤더 반환
- (UIView *)tableView:(UITableView *)tableView1 viewForHeaderInSection:(NSInteger)section
{
    GameLevel2ViewHeaderCell* headerCell = [tableView1 dequeueReusableCellWithIdentifier:@"GameLevel2ViewHeaderCell"];
    
    headerCell.labelNo.text = [UtilLocal localString:@"no"];
    headerCell.labelName.text = [UtilLocal localString:@"name"];
    headerCell.labelEstimated.text = [UtilLocal localString:@"estimated"];
    
    return headerCell;
}

// 테이블뷰 : 데이터 설정된 셀 반환
- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GameLevel2ViewCell";
    GameLevel2ViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[GameLevel2ViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textFieldName.delegate = self;
    cell.textFieldEstimated.delegate = self;
    
    [self.arrayTextFieldName insertObject:cell.textFieldName atIndex:indexPath.row];
    [self.arrayTextFieldEstimated insertObject:cell.textFieldEstimated atIndex:indexPath.row];
    
    [cell.textFieldName setPlaceholder:[UtilLocal localString:@"hintName"]];
    [cell.textFieldEstimated setPlaceholder:[UtilLocal localString:@"hintGuess"]];
    
    // 데이터 : 숫자
    NSInteger row = indexPath.row+1;
    NSString* strNo = [NSString stringWithFormat:@"%ld", row];
    
    // 데이터 : 셀
    cell.labelNo.text =  strNo;
    return cell;
}

//////////////////////////////////////////////////
////////// MARK: - UITextFieldDelegate
//////////////////////////////////////////////////

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField = textField;
    self.activeField.delegate = self;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // 네임
    for (int i=0; i<self.arrayTextFieldName.count; i++)
    {
        UITextField * field = [self.arrayTextFieldName objectAtIndex:i];
        if (field == textField)
        {
            if (i == self.arrayTextFieldName.count -1)
            {
                [self.activeField resignFirstResponder];
            }
            else
            {
                UITextField * field = [self.arrayTextFieldName objectAtIndex:i+1];
                [field becomeFirstResponder];
            }
        }
    }

    // 측정값
    for (int i=0; i<self.arrayTextFieldEstimated.count; i++)
    {
        UITextField * field = [self.arrayTextFieldEstimated objectAtIndex:i];
        if (field == textField)
        {
            if (i == self.arrayTextFieldEstimated.count -1)
            {
                [self.activeField resignFirstResponder];
            }
            else
            {
                UITextField * field = [self.arrayTextFieldEstimated objectAtIndex:i+1];
                [field becomeFirstResponder];
            }
        }
    }

    return NO;
}

//////////////////////////////////////////////////
////////// MARK: - Keyboard Event
//////////////////////////////////////////////////

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) )
    {
        [self.tableView setContentSize:CGSizeMake(CGRectGetWidth(self.tableView.frame), CGRectGetHeight(self.tableView.frame) + kbSize.height)];
        [self.tableView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}

-(void) registNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void) unregistNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification object:nil];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.tableView setContentOffset:CGPointZero animated:YES];
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

// 블루투스 장치 연결 해제
- (void) disconnectedDevice
{
//    NSString* s1 = [UtilLocal localString:@"DISCONNECT_TITLE"];
//    NSString* s2 = [UtilLocal localString:@"DISCONNECT_MESSAGE"];
//    NSString* s3 = [UtilLocal localString:@"Done"];
//    
//    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:s1 message:s2 delegate:self cancelButtonTitle:s3 otherButtonTitles: nil];
//    alert.alertViewStyle = UIAlertViewStyleDefault;
//    [alert show];
    
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}
// 블루투스 Off
- (void) statePoweredOff
{
    // 메인으로 이동
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)receiveData10:(NSDictionary*)dic
{
    [self touchUpButtonStart:nil];
}

@end
