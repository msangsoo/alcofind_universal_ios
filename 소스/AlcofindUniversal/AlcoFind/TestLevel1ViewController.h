//
//  TestLevel1ViewController.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 13..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TIBLECBKeyfob.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"

@interface TestLevel1ViewController : UIViewController <TIBLEDelegate>

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UIButton *buttonMe;
@property (strong, nonatomic) IBOutlet UIButton *buttonFriend;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

@end
