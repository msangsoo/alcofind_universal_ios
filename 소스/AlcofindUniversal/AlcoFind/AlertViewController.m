//
//  AlertViewController.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 11. 12..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "AlertViewController.h"

@interface AlertViewController ()

@end

@implementation AlertViewController

@synthesize labelMessage;
@synthesize labelTitle;
@synthesize buttonOk;

//////////////////////////////////////////////////
////////// MARK: - 뷰 Cycle
//////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegateAlert = self;
    
    [self.labelTitle setText:[UtilLocal localString:@"Error"]];
    [self.labelMessage setText:[UtilLocal localString:@"ErrorTitle1"]];
    [self.buttonOk setTitle:[UtilLocal localString:@"OK"] forState:UIControlStateNormal];
}

- (void)viewWillDisappear:(BOOL)animated
{
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    keyfob.delegateAlert = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view.superview setHidden:YES];
    [self.view.superview setUserInteractionEnabled:NO];
    
    [self.view setHidden:YES];
    [self.view setUserInteractionEnabled:NO];
}

//////////////////////////////////////////////////
////////// MARK: - TIBLEDelegate
//////////////////////////////////////////////////

- (void)receiveError1:(NSDictionary *)dic
{
    [self.view.superview setHidden:NO];
    [self.view.superview setUserInteractionEnabled:YES];
    
    [self.view setHidden:NO];
    [self.view setUserInteractionEnabled:YES];
}

//////////////////////////////////////////////////
////////// MARK: - 이벤트:
//////////////////////////////////////////////////

- (IBAction)touchUpButtonOk:(id)sender
{
    [self.view.superview setHidden:YES];
    [self.view.superview setUserInteractionEnabled:NO];
    
    [self.view setHidden:YES];
    [self.view setUserInteractionEnabled:NO];
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob disConnectNotiPeripheral];
}

- (IBAction)touchUpButtonClose:(id)sender
{
    [self.view.superview setHidden:YES];
    [self.view.superview setUserInteractionEnabled:NO];
    
    [self.view setHidden:YES];
    [self.view setUserInteractionEnabled:NO];
    
    TIBLECBKeyfob* keyfob = [TIBLECBKeyfob keyfob];
    [keyfob disConnectNotiPeripheral];
}

@end
