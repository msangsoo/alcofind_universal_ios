//
//  ArcView.h
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 6..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "shape.h"
#import "UtilScreen.h"

IB_DESIGNABLE
@interface ArcView : UIView

@property (retain, nonatomic) IBInspectable UIColor* strokeColor;
@property (assign, nonatomic) IBInspectable CGFloat angleStart;
@property (assign, nonatomic) IBInspectable CGFloat angleEnd;
@property (assign, nonatomic) IBInspectable CGFloat gauge;
@property (retain, nonatomic) shape* caLayer;

- (void) animateArcTo100:(float)second;
- (void) animateArcTo0:(float)second;
- (void) animateArcTo0;

@end
