//
//  shape.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 10. 7..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "shape.h"

@implementation shape

@dynamic volume;

+(BOOL)needsDisplayForKey:(NSString *)key
{
    
    //NSLog(@"%@", key);
    
    if ([key isEqualToString:@"arcAnimation"]) {
//        NSLog(@"zzz%@", key);
        return YES;
    }
    else if ([key isEqualToString:@"strokeEnd"]) {
//        NSLog(@"zzz%@", key);
        return YES;
    }
    else {
        return [super needsDisplayForKey:key];
    }
}

- (id<CAAction>)actionForKey:(NSString *)key
{
    if ([key isEqualToString:@"arcAnimation"]) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:key];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        animation.fromValue = @([[self presentationLayer] volume]);
        return animation;
    }
    return [super actionForKey:key];
}
- (CAAnimation *)animationForKey:(NSString *)key
{
    NSLog(@"animationForKey:%@", key);
    return [super animationForKey:key];
    
}
- (void)drawInContext:(CGContextRef)ctx
{
    NSLog(@"해봤자");
}
-(void)display
{
 
}

@end
