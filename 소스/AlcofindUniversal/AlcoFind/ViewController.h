//
//  ViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 8. 25..
//  Copyright (c) 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (retain, nonatomic) NSArray* arrayLanguage;
@property (retain, nonatomic) NSArray* arrayUnit;

@property (strong, nonatomic) IBOutlet UIPickerView *pickerViewLanguage;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerViewUnit;

@end

