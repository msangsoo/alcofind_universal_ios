//
//  TestResultViewController.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 11. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Social/Social.h>

#import "ArcView.h"
#import "DataDensity.h"
#import "UtilImage.h"
#import "UtilDate.h"
#import "UITextViewCustom.h"
#import "DataGraphView.h"
#import "DataGraphLabelY.h"
#import "TabbarViewController.h"
#import "UtilLocal.h"
#import "UIButtonCustom.h"
#import "TIBLECBKeyfob.h"
#import "UITextView+Placeholder.h"

@class BlowViewController;


@protocol TestResultViewDelegate <NSObject>
@optional
- (void) setStateBlowing;
@end

@interface TestResultViewController : UIViewController
<TIBLEDelegate, UITextViewDelegate, UINavigationControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic, retain) id<TestResultViewDelegate> delegate;

// ManagedObject
@property (retain, nonatomic) NSManagedObject* managedObjectCurrent;
@property (assign, nonatomic) NSUInteger density;

// 네비바
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UILabel *labelResultDate;

// 컨텐츠 영역
@property (strong, nonatomic) IBOutlet UIView *viewCapture;
@property (strong, nonatomic) IBOutlet UIView *viewResult;
@property (strong, nonatomic) IBOutlet UIView *viewPhoto;
@property (strong, nonatomic) IBOutlet UIView *viewBUttonSNS;
@property (strong, nonatomic) IBOutlet UIView *viewAlertMemo;
@property (strong, nonatomic) IBOutlet UIView *viewAlertWarning;
@property (strong, nonatomic) IBOutlet UIView *viewContents;

@property (retain, nonatomic) UITextView* avtiveTextView;
@property (strong, nonatomic) IBOutlet UILabel *labelNumber;
@property (strong, nonatomic) IBOutlet UILabel *labelText;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewContents;

@property (strong, nonatomic) IBOutlet UIImageView *imageViewPhoto;
@property (strong, nonatomic) IBOutlet UITextViewCustom *textViewMemo;

// 그래프
@property (strong, nonatomic) IBOutlet DataGraphView *dataGraphView;
@property (strong, nonatomic) IBOutlet DataGraphLabelY *dataGraphLabelY;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

// 얼럿 : 메모
@property (strong, nonatomic) IBOutlet UILabel *labelMemoInAlertMemo;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertMemo;

// 얼럿 : 워닝
@property (strong, nonatomic) IBOutlet UILabel *labelTitleInAlertWarning;
@property (strong, nonatomic) IBOutlet UILabel *labelMessageInAlertWarning;
@property (strong, nonatomic) IBOutlet UIButtonCustom *buttonOkInAlertWarning;
@property (strong, nonatomic) IBOutlet UILabel *labelCount;
@property (strong, nonatomic) IBOutlet UILabel *labelDday;

@property (strong, nonatomic) IBOutlet UIView *viewDimmedNavBar;
@property (strong, nonatomic) IBOutlet UIView *viewDimmendSNS;
@property(retain, nonatomic) TabbarViewController* tabbarVC;
@property (strong, nonatomic) IBOutlet UIButton *viewDimmedRestart;

@property (assign, nonatomic) BOOL flagMove;
@property (strong, nonatomic) IBOutlet UIButton *leftBtn;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;

@end
