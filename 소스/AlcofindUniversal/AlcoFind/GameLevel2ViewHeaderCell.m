//
//  GameLevel2ViewHeaderCell.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "GameLevel2ViewHeaderCell.h"

@implementation GameLevel2ViewHeaderCell

@synthesize labelNo;
@synthesize labelName;
@synthesize labelEstimated;

@end
