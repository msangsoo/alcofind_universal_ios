//
//  TIBLECBKeyfob.h
//  TI-BLE-Demo
//
//  Created by Ole Andreas Torvmark on 10/31/11.
//  Copyright (c) 2011 ST alliance AS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreBluetooth/CBService.h>
#import <UIKit/UIKit.h>
#import "TIBLECBKeyfobDefines.h"
#import "TIBLEUtil.h"
#import "UtilDate.h"

@protocol TIBLEDelegate <NSObject>
@optional
- (void)receiveData1:(NSDictionary *)dic;
- (void)receiveData4:(NSDictionary *)dic;
- (void)receiveData5:(NSDictionary *)dic;
- (void)receiveData5_1:(NSDictionary *)dic;
- (void)receiveData6:(NSDictionary *)dic;
- (void)receiveData7:(NSDictionary *)dic;
- (void)receiveData7_1:(NSDictionary *)dic;
- (void)receiveData8:(NSDictionary *)dic;
- (void)receiveData9:(NSDictionary *)dic;
- (void)receiveData10:(NSDictionary *)dic;

- (void)reFindDevice;

- (void)receiveError1:(NSDictionary *)dic;
- (void)receiveError2:(NSDictionary *)dic;
- (void)receiveError5:(NSDictionary *)dic;
- (void)receiveError6:(NSDictionary *)dic;
- (void)receiveError7:(NSDictionary *)dic;
- (void)receiveError9:(NSDictionary *)dic;
- (void)receiveError11:(NSDictionary *)dic;

- (void)disconnectedDevice;
- (void)connectedDevice:(NSDictionary *)dic;
- (void)connecting:(NSDictionary *)dic;

- (void)statePoweredOff;

@end

@interface TIBLECBKeyfob : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>
{
}

@property (nonatomic, retain) id<TIBLEDelegate> delegate;
@property (nonatomic, retain) id<TIBLEDelegate> delegateTabbar;
@property (nonatomic, retain) id<TIBLEDelegate> delegateAlert;

@property (strong, nonatomic) CBCentralManager *CM;
@property (strong, nonatomic) NSMutableArray *peripherals;
@property (strong, nonatomic) CBPeripheral *activePeripheral;
@property (strong, nonatomic) NSDictionary *activeAdvertisement;

// 상태값
@property (assign, nonatomic) BOOL boolBlowFlag;
@property (assign, nonatomic) BOOL boolResultFlag;
@property (assign, nonatomic) BOOL boolDeviceFlag;
@property (assign, nonatomic) BOOL boolBatteryFlag;
@property (retain, nonatomic) NSMutableArray* arrayGameInfo;
@property (assign, nonatomic) NSInteger arrayGameInfoIndex;
@property (retain, nonatomic) NSMutableDictionary* dicTestInfo;
@property (assign, nonatomic) NSInteger data4Length;
@property (assign, nonatomic) NSInteger data4Index;
@property (retain, nonatomic) NSMutableDictionary* dicNavInfo;

@property (retain, nonatomic) NSTimer* timer;

+ (TIBLECBKeyfob*) keyfob;
-(void) writeValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID  p:(CBPeripheral *)p data:(NSData *)data;
-(void) readValue: (int)serviceUUID characteristicUUID:(int)characteristicUUID  p:(CBPeripheral *)p;
-(void) notification:(int)serviceUUID characteristicUUID:(int)characteristicUUID  p:(CBPeripheral *)p on:(BOOL)on;

-(UInt16) swap:(UInt16) s;
-(int) controlSetup;
-(int) findDevices;
-(int) findBLEPeripherals:(int) timeout;
-(const char *) centralManagerStateToString:(int)state;
-(void) scanTimer:(NSTimer *)timer;
-(void) disConnectNotiPeripheral;
-(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p;
-(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service;
-(const char *) UUIDToString:(CFUUIDRef) UUID;
-(const char *) CBUUIDToString:(CBUUID *) UUID;
-(int) compareCBUUID:(CBUUID *) UUID1 UUID2:(CBUUID *)UUID2;
-(int) compareCBUUIDToInt:(CBUUID *) UUID1 UUID2:(UInt16)UUID2;
-(UInt16) CBUUIDToInt:(CBUUID *) UUID;
-(int) UUIDSAreEqual:(CFUUIDRef)u1 u2:(CFUUIDRef)u2;
- (void) stopScanning;
- (int) findBLEPeripherals;
- (void) findBLEPeripherals2;
// 블루투스 장치에 데이터 보내기
- (void)sendData:(Byte)b;
- (void) connectDirectDevice:(NSDictionary*)dic;

@end
