//
//  UtilImage.h
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 16..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UtilImage : NSObject

// 캡쳐이미지 반환
+ (UIImage*) captureView:(UIView*)view;
+ (void) saveImage:(UIImage*)image fileName:(NSString*)fileName;
+ (void) deleteImage:(NSString*)fileName;
+ (UIImage*) loadImage:(NSString*)fileName;
+ (UIImage *)cropImage:(UIImage *)image cropRect:(CGRect)cropRect;
+ (UIImage *) cropImageCenter:(UIImage *)image withOrientation:(UIImageOrientation)imageOrientation;
@end
