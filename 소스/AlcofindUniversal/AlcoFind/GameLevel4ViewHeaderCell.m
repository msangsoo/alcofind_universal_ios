//
//  GameLevel4ViewHeaderCell.m
//  AlcoFind
//
//  Created by GreamSoft_Mingz on 2015. 10. 5..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "GameLevel4ViewHeaderCell.h"

@implementation GameLevel4ViewHeaderCell

@synthesize labelEstimated;
@synthesize labelName;
@synthesize labelRanking;
@synthesize labelResult;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
