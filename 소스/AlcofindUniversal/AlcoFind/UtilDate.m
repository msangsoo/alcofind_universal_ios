//
//  UtilDate.m
//  AlcoFind
//
//  Created by MNJ_Mac on 2015. 9. 29..
//  Copyright © 2015년 GreamSoft_Mingz. All rights reserved.
//

#import "UtilDate.h"

@implementation UtilDate

// 날짜가져오기 : 오늘
+ (NSDate*)today
{
    return [NSDate date];
}

// 날짜가져오기 : 어제
+ (NSDate*)yesterday
{
    return [self day:-1];
}

// 날짜가져오기 : 내일
+ (NSDate*)tomorrow
{
    return [self day:+1];
}

// 날짜가져오기 : 이번주 첫번째 날짜 (일요일)
+ (NSDate*) firstdayInWeek
{
    NSDate* date = [NSDate date];
    NSCalendar* gregorian = [NSCalendar currentCalendar];
    NSDateComponents *components = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    
    //gregorian.firstWeekday;
    return [self day:-(components.weekday-1)];
}

// 날짜가져오기 : 며칠전 며칠후
+ (NSDate*)day:(NSInteger)num
{
    NSDate* date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit fromDate:date];
    
    comps.day = comps.day + num;
    comps.hour = comps.hour +1;
    return [calendar dateFromComponents:comps];
}

// 날짜가져오기 : 스트링을 날짜로 반환
+ (NSDate*) dateFromString:(NSString*)string
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [df setDateFormat:@"yyyyMMdd"];
    NSDate *myDate = [df dateFromString: string];
    
    return myDate;
}

// 날짜가져오기 : 스트링을 날짜로 반환
+ (NSDate*) dateFromString:(NSString*)string format:(NSString*)format
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [df setDateFormat:format];
    NSDate *myDate = [df dateFromString: string];
    
    return myDate;
}

// 날짜포멧 가져오기
+ (NSString*)stringFormat:(NSString*)format
{
    return [self stringFormat:format date:[NSDate date]];
}

// 날짜포멧 가져오기
+ (NSString*)stringFormat:(NSString*)format date:(NSDate*)date
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;  // yyyy.MM.dd HH:mm:ss
    
    // 미국 언어 설정
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:usLocale];
    
    NSString* strDate = [formatter stringFromDate:date];
    
    return strDate;
}

// NSDate 날짜 가져오기
+ (NSString*)stringFormat:(NSString*)format date:(NSDate*)date locale:(NSString*)locale
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;  // yyyy.MM.dd HH:mm:ss
    
    // 언어 설정
    // en, es, fr, de, pl, sv,
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:locale];
    [formatter setLocale:usLocale];
    
    NSString* strDate = [formatter stringFromDate:date];
    
    return strDate;
}

// 두날짜 비교 차이
+ (NSInteger) compareDate:(NSDate *)date1 date2:(NSDate *)date2
{
    NSDateComponents* dateComp;
    dateComp = [[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:date1 toDate:date2 options:0];
    NSInteger dayGap = [dateComp day];
    return dayGap;
}

// 현재 날짜 밀리세컨드로 가져오기
+ (double) getMilliseconds
{
    NSTimeInterval seconds = [NSDate timeIntervalSinceReferenceDate];
    return seconds*1000;
}

@end
